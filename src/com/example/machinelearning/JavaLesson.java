package com.example.machinelearning;

import java.util.ArrayList;
import java.util.Locale;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.machinelearning.Adapter.MarkAdapter;
import com.example.machinelearning.Database.Database;
import com.example.machinelearning.Database.LessonHandler;
import com.example.machinelearning.Database.QuizHandler;
import com.example.machinelearning.Database.StatisticsHandler;
import com.example.machinelearning.Utilities.GlobalVariables;
import com.example.machinelearning.imagedownloader.ImageLoader;

public class JavaLesson extends SwitchFragmentParent {
	//implements TextToSpeech.OnInitListener
	protected static final String TAG = "Java Lesson";
	int chapterNumber;
	int lessonNumber;
	int maxLesson = 0;
	LessonHandler lesson;
	//TextToSpeech textToSpeech;
	//TextView content;
	//private Button stop, read;
	TextView content;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {   
		view = inflater.inflate(R.layout.lesson, container, false);
		//textToSpeech = new TextToSpeech(getActivity(), this);
		//stop = (Button) view.findViewById(R.id.stop);
		//read = (Button) view.findViewById(R.id.read);

		// button on click event
		/*  stop.setOnClickListener(new View.OnClickListener() {

	    @Override
	    public void onClick(View arg0) {
	    textToSpeech.stop();

	        }
	    }); 		

		  // button on click event
        read.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                speakOut();
            }

        }); */

		imageLoad  = new ImageLoader(getActivity());
		Bundle bundle = this.getArguments();
		lessonNumber = bundle.getInt("lesson");
		chapterNumber = bundle.getInt("chapter");
		db = new Database(getActivity());
		maxLesson =  db.getMaxValueJava(chapterNumber);
		Log.i(TAG ,"max base of this chapter " + db.getMaxValueJava(chapterNumber));
		lesson = db.getJavaIDandChapter(lessonNumber, chapterNumber);
		Log.i(TAG, " chapter "+ chapterNumber + " lesson " + lessonNumber);
		setWidgets();
		return view; 

	}

	private void setWidgets() {
		//initialize all variables

		TextView title = (TextView) view.findViewById(R.id.title);
		content = (TextView) view.findViewById(R.id.content);
		ImageView image = (ImageView) view.findViewById(R.id.image);
		image.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				showFullScreenImage(lesson.getImage());
			}
		});


		Log.i(TAG, "image : " + lesson.getImage());
		imageLoad.DisplayImage(lesson.getImage(), image, R.drawable.ic_launcher);
		//fill xml
		title.setText(lesson.getTitle());
		content.setText(Html.fromHtml(lesson.getContent()));

		//set click listener on back button
		TextView back = (TextView) view.findViewById(R.id.back);
		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Log.i(TAG, "back");
				switchFrag.goBack(new Java());
			}
		});


		//set click listener on quiz button
		TextView quiz = (TextView) view.findViewById(R.id.quiz);
		//hide quiz

		quiz.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Log.i(TAG, "back");
				//textToSpeech.stop();
				confirmation();

			}
		});
		//set click listener on next button
		TextView next = (TextView) view.findViewById(R.id.next);
		next.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				int nextLesson  = db.getJavaNextLesson(lessonNumber, chapterNumber);
				Log.i(TAG, "next lesson : " + nextLesson + " previous " + lessonNumber + "chapter " + chapterNumber);
				LessonHandler lesson = db.getJavaIDandChapter(nextLesson, chapterNumber);
				if(lesson != null)
					switchFrag.nextLesson(nextLesson, chapterNumber , new JavaLesson());
				else
					switchFrag.goBack(new JavaLesson());
			}
		});

		if(lessonNumber == maxLesson){
			quiz.setVisibility(View.VISIBLE);
			next.setVisibility(View.GONE);
		}else{
			quiz.setVisibility(View.GONE);
			next.setVisibility(View.VISIBLE);
		}
	}

	protected void showFullScreenImage(String string) {
		final Dialog dialog = new Dialog(getActivity());
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow().setBackgroundDrawableResource(android.R.color.white);
		dialog.setContentView(R.layout.image_view);
		ImageView image = (ImageView) dialog.findViewById(R.id.image);

		imageLoad.DisplayImage(string, image, R.drawable.ic_launcher);

		dialog.show();
	}

	//	
	//	void confirmation(){
	//		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
	//			@Override
	//			public void onClick(DialogInterface dialog, int which) {
	//				switch (which){
	//				case DialogInterface.BUTTON_POSITIVE:
	//					ArrayList<QuizHandler> quizArray = db.getJavaAllQuizByChapter(chapterNumber);
	//					if(quizArray != null)
	//						switchLesson.switchToQuiz(chapterNumber, new JavaQuiz());
	//					else
	//						Toast.makeText(getActivity(), "No Exercise found in this chapter", Toast.LENGTH_SHORT).show();
	//					break;
	//				case DialogInterface.BUTTON_NEGATIVE:
	//					switchFrag.goBack(new JavaLesson());
	//					break;
	//				}
	//			}
	//		};
	//		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
	//		builder.setTitle("Alert").setMessage("Are you sure you want to take the exercise?").setPositiveButton("Yes", dialogClickListener)
	//		.setNegativeButton("No", dialogClickListener).show();
	//	
	//	}



	void confirmation(){
		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				switch (which){
				case DialogInterface.BUTTON_POSITIVE:
					Bundle bundle = new Bundle();
					bundle.putInt( "chapter", chapterNumber );
//					if(db.getStatsByChapterAndTypeCount(chapterNumber, GlobalVariables.JAVA_EXC) <= 0){
//						
//						switchFrag.switchFragment(new SimulationQuiz(), bundle);
//					}else{
//						Log.i(TAG, "result : " + db.getStatsByChapterAndTypeCount(chapterNumber, GlobalVariables.JAVA));
//						if(db.getStatsByChapterAndTypeCount(chapterNumber, GlobalVariables.JAVA) < GlobalVariables.QUOTA ){
							switchFrag.switchFragment(new SimulationQuiz(), bundle);
//						}else{
//							Toast.makeText(getActivity(), "You've already taken this simulation", Toast.LENGTH_LONG).show();
//							switchFrag.goBack(new JavaLesson());
//						}
//					}
					break;
				case DialogInterface.BUTTON_NEGATIVE:
					switchFrag.goBack(new JavaLesson());
					break;
				}
			}
		};
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Reminder").setMessage("Exercise Instruction:\n\nThe exercise is a Program Simulation\n\n- You have to simulate the program and identify the output.\n\n- You can type your answer on the given textbox below the program.\n\n- After answering the program simulation please tap the simulate button to finish the exercise.\n\nDo you want to continue on taking the exercise?").setPositiveButton("Yes", dialogClickListener)
		.setNegativeButton("No", dialogClickListener).show();


	}
	/*	

	   @Override
	    public void onDestroy() {
	        // Don't forget to shutdown tts!
	        if (textToSpeech != null) {
	        	textToSpeech.stop();
	        	textToSpeech.shutdown();
	        }
	        super.onDestroy();
	    }

	@Override
	public void onInit(int status) {
		// TODO Auto-generated method stub
		if (status == TextToSpeech.SUCCESS) {

            int result = textToSpeech.setLanguage(Locale.US);

            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "This Language is not supported");
            } else {
                read.setEnabled(true);
                speakOut();
            }

        } else {
            Log.e("TTS", "Initilization Failed!");
        }
	}

	private void speakOut() {
	String text = content.getText().toString();

       textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null);
    }
	 */

}
