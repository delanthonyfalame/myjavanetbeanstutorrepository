package com.example.machinelearning;

import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.example.machinelearning.Database.ChapterHandler;
import com.example.machinelearning.Database.Database;
import com.example.machinelearning.Database.LessonHandler;
import com.example.machinelearning.Utilities.GlobalVariables;

public class Objective extends SwitchFragmentParent implements GlobalVariables{
	protected static final String TAG = "Java";
	private View view;
	private int chapter;
	private TextView content;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.objective, container, false);
		db = new Database(getSherlockActivity());
		setWidgets();
		return view;
	}

	private void setWidgets() {
		Bundle bundle = this.getArguments();
		chapter = bundle.getInt("chapter");
		ChapterHandler item = db.getJavaChapter(chapter);
		content = (TextView) view.findViewById(R.id.content);
		content.setText(Html.fromHtml(item.getObjective()));

		TextView back = (TextView) view.findViewById(R.id.back);
		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Log.i(TAG, "back");
				switchFrag.goBack(new Java());
			}
		});


		//set click listener on quiz button
		TextView quiz = (TextView) view.findViewById(R.id.quiz2);
		//hide quiz

		quiz.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Log.i(TAG, "back");
				Bundle bundle = new Bundle();
				bundle.putInt( "chapter", chapter );
				switchLesson.switchToQuiz(chapter, new JavaQuiz());
			}
		});
	}

}
