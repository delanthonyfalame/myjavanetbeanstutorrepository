package com.example.machinelearning;

import java.util.ArrayList;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.example.machinelearning.Adapter.TutorialAdapter;
import com.example.machinelearning.Database.TutorialHandler;

public class Tutorials extends SwitchFragmentParent{
	protected static final String TAG = "tutorial";
	private View view;
	ArrayList<TutorialHandler> tutors = new ArrayList<TutorialHandler>();
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.tutorials, container, false);
		//tutors.add(new TutorialHandler(R.raw.lesson_one, "Using NetBeans and NetBeans SDK", "wwwasdfasf", "refreemce"));
		setWidgets();
		return view;
	}

	private void setWidgets() {
		ListView list = (ListView) view.findViewById(R.id.list);
		list.setAdapter(new TutorialAdapter(getActivity(), tutors));
		list.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				String url = "android.resource://" + getActivity().getPackageName() +"/"
						+tutors.get(position).getVideo(); 
				Log.i(TAG, "url : " + url);
				Intent i=new Intent(getActivity() , Video.class);
				i.putExtra("url", url);
				getActivity().startActivity(i);
//				switchFrag.playVideo(url);
			}
		});
		
		//switch to online tutorials
				view.findViewById(R.id.url).setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
//						switchFrag.switchFragment(new );
						Log.i(TAG, "link to More Java Tutorials");
						String url = "http://www.youtube.com/watch?v=yhjrTlzL4CY&list=PLh63eeG16JnOWxhqd89mvtblKzKmsxr_3";
						Intent i = new Intent(Intent.ACTION_VIEW);
						i.setData(Uri.parse(url));
						startActivity(i);
					}
				});
	}

}
