package com.example.machinelearning;

import java.util.ArrayList;
import java.util.Locale;

import android.app.Dialog;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.machinelearning.Database.Database;
import com.example.machinelearning.Database.LessonHandler;
import com.example.machinelearning.Database.QuizHandler;
import com.example.machinelearning.imagedownloader.ImageLoader;

public class NetBeansLesson extends SwitchFragmentParent{
// implements TextToSpeech.OnInitListener
	protected static final String TAG = "Java Lesson";
	int chapterNumber;
	int lessonNumber;
	int maxLesson = 0;
	LessonHandler lesson;
	// textToSpeech;
	//ImageView stop, read;
	TextView content;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.lesson, container, false);
		//textToSpeech = new TextToSpeech(getActivity(), this);
		//stop = (ImageView) view.findViewById(R.id.stop);
		//read = (ImageView) view.findViewById(R.id.read);
		
		  // button on click event
	   // stop.setOnClickListener(new View.OnClickListener() {
	 
	 //   @Override
	  // void onClick(View arg0) {
	  //.stop();
	    			
	//        }
	//    }); 		
		
		  // button on click event
     //   read.setOnClickListener(new View.OnClickListener() {
 
    //        @Override
     //       public void onClick(View arg0) {
     //();
      //      }
                      
      //  });
		
		imageLoad  = new ImageLoader(getActivity());
		Bundle bundle = this.getArguments();
		lessonNumber = bundle.getInt("lesson");
		chapterNumber = bundle.getInt("chapter");
	//	Log.i(TAG, "lesson number " +  lessonNumber + " chapter " + chapterNumber);
		db = new Database(getActivity());
		
		maxLesson = db.getMaxValueNetBeans(chapterNumber);
		lesson = db.getNetBeansIDandChapter(lessonNumber, chapterNumber);
		Log.i(TAG, " chapter "+ chapterNumber + " lesson " + lessonNumber);
		setWidgets();
		return view; 
	}

	private void setWidgets() {
		//initialize all variables
		TextView title = (TextView) view.findViewById(R.id.title);
		content = (TextView) view.findViewById(R.id.content);
		ImageView image = (ImageView) view.findViewById(R.id.image);
		image.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				showFullScreenImage(lesson.getImage());
			}
		});
		Log.i(TAG, "image : " + lesson.getImage());
		imageLoad.DisplayImage(lesson.getImage(), image, R.drawable.ic_launcher);
		//fill xml
		title.setText(lesson.getTitle());
		content.setText(Html.fromHtml(lesson.getContent()));
		
		//set click listener on back button
		TextView back = (TextView) view.findViewById(R.id.back);
		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Log.i(TAG, "back");
				switchFrag.goBack(new NetBeans());
			}
		});
		
		
		//set click listener on quiz button
		TextView quiz = (TextView) view.findViewById(R.id.quiz);
		//hide quiz
		quiz.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Log.i(TAG, "back");
				ArrayList<QuizHandler> quizArray = db.getNetBeansAllQuizByChapter(chapterNumber);
				if(quizArray != null)
					switchLesson.switchToQuiz(chapterNumber, new NetBeansQuiz());
				else
					Toast.makeText(getActivity(), "No quiz found in this chapter", Toast.LENGTH_SHORT).show();
			}
		});
		
		//set click listener on next button
		TextView next = (TextView) view.findViewById(R.id.next);
		next.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				int nextLesson  = db.getNetBeansNextLesson(lessonNumber, chapterNumber);
				Log.i(TAG, "next lesson : " + nextLesson + " previous " + lessonNumber + "chapter " + chapterNumber);
				LessonHandler lesson = db.getNetBeansIDandChapter(nextLesson, chapterNumber);
				if(lesson!=null)
					switchFrag.nextLesson(nextLesson, chapterNumber , new NetBeansLesson());
				else
					switchFrag.goBack(new NetBeansLesson());
			}
		});
		
		if(lessonNumber == maxLesson){
			quiz.setVisibility(View.VISIBLE);
			next.setVisibility(View.GONE);
		}else{
			quiz.setVisibility(View.GONE);
			next.setVisibility(View.VISIBLE);
		}
	
	}
	

	protected void showFullScreenImage(String string) {
		final Dialog dialog = new Dialog(getActivity());
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow().setBackgroundDrawableResource(android.R.color.white);
		dialog.setContentView(R.layout.image_view);
		ImageView image = (ImageView) dialog.findViewById(R.id.image);
		
		imageLoad.DisplayImage(string, image, R.drawable.ic_launcher);
		
		dialog.show();
	}
/*	
	 @Override
	    public void onDestroy() {
	        // Don't forget to shutdown tts!
	        if (textToSpeech != null) {
	        	textToSpeech.stop();
	        	textToSpeech.shutdown();
	        }
	        super.onDestroy();
	    }

	@Override
	public void onInit(int status) {
		// TODO Auto-generated method stub
		if (status == TextToSpeech.SUCCESS) {
			 
         int result = textToSpeech.setLanguage(Locale.US);

         if (result == TextToSpeech.LANG_MISSING_DATA
                 || result == TextToSpeech.LANG_NOT_SUPPORTED) {
             Log.e("TTS", "This Language is not supported");
         } else {
             read.setEnabled(true);
             speakOut();
         }

     } else {
         Log.e("TTS", "Initilization Failed!");
     }
	}
	
	private void speakOut() {
	String text = content.getText().toString();
		 
    textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null);
 }
*/
}