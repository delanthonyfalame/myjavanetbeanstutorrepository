package com.example.machinelearning.Adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.machinelearning.R;
import com.example.machinelearning.Database.Database;
import com.example.machinelearning.Database.MarkHandler;

@SuppressWarnings("unused")
public class MarkAdapter extends BaseAdapter{

	private Context activity;
	private ArrayList<MarkHandler> data;
	private static LayoutInflater inflater = null;
	View vi;
	Database db;
	private final static String TAG = "Deals Adapter";

	public MarkAdapter(Context context, ArrayList<MarkHandler> imageArry) {
		activity = context;
		data = imageArry;
		inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		db = new  Database(context);
	}
	public int getCount() {
		return data.size();
	}

	public Object getItem(int position) {
		return position;
	}

	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		vi = convertView;
		final int pos = position;
		vi = inflater.inflate(R.layout.list_item, null);
		//
		TextView title = (TextView)vi.findViewById(R.id.title); // title
		// Setting all values in listview
		int news = data.get(pos).getQuestion_id();
		title.setText("Question number : " + news);
		return vi;
	}


}
