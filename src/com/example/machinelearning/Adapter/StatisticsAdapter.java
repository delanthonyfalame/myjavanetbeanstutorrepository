package com.example.machinelearning.Adapter;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.machinelearning.R;
import com.example.machinelearning.Adapter.TutorialAdapter.ViewHolder;
import com.example.machinelearning.Database.Database;
import com.example.machinelearning.Database.StatisticsHandler;
import com.example.machinelearning.Utilities.GlobalVariables;

@SuppressLint("NewApi")
@SuppressWarnings("unused")
public class StatisticsAdapter extends BaseAdapter implements GlobalVariables{

	private Context activity;
	private ArrayList<StatisticsHandler> data;
	private static LayoutInflater inflater = null;
	View vi;
	Database db;
	private final static String TAG = "Statistics Adapter";
	ViewHolder holder;

	public StatisticsAdapter(Context context, ArrayList<StatisticsHandler> imageArry) {
		activity = context;
		data = imageArry;
		inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		db = new  Database(context);
	}
	public int getCount() {
		return data.size();
	}

	public Object getItem(int position) {
		return position;
	}

	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		vi = convertView;
		final int pos = position;
		StatisticsHandler stats = data.get(pos);
		vi = inflater.inflate(R.layout.statistics_item, null);

		ProgressBar progressBar = (ProgressBar) vi.findViewById(R.id.progress);
		TextView quiz = (TextView) vi.findViewById(R.id.quiz);
		TextView percent = (TextView) vi.findViewById(R.id.percent);
		TextView type = (TextView) vi.findViewById(R.id.type);
		TextView date = (TextView) vi.findViewById(R.id.date);
		TextView attemps= (TextView) vi.findViewById(R.id.attemp);
		TextView scores= (TextView) vi.findViewById(R.id.scores);
		
		StatisticsHandler latestStats = db.getLatestStates(stats.getBase_id());
		if(stats.getType() == JAVA){
			type.setText("Java Quiz");
			int count = db.getStatisticsCount(stats.getBase_id(), stats.getType());
			Log.i(TAG, " count : " + count);
			quiz.setText("Chapter : " + stats.getBase_id());
			scores.setText("Score : " + stats.getScore());
			percent.setText(String.valueOf(stats.getPercentage())+ "%");
			progressBar.setMax(100);
			progressBar.setProgress(stats.getPercentage());
			date.setText(latestStats.getDate_entry());
			attemps.setText("Attemps : " + count );
		}else if(stats.getType() == NETBEANS){
			type.setText("NetBeans Exercise");
			int count = db.getStatisticsCount(stats.getBase_id(), stats.getType());
			Log.i(TAG, " count : " + count);
			quiz.setText("Chapter : " + stats.getBase_id());
			scores.setText("Score : " + stats.getScore());
			percent.setText(String.valueOf(stats.getPercentage())+ "%");
			progressBar.setMax(100);
			progressBar.setProgress(stats.getPercentage());
			date.setText(latestStats.getDate_entry());
			attemps.setText("Attempts : " + count );
		}else{
			int count = db.getStatisticsCount(stats.getBase_id(), stats.getType());
			type.setText("Java Simulation Exercise");
			Log.i(TAG, " count : " + count);
			quiz.setText("Chapter : " + stats.getBase_id());
			scores.setText("Score : " + stats.getScore());
			percent.setText(String.valueOf(stats.getPercentage())+ "%");
			progressBar.setMax(100);
			progressBar.setProgress(stats.getPercentage());
			date.setText(latestStats.getDate_entry());
			attemps.setText("Attempt : " + count );
		}

		return vi;
	}

}
