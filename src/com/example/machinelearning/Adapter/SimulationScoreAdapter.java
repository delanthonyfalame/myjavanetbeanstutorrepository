package com.example.machinelearning.Adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.machinelearning.R;
import com.example.machinelearning.Database.Database;
import com.example.machinelearning.Database.DictionaryHandler;
import com.example.machinelearning.Database.MarkHandler;
import com.example.machinelearning.Database.QuizScoreHandler;
import com.example.machinelearning.Database.QuizTotal;
import com.example.machinelearning.Database.Quiz_resultHandler;
import com.example.machinelearning.Database.SimulationAnswerHandler;

@SuppressWarnings("unused")
public class SimulationScoreAdapter extends BaseAdapter{

	private Context activity;
	private ArrayList<SimulationAnswerHandler> data;
	private static LayoutInflater inflater = null;
	View vi;
	Database db;
	private final static String TAG = "Deals Adapter";

	public SimulationScoreAdapter(Context context, ArrayList<SimulationAnswerHandler> quizScore) {
		activity = context;
		data = quizScore;
		inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		db = new  Database(context);
	}
	public int getCount() {
		return data.size();
	}

	public Object getItem(int position) {
		return position;
	}

	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		vi = convertView;
		final int pos = position;
		SimulationAnswerHandler quiz = data.get(pos);
		vi = inflater.inflate(R.layout.simulation_list_item, null);
		//answer
		TextView title = (TextView)vi.findViewById(R.id.question_num);
		TextView question = (TextView)vi.findViewById(R.id.question); 
		TextView correct = (TextView) vi.findViewById(R.id.correct_answer);
		TextView answer = (TextView)vi.findViewById(R.id.answer); 
		TextView explanation = (TextView)vi.findViewById(R.id.explanation); 
		ImageView image = (ImageView) vi.findViewById(R.id.status);
		// Setting all values in listview
		title.setText(String.valueOf(quiz.getItemNumber()));
		question.setText((quiz.getSimulation().getQuestion()));
		answer.setText(quiz.getSimulation().getAnswerContainer());
		correct.setText(quiz.getSimulation().getAnswer());
		explanation.setText(quiz.getSimulation().getExplanation());
		if(quiz.isCorrect())
			image.setImageResource(R.drawable.success);
		return vi;
	}


}
