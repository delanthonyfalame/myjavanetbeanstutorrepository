package com.example.machinelearning.Adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnFocusChangeListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.machinelearning.R;
import com.example.machinelearning.Database.Simulation_quizHandler;
import com.example.machinelearning.imagedownloader.ImageLoader;

public class SimulatorAdapter extends BaseAdapter{

	private Context mContext;
	private ArrayList<Simulation_quizHandler> simulationData = new ArrayList<Simulation_quizHandler>();

	private ImageLoader imageLoader;

	public SimulatorAdapter(Context mContext, ArrayList<Simulation_quizHandler> simulationData) {
		this.mContext = mContext;
		this.simulationData = simulationData;
		imageLoader = new ImageLoader(mContext);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return simulationData.size();
	}

	@Override
	public Simulation_quizHandler getItem(int position) {
		return simulationData.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;

		
//		TextView simulationQuestion = (TextView) view.findViewById(R.id.simulation_question);
//		TextView ilo = (TextView) view.findViewById(R.id.ilo2);
//		final EditText answer = (EditText) view.findViewById(R.id.answer);
//		
//		simulationQuestion.setText(simulationData.getQuestion());
//		ilo.setText(simulationData.getIlo());
		if(convertView == null){
			// inflate the layout
			LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
			convertView = inflater.inflate(R.layout.simulation_item, parent, false);
			// well set up the ViewHolder
			viewHolder = new ViewHolder();
			viewHolder.simulationQuestion = (TextView) convertView.findViewById(R.id.simulation_question);
			viewHolder.ilo = (TextView) convertView.findViewById(R.id.ilo2);
			viewHolder.answer = (EditText) convertView.findViewById(R.id.answer);
			// store the holder with the view.
			convertView.setTag(viewHolder);

		}else{
			viewHolder = (ViewHolder) convertView.getTag();
		}

		// object item based on the position
		Simulation_quizHandler lessonItem = simulationData.get(position);
		viewHolder.simulationQuestion.setText(lessonItem.getQuestion());
		viewHolder.ilo.setText(lessonItem.getIlo());
		
		viewHolder.answer.setText(lessonItem.getAnswerContainer());
		viewHolder.answer.setId(position);
		viewHolder.answer.setOnFocusChangeListener(new OnFocusChangeListener() {
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus){
					final int position = v.getId();
					final EditText Caption = (EditText) v;
					simulationData.get(position).setAnswerContainer( Caption.getText().toString());
				}
			}
		});
		
		return convertView;
	}

	private static class ViewHolder{
		TextView simulationQuestion, ilo;
		EditText answer;
	}

	
	public ArrayList<Simulation_quizHandler> getSimulationData() {
		return simulationData;
	}
}
