package com.example.machinelearning.Adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.machinelearning.R;
import com.example.machinelearning.Database.Database;
import com.example.machinelearning.Database.DictionaryHandler;
import com.example.machinelearning.Database.MarkHandler;
import com.example.machinelearning.Database.QuizTotal;
import com.example.machinelearning.Database.Quiz_resultHandler;

@SuppressWarnings("unused")
public class QuizResultAdapter extends BaseAdapter{

	private Context activity;
	private ArrayList<QuizTotal> data;
	private static LayoutInflater inflater = null;
	View vi;
	Database db;
	private final static String TAG = "Deals Adapter";

	public QuizResultAdapter(Context context, ArrayList<QuizTotal> imageArry) {
		activity = context;
		data = imageArry;
		inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		db = new  Database(context);
	}
	public int getCount() {
		return data.size();
	}

	public Object getItem(int position) {
		return position;
	}

	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		vi = convertView;
		final int pos = position;
		vi = inflater.inflate(R.layout.list_item, null);
		//
		TextView title = (TextView)vi.findViewById(R.id.title); // title
		// Setting all values in listview
		int chapter = data.get(pos).getResult();
		title.setText("Quiz " + chapter);
		return vi;
	}


}
