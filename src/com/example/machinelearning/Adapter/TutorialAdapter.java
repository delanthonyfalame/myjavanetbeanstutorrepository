package com.example.machinelearning.Adapter;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.machinelearning.R;
import com.example.machinelearning.Database.Database;
import com.example.machinelearning.Database.TutorialHandler;

@SuppressLint("NewApi")
@SuppressWarnings("unused")
public class TutorialAdapter extends BaseAdapter{

	private Context activity;
	private ArrayList<TutorialHandler> data;
	private static LayoutInflater inflater = null;
	View vi;
	Database db;
	private final static String TAG = "Instructions Adapter";
	ViewHolder holder;

	public TutorialAdapter(Context context, ArrayList<TutorialHandler> imageArry) {
		activity = context;
		data = imageArry;
		inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		db = new  Database(context);
	}
	public int getCount() {
		return data.size();
	}

	public Object getItem(int position) {
		return position;
	}

	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		vi = convertView;
		final int pos = position;
		TutorialHandler tutorData = data.get(pos);
		if(convertView == null){
			vi = inflater.inflate(R.layout.image_view_item, null);
			/** Widgets **/
			holder = new ViewHolder();
			holder.video = (ImageView) vi.findViewById(R.id.video_view);
			vi.setTag(holder);
		}else
			holder = (ViewHolder) convertView.getTag();
		//
		final String url = "android.resource://" + activity.getPackageName() +"/"
				+tutorData.getVideo();
		TextView title = (TextView)vi.findViewById(R.id.title); // title
		Uri  videoURI = Uri.parse(url);
		MediaMetadataRetriever retriever = new MediaMetadataRetriever();
		retriever.setDataSource(activity.getApplicationContext(), videoURI);
		Bitmap bitmap = retriever
				.getFrameAtTime(100000,MediaMetadataRetriever.OPTION_PREVIOUS_SYNC);
		Drawable drawable = new BitmapDrawable(activity.getResources(), bitmap);
		holder.video.setImageDrawable(drawable);
		//	    holder.video.setVideoURI(Uri.parse("android.resource://" + activity.getPackageName() + "/" + R.raw.lessontwotwo));

		// Setting all values in listview
		String news = data.get(pos).getMessage();
		title.setText(news);
		return vi;
	}


	class ViewHolder{
		ImageView video;
	}
}
