package com.example.machinelearning.Adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.machinelearning.R;
import com.example.machinelearning.Database.Database;
import com.example.machinelearning.Database.DictionaryHandler;
import com.example.machinelearning.Database.MarkHandler;
import com.example.machinelearning.Database.QuizScoreHandler;
import com.example.machinelearning.Database.QuizTotal;
import com.example.machinelearning.Database.Quiz_resultHandler;

@SuppressWarnings("unused")
public class QuizScoreAdapter extends BaseAdapter{

	private Context activity;
	private ArrayList<QuizScoreHandler> data;
	private static LayoutInflater inflater = null;
	View vi;
	Database db;
	private final static String TAG = "Deals Adapter";

	public QuizScoreAdapter(Context context, ArrayList<QuizScoreHandler> quizScore) {
		activity = context;
		data = quizScore;
		inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		db = new  Database(context);
	}
	public int getCount() {
		return data.size();
	}

	public Object getItem(int position) {
		return position;
	}

	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		vi = convertView;
		final int pos = position;
		QuizScoreHandler quiz = data.get(pos);
		vi = inflater.inflate(R.layout.score_list_item, null);
		//answer
		TextView title = (TextView)vi.findViewById(R.id.question_num);
		TextView question = (TextView)vi.findViewById(R.id.question); 
		TextView correct = (TextView) vi.findViewById(R.id.correct_answer);
		TextView answer = (TextView)vi.findViewById(R.id.answer); 
		ImageView image = (ImageView) vi.findViewById(R.id.status);
		// Setting all values in listview
		title.setText(String.valueOf(quiz.getPosition()));
		question.setText((quiz.getQuestion().getQuestion()));
		answer.setText(quiz.getAnswer());
		correct.setText(quiz.getCorrectAnswer());
		if(quiz.isCorret())
			image.setImageResource(R.drawable.success);
		return vi;
	}


}
