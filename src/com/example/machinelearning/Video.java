package com.example.machinelearning;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.MediaController;
import android.widget.VideoView;

public class Video extends Activity{
	private static final String TAG = "PlayVideo";
	String videoURL;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		setContentView(R.layout.video_view);
		super.onCreate(savedInstanceState);
		Intent intent = getIntent();
		videoURL = intent.getStringExtra("url");
		setWidget();
		
	}
	
	private void setWidget() {
		
		Log.i(TAG, "url : " + videoURL);
		VideoView video = (VideoView) findViewById(R.id.video_view);
		Uri uri = Uri.parse(videoURL); //Declare your url here.
		video.setMediaController(new MediaController(Video.this));    
		video.setVideoURI(uri);
		video.requestFocus();
		video.start();
	}
}
