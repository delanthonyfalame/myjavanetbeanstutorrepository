package com.example.machinelearning;

import java.text.SimpleDateFormat;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.example.machinelearning.SwitchFragmentParent.SwitchFragment;
import com.example.machinelearning.SwitchFragmentParent.SwitchToLesson;
import com.example.machinelearning.Database.Database;
import com.example.machinelearning.Database.Simulation_quizHandler;
import com.example.machinelearning.Database.Simulation_quizHandler2;

/**
 * @author user
 *
 */
public class MainActivity extends SherlockFragmentActivity implements SwitchFragment, SwitchToLesson {

	private static final String TAG = "Main Fragment";
	Database db = new Database(this);
	/* (non-Javadoc)
	 * initialize all variables
	 * @see android.support.v4.app.FragmentActivity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		if(db.getSimulation_quizCount() <=0)
		addSimulation();

		if(savedInstanceState == null){
			// add quiz to database
			FragmentManager fm = getSupportFragmentManager();
			FragmentTransaction transaction = fm.beginTransaction();
			//replace fragments
			transaction.replace(R.id.fragment, new Loading()).addToBackStack(null);
			//confirm changes
			transaction.commit();
		}else
			switchFragment(new Dashboard());
	}


	@Override
	public void onBackPressed() {
		FragmentManager fm = getSupportFragmentManager();
		if(fm.getBackStackEntryCount() > 2)
			super.onBackPressed();
		else
			return;
	}


	/* (non-Javadoc)
	 * switch layout... implemented using interface.. in SwitchFragmentParent.java
	 * @see com.example.machinelearning.SwitchFragmentParent.SwitchFragment#switchFragment(android.support.v4.app.Fragment)
	 */
	@Override
	public void switchFragment(Fragment fragment) {
		String backStateName = fragment.getClass().getName();
		FragmentManager fm = getSupportFragmentManager();
		FragmentTransaction transaction = fm.beginTransaction();
		//replace fragments
		transaction.replace(R.id.fragment, fragment);
		//add to back stack
		transaction.addToBackStack(backStateName);
		//confirm changes
		transaction.commitAllowingStateLoss();
	}

	Bundle bundle;
	@Override
	public void switchToLesson(int chapter, Fragment fragment) {
		String backStateName = fragment.getClass().getName();
		FragmentManager fm = getSupportFragmentManager();

		FragmentTransaction transaction = fm.beginTransaction();
		//replace fragments
		transaction.replace(R.id.fragment, fragment);
		//add to back stack
		transaction.addToBackStack(backStateName);
		//confirm changes
		transaction.commit();
	}

	@Override
	public void switchToQuiz(int chapter, Fragment fragment) {
		String backStateName = fragment.getClass().getName();
		FragmentManager fm = getSupportFragmentManager();
		FragmentTransaction transaction = fm.beginTransaction();
		//replace fragmentss
		transaction.replace(R.id.fragment, fragment);
		// initialize bundle
		bundle = new Bundle();
		//add data
		bundle.putInt("chapter", chapter);
		//add data to fragment
		fragment.setArguments(bundle);
		//lesson
		transaction.addToBackStack(backStateName);
		//confirm changes
		transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
		transaction.commit();

	}


	@Override
	public void nextLesson(int lesson, int chapter, Fragment fragment) {
		Log.i(TAG, "lesson " + lesson);
		String backStateName = fragment.getClass().getName();
		FragmentManager fm = getSupportFragmentManager();
		FragmentTransaction transaction = fm.beginTransaction();
		// initialize bundle
		bundle = new Bundle();
		//add data
		bundle.putInt("lesson", lesson);
		bundle.putInt("chapter", chapter);
		//add data to fragment
		fragment.setArguments(bundle);
		//replace fragmentss
		transaction.replace(R.id.fragment, fragment);

		transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
		//lesson
		transaction.addToBackStack(backStateName);
		//confirm changes
		transaction.commit();
	}


	@Override
	public void goBack(Fragment fragment) {
		String backStateName = fragment.getClass().getName();
		getSupportFragmentManager().popBackStackImmediate(backStateName, FragmentManager.POP_BACK_STACK_INCLUSIVE);
		//		onBackPressed();
	}


	@Override
	public void switchSearch(int id, Fragment fragment) {
		String backStateName = fragment.getClass().getName();
		FragmentManager fm = getSupportFragmentManager();
		FragmentTransaction transaction = fm.beginTransaction();
		// initialize bundle
		bundle = new Bundle();
		//add data
		bundle.putInt("dictionary", id);
		//add data to fragment
		fragment.setArguments(bundle);
		//replace fragmentss
		transaction.replace(R.id.fragment, fragment);

		transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
		//lesson
		transaction.addToBackStack(backStateName);
		//confirm changes
		transaction.commit();
	}


	@Override
	public void switchInstruction(int instructionID, Fragment fragment) {
		String backStateName = fragment.getClass().getName();
		FragmentManager fm = getSupportFragmentManager();
		FragmentTransaction transaction = fm.beginTransaction();
		// initialize bundle
		bundle = new Bundle();
		//add data
		bundle.putInt("instruction", instructionID);
		//add data to fragment
		fragment.setArguments(bundle);
		//replace fragmentss
		transaction.replace(R.id.fragment, fragment);

		transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
		//lesson
		transaction.addToBackStack(backStateName);
		//confirm changes
		transaction.commit();

	}

	//	@Override
	//	public void playVideo(String location) {
	//		FragmentManager fm = getSupportFragmentManager();
	//		FragmentTransaction transaction = fm.beginTransaction();
	//		// initialize bundle
	//		bundle = new Bundle();
	//		//add data
	//		Log.i(TAG, "location " + location);
	//		bundle.putString("url", location);
	//		//set Fragment
	//		Fragment fragment = new Video();
	//		//add data to fragment
	//		fragment.setArguments(bundle);
	//		//replace fragmentss
	//		transaction.replace(R.id.fragment, fragment);
	//		String backStateName = fragment.getClass().getName();
	//		transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
	//		//lesson
	//		transaction.addToBackStack(backStateName);
	//		//confirm changes
	//		transaction.commit();
	//	}


	public String getTimeStomp() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return sdf.format(new java.util.Date());
	}


	@Override
	public void switchFragment(Fragment fragment, Bundle bundle) {
		String backStateName = fragment.getClass().getName();
		FragmentManager fm = getSupportFragmentManager();
		//set arguemtns to fragment
		fragment.setArguments(bundle);
		//begin transaction
		FragmentTransaction transaction = fm.beginTransaction();
		//replace fragments
		transaction.replace(R.id.fragment, fragment);
		//add to back stack
		transaction.addToBackStack(backStateName);
		//confirm changes
		transaction.commit();
	}

	
	private void addSimulation() {
		db.addSimulation_quiz(new Simulation_quizHandler(1, getTimeStomp(), "My\nJava\nNetBeans\nTutor\nFirstProgram", "What is the output of the program that is provided below:\n\npublic class MyFirstJavaProgram{\n\n\tpublic static void main(String []args){\n\t\tSystem.out.println(\"My\");\n\t\tSystem.out.println(\"Java\");\n\t\tSystem.out.println(\"NetBeans\");\n\t\tSystem.out.println(\"Tutor\");\n\t\tSystem.out.print(\"First\");\n\t\tSystem.out.print(\"Program\");\n\t}\n}","Java will first start the program by reading the public class MyFirstJavaProgram, this public class stands for the class name or the class definition, after reading the class name, Java will then proceed on reading the next line, the public static void main (String[] args) which is the main method, Java will first beging reading  public, which means that everyone can access the class, next is the static, the static is used for creating an instance of the class, next is the void which is a return type of this method and means that there is no return value, the next is the main(), it simply stands for the name of the method,and lastly the String args[], the String[] args says that main() has a parameter which is an array of String references. After rading the main method, Java will proceed on reading the System.out.println() and the System.out.print() statements, these statements are the basic output statements in Java, and these statements will display the values that are passed or within the println() and print(), and in the case of our program the values are the following, \"My\", \"Java\", \"NetBeans\", \"Tutor\", \"First\" and \"Program\".",""));
		db.addSimulation_quiz(new Simulation_quizHandler(1, getTimeStomp(), "Basic Output statement", "What is the output of the program provided below:\n\npublic class Main\n{\n\tpublic static void main(String[] args)\n\t\t{\n\t\t\t   System.out.print(''Basic Output statement'');\n\t\t}\n}","None",""));
		db.addSimulation_quiz(new Simulation_quizHandler(1, getTimeStomp(), "Print a simple text", "What is the output of the program provided below:\n\npublic class Main\n\n{\n\tpublic static void main(String[] args)\n\t{\n\t\t System.out.print(''Print a simple text'');\n\t}","None",""));
		db.addSimulation_quiz(new Simulation_quizHandler(1, getTimeStomp(), "Enter a number and I will echo it later: 88\nEnter a word and I will echo it later:hello\nHere is the echo of what you have entered:\nThe number you have entered is 88\nThe word you have entered is hello", "What is the output of the program provided below:\n\npublic class Main\n{\n\t\tpublic static void main(String[] args)\n\t\t{\n\n\t\t\tScanner input = new Scanner(System.in);\n\t\t\tint num1;\n\t\t\tString word1;\n\t\t\tSystem.out.print(\"Enter a number and I will echo it later: \");\n\t\t\tnum1=input.nextInt();\n\t\t\tSystem.out.print(\"Enter a word and I will echo it later: \");\n\t\t\t word1 = input.next();\n\t\t\t System.out.println(\"Here is the echo of what you have entered:\");\n\t\t\tSystem.out.println(''The number you have entered is: '' + num1);\n\t\t\t System.out.println(''The word you have entered is: '' + word1); \n\t\t }\n}\n","None",""));
//		db.addSimulation_quiz(new Simulation_quizHandler(1, getTimeStomp(), "My\nJava\nNetBeans\nTutor\nFirstProgram", "What is the output of the program that is provided below:\n\npublic class MyFirstJavaProgram{\n\n\tpublic static void main(String []args){\n\t\tSystem.out.println(\"My\");\n\t\tSystem.out.println(\"Java\");\n\t\tSystem.out.println(\"NetBeans\");\n\t\tSystem.out.println(\"Tutor\");\n\t\tSystem.out.print(\"First\");\n\t\tSystem.out.print(\"Program\");\n\t}\n}","Java will first start the program by reading the public class MyFirstJavaProgram, this public class stands for the class name or the class definition, after reading the class name, Java will then proceed on reading the next line, the public static void main (String[] args) which is the main method, Java will first beging reading  public, which means that everyone can access the class, next is the static, the static is used for creating an instance of the class, next is the void which is a return type of this method and means that there is no return value, the next is the main(), it simply stands for the name of the method,and lastly the String args[], the String[] args says that main() has a parameter which is an array of String references. After rading the main method, Java will proceed on reading the System.out.println() and the System.out.print() statements, these statements are the basic output statements in Java, and these statements will display the values that are passed or within the println() and print(), and in the case of our program the values are the following, \"My\", \"Java\", \"NetBeans\", \"Tutor\", \"First\" and \"Program\".","Simulate the programming process of Java."));
		db.addSimulation_quiz(new Simulation_quizHandler(2, getTimeStomp(), "abcghidef", "Identify the output of the program below:\n\npublic class StringRef\n{\n\tpublic static void main(String [] args)\n\t{\n\t\tString s1 = \"abc\";\n\t\tString s2 = \"def\";\n\t\tString s3 = s2;   /* Line 7 */\n\t\ts2 = \"ghi\";\t\tSystem.out.println(s1 + s2 + s3);\n\t}\n}","After line 7 executes, both s2 and s3 refer to a String object that contains the value \"def\". When line 8 executes, a new String object is created with the value \"ghi\" to which s2 refers. The reference variable s3 still refers to the (immutable) String object with the value \"def\".",""));
		db.addSimulation_quiz(new Simulation_quizHandler(2, getTimeStomp(), "i = 10\nj = 8", "What would be the output of the program?\npublic class ArithmeticAssignmentOperatorExample {\n\t public static void main(String[] args) {\n\n\t\t int i = 5;\n\t\t int j = 10;\n\t\t i += 5; //same as i = i + 5\n\t\t j -= 2; //same as j = j - 2\n\t\t  System.out.println(\"i = \" + i);\n\t\t  System.out.println(\"j = \" + j);\n\t   }\n}","None",""));
		db.addSimulation_quiz(new Simulation_quizHandler(2, getTimeStomp(), "Arithmetic operators example :\ni = 70\nj = 60\nk = 120\nl = 20.0\n", "what would be the ouput of the program?\n\npublic class ArithmeticOperatorsExample {\n\tpublic static void main(String[] args) {\n \t\t\tSystem.out.println(\"Arithmetic operators example :\");\n\t\t\tint i = 50 + 20;\n\t\t\tint j = i - 10;\n\t\t\tint k = j * 2;\n\t\t\tdouble l = k / 6;\n\t\t\tSystem.out.println(\"i = \" + i);\n\t\t\tSystem.out.println(\"j = \" + j);\n\t\t\tSystem.out.println(\"k = \" + k);\n\t\t\tSystem.out.println(\"l = \" + l);\n\t }\n}","None",""));
		db.addSimulation_quiz(new Simulation_quizHandler(2, getTimeStomp(), "i = 11\nj = 11\nk = 11\nl = 12", "what would be the ouput of the program?\n\npublic class IncrementDecrementOperatorExample {\n\t public static void main(String[] args) {\n\t\tint i = 10;\n\t\tint j = 10;\n\t\ti++;\n\t\tj++;\n\t\tSystem.out.println(\"i = \" + i);\n\t\tSystem.out.println(\"j = \" + j);\n\t\t int k = i++;\n\n\t\tint l = ++j;\n\n\t\tSystem.out.println(\"k = \" + k);\n\t\t System.out.println(\"l = \" + l);\n\n\t}\n}","None",""));
//		db.addSimulation_quiz(new Simulation_quizHandler(2, getTimeStomp(), "answer", "question","expalantion","ilo"));
		db.addSimulation_quiz(new Simulation_quizHandler(3, getTimeStomp(), "tiny", "What will be the output of the program?\n\nclass Test\n{\n\tpublic static void main(String [] args)\n\t{\n\t\tint x=20;\n\t\tString sup = (x < 15) ? \"small\" : (x < 22)? \"tiny\" : \"huge\";\n\t\tSystem.out.println(sup);\n\t}\n}","This is a nested ternary operator. The second evaluation (x < 22) is true, so the \"tiny\" value is assigned to sup.",""));
		db.addSimulation_quiz(new Simulation_quizHandler(3, getTimeStomp(), "7 14", "What will be the output of the program?\n\nclass Test \n{\n\tstatic int s;\n\tpublic static void main(String [] args) \n\t{\n\t\t    Test p = new Test();\n\t\t    p.start();\n\t\t   System.out.println(s);\n\t}\n\n\t void start() \n\t {\n\t\t    int x = 7;\n\t\t    twice(x);\n\t\t   System.out.print(x + '' '');\n\t  }\n\n\t void twice(int x) \n\t{\n\t\t   x = x*2;\n\t\t   s = x;\n\t }\n}","None",""));
		db.addSimulation_quiz(new Simulation_quizHandler(3, getTimeStomp(), "4", "what would be the output of the program?\n\npublic class Test \n{ \n\tpublic static void leftshift(int i, int j) \n  \t{\n\t\t    i <<= j; \n\t} \n\tpublic static void main(String args[]) \n\t{\n\t\t    int i = 4, j = 2; \n\t\t   leftshift(i, j); \n\t\t    System.out.printIn(i); \n\t} \n}","None",""));
		db.addSimulation_quiz(new Simulation_quizHandler(3, getTimeStomp(), "6 3", "What would be the output of the program?\n\nclass Test \n{\n\tpublic static void main(String [] args) \n\t{\n\t\t    int x= 0;\n\t\tint y= 0;\n\t\tfor (int z = 0; z < 5; z++) \n\t{\n\t\tif (( ++x > 2 ) && (++y > 2)) \n\t       {\n\t\tx++;\n\t}\n\t}\n\t\tSystem.out.println(x + '' '' + y);\n\t}\n}","In the first two iterations x is incremented once and y is not because of the short circuit && operator. In the third and forth iterations x and y are each incremented, and in the fifth iteration x is doubly incremented and y is incremented.",""));
//		db.addSimulation_quiz(new Simulation_quizHandler(2, getTimeStomp(), "answer", "question","expalantion","ilo"));
		db.addSimulation_quiz(new Simulation_quizHandler(4, getTimeStomp(), "42", "Provided below is a sample program using objects\nWhat will be the output of the program?\npublic static void main(String[] args)\n{\n\tObject obj = new Object()\n\t{\n\t\tpublic int hashCode()\n\t\t{\n\t\t\treturn 42;\n\t\t}\n\t};\n\tSystem.out.println(obj.hashCode());\n}","This code is an example of an anonymous inner class. They can be declared to extend another class or implement a single interface. Since they have no name you can not use the /''new'' keyword on them./n/nIn this case the annoynous class is extending the Object class. Within the {} you place the methods you want for that class. After this class has been declared its methods can be used by that object in the usual way e.g. objectname.annoymousClassMethod()",""));
		db.addSimulation_quiz(new Simulation_quizHandler(4, getTimeStomp(), "four one three two", "What will be the output of the program?\n\n\tTreeSet map = new TreeSet();\n\tmap.add(\"one\");\n\tmap.add(\"two\");\n\tmap.add(\"three\");\n\tmap.add(\"four\");\n\tmap.add(\"one\");\n\tIterator it = map.iterator();\n\twhile (it.hasNext() ) \n\t{\n\t  \t\t System.out.print( it.next() + '' '' );\n\t}\n","TreeSet assures no duplicate entries; also, when it is accessed it will return elements in natural order, which typically means alphabetical",""));
		db.addSimulation_quiz(new Simulation_quizHandler(4, getTimeStomp(), "Zippo", "What will be the output of the program?\n\npublic class HorseTest \n\n{\n\tpublic static void main (String [] args) \n\t{\n\t\tclass Horse \n\t\t{\n\t\t\tpublic String name; /* Line 7 */\n\t\t\tpublic Horse(String s) \n\t\t       {\n\t\t\tname = s;\n\t\t}\n\t\t} /* class Horse ends */\n\n\t\tObject obj = new Horse(\"Zippo\"); /* Line 13 */\n\t\tHorse h = (Horse) obj; /* Line 14 */\n\t\t   System.out.println(h.name);\n\t\t}\n} /* class HorseTest ends */","The code in the HorseTest class is perfectly legal. Line 13 creates an instance of the method-local inner class Horse, using a reference variable declared as type Object. Line 14 casts the Horse object to a Horse reference variable, which allows line 15 to compile. If line 14 were removed, the HorseTest code would not compile, because class Object does not have a name variable.",""));
		db.addSimulation_quiz(new Simulation_quizHandler(4, getTimeStomp(), "57 22", "What will be the output of the program?\n\npublic abstract class AbstractTest \n{\n\tpublic int getNum() \n\t{\n    \t\treturn 45;\n\t}\n\tpublic abstract class Bar \n\t{\n\t\tpublic int getNum() \n\t\t{\n\t\t\treturn 38;\n\t\t}\n\t}\n\t public static void main (String [] args) \n\t {\n\t\tAbstractTest t = new AbstractTest() \n\t\t{\n\t\t\tpublic int getNum() \n\t\t{\n\t\t\treturn 22;\n\t\t}\n\t};\nAbstractTest.Bar f = t.new Bar() \n\t\t    {\n\tpublic int getNum() \n\t\t{\n\t\t \treturn 57;\n\t\t}\n\t\t};\n\n\t\t\t   System.out.println(f.getNum() + '' '' + t.getNum());\n\t\t}\n}\n","You can define an inner class as abstract, which means you can instantiate only concrete subclasses of the abstract inner class. The object referenced by the variable t is an instance of an anonymous subclass of AbstractTest, and the anonymous class overrides the getNum() method to return 22. The variable referenced by f is an instance of an anonymous subclass of Bar, and the anonymous Bar subclass also overrides the getNum() method (to return 57). Remember that to instantiate a Bar instance, we need an instance of the enclosing AbstractTest class to tie to the new Bar inner class instance. AbstractTest can't be instantiated because it's abstract, so we created an anonymous subclass (non-abstract) and then used the instance of that anonymous subclass to tie to the new Bar subclass instance.",""));
//		db.addSimulation_quiz(new Simulation_quizHandler(2, getTimeStomp(), "answer", "question","expalantion","ilo"));
		db.addSimulation_quiz(new Simulation_quizHandler(5, getTimeStomp(), "101", "This is a sample program using an if-else-if statement\nWhat will be the output of the program?\n\npublic class If2\n{\n\tstatic boolean b1, b2;\n\tpublic static void main(String [] args)\n\t{\n\t\tint x = 0;\n\t\tif ( !b1 ) /* Line 7 */\n\t\t{\n\t\t\tif ( !b2 ) /* Line 9 */\n\t\t\t{\n\t\t\t\tb1 = true;\n\t\t\t\tx++;\n\t\t\t\tif ( 5 > 6 )\n\t\t\t\t{\n\t\t\t\tx++;\n\t\t\t\t}\n\t\t\t\tif ( !b1 )\n\t\t\t\tx = x + 10;\n\t\t\t\telse if ( b2 = true ) /* Line 19 */\n\t\t\t\tx = x + 100;\n\t\t\t\telse if ( b1 | b2 ) /* Line 21 */\n\t\t\t\tx = x + 1000;\n\t\t\t}\n\t\t}\n\t\tSystem.out.println(x);\n\t}\n}","As instance variables, b1 and b2 are initialized to false. The if tests on lines 7 and 9 are successful so b1 is set to true and x is incremented. The next if test to succeed is on line 19 (note that the code is not testing to see if b2 is true, it is setting b2 to be true). Since line 19 was successful, subsequent else-ifs (line 21) will be skipped.",""));
		db.addSimulation_quiz(new Simulation_quizHandler(5, getTimeStomp(), "i = 5 and j = 6", "What will be the output of the program?\n\nint i = 1, j = 10; \ndo \n{\n\tif(i > j) \n\t{\n\t\t    break; \n\t} \n   \t j--; \n} while (++i < 5); \nSystem.out.println(\"i = \" + i + \" and j = \" + j);","This loop is a do-while loop, which always executes the code block within the block at least once, due to the testing condition being at the end of the loop, rather than at the beginning. This particular loop is exited prematurely if i becomes greater than j.\n\nThe order is, test i against j, if bigger, it breaks from the loop, decrements j by one, and then tests the loop condition, where a pre-incremented by one i is tested for being lower than 5. The test is at the end of the loop, so i can reach the value of 5 before it fails. So it goes, start:\n1, 10\n2, 9\n3, 8\n4, 7\n5, 6 loop condition fails.",""));
		db.addSimulation_quiz(new Simulation_quizHandler(5, getTimeStomp(), "2 1 2 0 1 2", "What will be the output of the program?\n\npublic class Switch2 \n{\n\t final static short x = 2;\n\t  public static int y = 0;\n\t public static void main(String [] args)\n \t {\n\t\tfor (int z=0; z < 3; z++) \n\t\t{\n\t\t\tswitch (z) \n\t\t{\n\t\t\tcase x: System.out.print(\"0 \");\n\t\t\tcase x-1: System.out.print(\"1 \");\n\t\t\tcase x-2: System.out.print(\"2 \");\n\t\t}\n\t\t}\n\t}\n}","The case expressions are all legal because x is marked final, which means the expressions can be evaluated at compile time. In the first iteration of the for loop case x-2 matches, so 2 is printed. In the second iteration, x-1 is matched so 1 and 2 are printed (remember, once a match is found all remaining statements are executed until a break statement is encountered). In the third iteration, x is matched. So 0 1 and 2 are printed.",""));
		db.addSimulation_quiz(new Simulation_quizHandler(5, getTimeStomp(), "value = 8", "What will be the output of the program?\n\npublic class SwitchTest \n{\n\tpublic static void main(String[] args) \n\t {\n\t\tSystem.out.println(\"value =\" + switchIt(4)); \n\t}\n\t public static int switchIt(int x) \n\t{\n\t\tint j = 1;  \n\t\tswitch (x) \n\t\t{ \n\t\t\tcase l: j++; \n\t\t\tcase 2: j++;  \n\t\t\tcase 3: j++; \n\t\t\tcase 4: j++; \n\t\t\tcase 5: j++; \n\t\t\tdefault: j++; \n\t\t\t} \n\t\treturn j + x;  \n\t } \n}","Because there are no break statements, once the desired result is found, the program continues though each of the remaining options.",""));
//		db.addSimulation_quiz(new Simulation_quizHandler(2, getTimeStomp(), "answer", "question","expalantion","ilo"));
//		db.addSimulation_quiz(new Simulation_quizHandler(2, getTimeStomp(), "answer", "question","expalantion","ilo"));
		db.addSimulation_quiz(new Simulation_quizHandler(6, getTimeStomp(), "101", "This is a sample program using an if-else-if statement\nWhat will be the output of the program?\n\npublic class If2\n{\n\tstatic boolean b1, b2;\n\tpublic static void main(String [] args)\n\t{\n\t\tint x = 0;\n\t\tif ( !b1 ) /* Line 7 */\n\t\t{\n\t\t\tif ( !b2 ) /* Line 9 */\n\t\t\t{\n\t\t\t\tb1 = true;\n\t\t\t\tx++;\n\t\t\t\tif ( 5 > 6 )\n\t\t\t\t{\n\t\t\t\tx++;\n\t\t\t\t}\n\t\t\t\tif ( !b1 )\n\t\t\t\tx = x + 10;\n\t\t\t\telse if ( b2 = true ) /* Line 19 */\n\t\t\t\tx = x + 100;\n\t\t\t\telse if ( b1 | b2 ) /* Line 21 */\n\t\t\t\tx = x + 1000;\n\t\t\t}\n\t\t}\n\t\tSystem.out.println(x);\n\t}\n}","As instance variables, b1 and b2 are initialized to false. The if tests on lines 7 and 9 are successful so b1 is set to true and x is incremented. The next if test to succeed is on line 19 (note that the code is not testing to see if b2 is true, it is setting b2 to be true). Since line 19 was successful, subsequent else-ifs (line 21) will be skipped.",""));
		db.addSimulation_quiz(new Simulation_quizHandler(6, getTimeStomp(), "i = 5 and j = 6", "What will be the output of the program?\n\nint i = 1, j = 10; \ndo \n{\n\tif(i > j) \n\t{\n\t\t    break; \n\t} \n   \t j--; \n} while (++i < 5); \nSystem.out.println(\"i = \" + i + \" and j = \" + j);","This loop is a do-while loop, which always executes the code block within the block at least once, due to the testing condition being at the end of the loop, rather than at the beginning. This particular loop is exited prematurely if i becomes greater than j.\n\nThe order is, test i against j, if bigger, it breaks from the loop, decrements j by one, and then tests the loop condition, where a pre-incremented by one i is tested for being lower than 5. The test is at the end of the loop, so i can reach the value of 5 before it fails. So it goes, start:\n1, 10\n2, 9\n3, 8\n4, 7\n5, 6 loop condition fails.",""));
		db.addSimulation_quiz(new Simulation_quizHandler(6, getTimeStomp(), "2 1 2 0 1 2", "What will be the output of the program?\n\npublic class Switch2 \n{\n\t final static short x = 2;\n\t  public static int y = 0;\n\t public static void main(String [] args)\n \t {\n\t\tfor (int z=0; z < 3; z++) \n\t\t{\n\t\t\tswitch (z) \n\t\t{\n\t\t\tcase x: System.out.print(\"0 \");\n\t\t\tcase x-1: System.out.print(\"1 \");\n\t\t\tcase x-2: System.out.print(\"2 \");\n\t\t}\n\t\t}\n\t}\n}","The case expressions are all legal because x is marked final, which means the expressions can be evaluated at compile time. In the first iteration of the for loop case x-2 matches, so 2 is printed. In the second iteration, x-1 is matched so 1 and 2 are printed (remember, once a match is found all remaining statements are executed until a break statement is encountered). In the third iteration, x is matched. So 0 1 and 2 are printed.",""));
		db.addSimulation_quiz(new Simulation_quizHandler(6, getTimeStomp(), "value = 8", "What will be the output of the program?\n\npublic class SwitchTest \n{\n\tpublic static void main(String[] args) \n\t {\n\t\tSystem.out.println(\"value =\" + switchIt(4)); \n\t}\n\t public static int switchIt(int x) \n\t{\n\t\tint j = 1;  \n\t\tswitch (x) \n\t\t{ \n\t\t\tcase l: j++; \n\t\t\tcase 2: j++;  \n\t\t\tcase 3: j++; \n\t\t\tcase 4: j++; \n\t\t\tcase 5: j++; \n\t\t\tdefault: j++; \n\t\t\t} \n\t\treturn j + x;  \n\t } \n}","Because there are no break statements, once the desired result is found, the program continues though each of the remaining options.",""));
//		db.addSimulation_quiz(new Simulation_quizHandler(2, getTimeStomp(), "answer", "question","expalantion","ilo"));
		db.addSimulation_quiz(new Simulation_quizHandler(7, getTimeStomp(), "101", "This is a sample program using an if-else-if statement\nWhat will be the output of the program?\n\npublic class If2\n{\n\tstatic boolean b1, b2;\n\tpublic static void main(String [] args)\n\t{\n\t\tint x = 0;\n\t\tif ( !b1 ) /* Line 7 */\n\t\t{\n\t\t\tif ( !b2 ) /* Line 9 */\n\t\t\t{\n\t\t\t\tb1 = true;\n\t\t\t\tx++;\n\t\t\t\tif ( 5 > 6 )\n\t\t\t\t{\n\t\t\t\tx++;\n\t\t\t\t}\n\t\t\t\tif ( !b1 )\n\t\t\t\tx = x + 10;\n\t\t\t\telse if ( b2 = true ) /* Line 19 */\n\t\t\t\tx = x + 100;\n\t\t\t\telse if ( b1 | b2 ) /* Line 21 */\n\t\t\t\tx = x + 1000;\n\t\t\t}\n\t\t}\n\t\tSystem.out.println(x);\n\t}\n}","As instance variables, b1 and b2 are initialized to false. The if tests on lines 7 and 9 are successful so b1 is set to true and x is incremented. The next if test to succeed is on line 19 (note that the code is not testing to see if b2 is true, it is setting b2 to be true). Since line 19 was successful, subsequent else-ifs (line 21) will be skipped.",""));
		db.addSimulation_quiz(new Simulation_quizHandler(7, getTimeStomp(), "i = 5 and j = 6", "What will be the output of the program?\n\nint i = 1, j = 10; \ndo \n{\n\tif(i > j) \n\t{\n\t\t    break; \n\t} \n   \t j--; \n} while (++i < 5); \nSystem.out.println(\"i = \" + i + \" and j = \" + j);","This loop is a do-while loop, which always executes the code block within the block at least once, due to the testing condition being at the end of the loop, rather than at the beginning. This particular loop is exited prematurely if i becomes greater than j.\n\nThe order is, test i against j, if bigger, it breaks from the loop, decrements j by one, and then tests the loop condition, where a pre-incremented by one i is tested for being lower than 5. The test is at the end of the loop, so i can reach the value of 5 before it fails. So it goes, start:\n1, 10\n2, 9\n3, 8\n4, 7\n5, 6 loop condition fails.",""));
		db.addSimulation_quiz(new Simulation_quizHandler(7, getTimeStomp(), "2 1 2 0 1 2", "What will be the output of the program?\n\npublic class Switch2 \n{\n\t final static short x = 2;\n\t  public static int y = 0;\n\t public static void main(String [] args)\n \t {\n\t\tfor (int z=0; z < 3; z++) \n\t\t{\n\t\t\tswitch (z) \n\t\t{\n\t\t\tcase x: System.out.print(\"0 \");\n\t\t\tcase x-1: System.out.print(\"1 \");\n\t\t\tcase x-2: System.out.print(\"2 \");\n\t\t}\n\t\t}\n\t}\n}","The case expressions are all legal because x is marked final, which means the expressions can be evaluated at compile time. In the first iteration of the for loop case x-2 matches, so 2 is printed. In the second iteration, x-1 is matched so 1 and 2 are printed (remember, once a match is found all remaining statements are executed until a break statement is encountered). In the third iteration, x is matched. So 0 1 and 2 are printed.",""));
		db.addSimulation_quiz(new Simulation_quizHandler(7, getTimeStomp(), "value = 8", "What will be the output of the program?\n\npublic class SwitchTest \n{\n\tpublic static void main(String[] args) \n\t {\n\t\tSystem.out.println(\"value =\" + switchIt(4)); \n\t}\n\t public static int switchIt(int x) \n\t{\n\t\tint j = 1;  \n\t\tswitch (x) \n\t\t{ \n\t\t\tcase l: j++; \n\t\t\tcase 2: j++;  \n\t\t\tcase 3: j++; \n\t\t\tcase 4: j++; \n\t\t\tcase 5: j++; \n\t\t\tdefault: j++; \n\t\t\t} \n\t\treturn j + x;  \n\t } \n}","Because there are no break statements, once the desired result is found, the program continues though each of the remaining options.",""));
//		db.addSimulation_quiz(new Simulation_quizHandler(2, getTimeStomp(), "answer", "question","expalantion","ilo"));
		db.addSimulation_quiz(new Simulation_quizHandler(8, getTimeStomp(), "101", "This is a sample program using an if-else-if statement\nWhat will be the output of the program?\n\npublic class If2\n{\n\tstatic boolean b1, b2;\n\tpublic static void main(String [] args)\n\t{\n\t\tint x = 0;\n\t\tif ( !b1 ) /* Line 7 */\n\t\t{\n\t\t\tif ( !b2 ) /* Line 9 */\n\t\t\t{\n\t\t\t\tb1 = true;\n\t\t\t\tx++;\n\t\t\t\tif ( 5 > 6 )\n\t\t\t\t{\n\t\t\t\tx++;\n\t\t\t\t}\n\t\t\t\tif ( !b1 )\n\t\t\t\tx = x + 10;\n\t\t\t\telse if ( b2 = true ) /* Line 19 */\n\t\t\t\tx = x + 100;\n\t\t\t\telse if ( b1 | b2 ) /* Line 21 */\n\t\t\t\tx = x + 1000;\n\t\t\t}\n\t\t}\n\t\tSystem.out.println(x);\n\t}\n}","As instance variables, b1 and b2 are initialized to false. The if tests on lines 7 and 9 are successful so b1 is set to true and x is incremented. The next if test to succeed is on line 19 (note that the code is not testing to see if b2 is true, it is setting b2 to be true). Since line 19 was successful, subsequent else-ifs (line 21) will be skipped.",""));
		db.addSimulation_quiz(new Simulation_quizHandler(8, getTimeStomp(), "i = 5 and j = 6", "What will be the output of the program?\n\nint i = 1, j = 10; \ndo \n{\n\tif(i > j) \n\t{\n\t\t    break; \n\t} \n   \t j--; \n} while (++i < 5); \nSystem.out.println(\"i = \" + i + \" and j = \" + j);","This loop is a do-while loop, which always executes the code block within the block at least once, due to the testing condition being at the end of the loop, rather than at the beginning. This particular loop is exited prematurely if i becomes greater than j.\n\nThe order is, test i against j, if bigger, it breaks from the loop, decrements j by one, and then tests the loop condition, where a pre-incremented by one i is tested for being lower than 5. The test is at the end of the loop, so i can reach the value of 5 before it fails. So it goes, start:\n1, 10\n2, 9\n3, 8\n4, 7\n5, 6 loop condition fails.",""));
		db.addSimulation_quiz(new Simulation_quizHandler(8, getTimeStomp(), "2 1 2 0 1 2", "What will be the output of the program?\n\npublic class Switch2 \n{\n\t final static short x = 2;\n\t  public static int y = 0;\n\t public static void main(String [] args)\n \t {\n\t\tfor (int z=0; z < 3; z++) \n\t\t{\n\t\t\tswitch (z) \n\t\t{\n\t\t\tcase x: System.out.print(\"0 \");\n\t\t\tcase x-1: System.out.print(\"1 \");\n\t\t\tcase x-2: System.out.print(\"2 \");\n\t\t}\n\t\t}\n\t}\n}","The case expressions are all legal because x is marked final, which means the expressions can be evaluated at compile time. In the first iteration of the for loop case x-2 matches, so 2 is printed. In the second iteration, x-1 is matched so 1 and 2 are printed (remember, once a match is found all remaining statements are executed until a break statement is encountered). In the third iteration, x is matched. So 0 1 and 2 are printed.",""));
		db.addSimulation_quiz(new Simulation_quizHandler(8, getTimeStomp(), "value = 8", "What will be the output of the program?\n\npublic class SwitchTest \n{\n\tpublic static void main(String[] args) \n\t {\n\t\tSystem.out.println(\"value =\" + switchIt(4)); \n\t}\n\t public static int switchIt(int x) \n\t{\n\t\tint j = 1;  \n\t\tswitch (x) \n\t\t{ \n\t\t\tcase l: j++; \n\t\t\tcase 2: j++;  \n\t\t\tcase 3: j++; \n\t\t\tcase 4: j++; \n\t\t\tcase 5: j++; \n\t\t\tdefault: j++; \n\t\t\t} \n\t\treturn j + x;  \n\t } \n}","Because there are no break statements, once the desired result is found, the program continues though each of the remaining options.",""));
//		db.addSimulation_quiz(new Simulation_quizHandler(2, getTimeStomp(), "answer", "question","expalantion","ilo"));
		db.addSimulation_quiz(new Simulation_quizHandler(9, getTimeStomp(), "101", "This is a sample program using an if-else-if statement\nWhat will be the output of the program?\n\npublic class If2\n{\n\tstatic boolean b1, b2;\n\tpublic static void main(String [] args)\n\t{\n\t\tint x = 0;\n\t\tif ( !b1 ) /* Line 7 */\n\t\t{\n\t\t\tif ( !b2 ) /* Line 9 */\n\t\t\t{\n\t\t\t\tb1 = true;\n\t\t\t\tx++;\n\t\t\t\tif ( 5 > 6 )\n\t\t\t\t{\n\t\t\t\tx++;\n\t\t\t\t}\n\t\t\t\tif ( !b1 )\n\t\t\t\tx = x + 10;\n\t\t\t\telse if ( b2 = true ) /* Line 19 */\n\t\t\t\tx = x + 100;\n\t\t\t\telse if ( b1 | b2 ) /* Line 21 */\n\t\t\t\tx = x + 1000;\n\t\t\t}\n\t\t}\n\t\tSystem.out.println(x);\n\t}\n}","As instance variables, b1 and b2 are initialized to false. The if tests on lines 7 and 9 are successful so b1 is set to true and x is incremented. The next if test to succeed is on line 19 (note that the code is not testing to see if b2 is true, it is setting b2 to be true). Since line 19 was successful, subsequent else-ifs (line 21) will be skipped.",""));
		db.addSimulation_quiz(new Simulation_quizHandler(9, getTimeStomp(), "i = 5 and j = 6", "What will be the output of the program?\n\nint i = 1, j = 10; \ndo \n{\n\tif(i > j) \n\t{\n\t\t    break; \n\t} \n   \t j--; \n} while (++i < 5); \nSystem.out.println(\"i = \" + i + \" and j = \" + j);","This loop is a do-while loop, which always executes the code block within the block at least once, due to the testing condition being at the end of the loop, rather than at the beginning. This particular loop is exited prematurely if i becomes greater than j.\n\nThe order is, test i against j, if bigger, it breaks from the loop, decrements j by one, and then tests the loop condition, where a pre-incremented by one i is tested for being lower than 5. The test is at the end of the loop, so i can reach the value of 5 before it fails. So it goes, start:\n1, 10\n2, 9\n3, 8\n4, 7\n5, 6 loop condition fails.",""));
		db.addSimulation_quiz(new Simulation_quizHandler(9, getTimeStomp(), "2 1 2 0 1 2", "What will be the output of the program?\n\npublic class Switch2 \n{\n\t final static short x = 2;\n\t  public static int y = 0;\n\t public static void main(String [] args)\n \t {\n\t\tfor (int z=0; z < 3; z++) \n\t\t{\n\t\t\tswitch (z) \n\t\t{\n\t\t\tcase x: System.out.print(\"0 \");\n\t\t\tcase x-1: System.out.print(\"1 \");\n\t\t\tcase x-2: System.out.print(\"2 \");\n\t\t}\n\t\t}\n\t}\n}","The case expressions are all legal because x is marked final, which means the expressions can be evaluated at compile time. In the first iteration of the for loop case x-2 matches, so 2 is printed. In the second iteration, x-1 is matched so 1 and 2 are printed (remember, once a match is found all remaining statements are executed until a break statement is encountered). In the third iteration, x is matched. So 0 1 and 2 are printed.",""));
		db.addSimulation_quiz(new Simulation_quizHandler(9, getTimeStomp(), "value = 8", "What will be the output of the program?\n\npublic class SwitchTest \n{\n\tpublic static void main(String[] args) \n\t {\n\t\tSystem.out.println(\"value =\" + switchIt(4)); \n\t}\n\t public static int switchIt(int x) \n\t{\n\t\tint j = 1;  \n\t\tswitch (x) \n\t\t{ \n\t\t\tcase l: j++; \n\t\t\tcase 2: j++;  \n\t\t\tcase 3: j++; \n\t\t\tcase 4: j++; \n\t\t\tcase 5: j++; \n\t\t\tdefault: j++; \n\t\t\t} \n\t\treturn j + x;  \n\t } \n}","Because there are no break statements, once the desired result is found, the program continues though each of the remaining options.",""));
//		db.addSimulation_quiz(new Simulation_quizHandler(2, getTimeStomp(), "abcghidef", "Identify the output of the program below:\n\npublic class StringRef\n{\n\tpublic static void main(String [] args)\n\t{\n\t\tString s1 = \"abc\";\n\t\tString s2 = \"def\";\n\t\tString s3 = s2;   /* Line 7 */\n\t\ts2 = \"ghi\";\t\tSystem.out.println(s1 + s2 + s3);\n\t}\n}","After line 7 executes, both s2 and s3 refer to a String object that contains the value \"def\". When line 8 executes, a new String object is created with the value \"ghi\" to which s2 refers. The reference variable s3 still refers to the (immutable) String object with the value \"def\".","Simulate the Java program and analyze the data types and variables."));
//		db.addSimulation_quiz(new Simulation_quizHandler(3, getTimeStomp(), "tiny", "What will be the output of the program?\n\nclass Test\n{\n\tpublic static void main(String [] args)\n\t{\n\t\tint x=20;\n\t\tString sup = (x < 15) ? \"small\" : (x < 22)? \"tiny\" : \"huge\";\n\t\tSystem.out.println(sup);\n\t}\n}","This is a nested ternary operator. The second evaluation (x < 22) is true, so the \"tiny\" value is assigned to sup.","Evaluate the expression by applying the operator hierarchy."));
//		db.addSimulation_quiz(new Simulation_quizHandler(4, getTimeStomp(), "42", "Provided below is a sample program using objects\nWhat will be the output of the program?\npublic static void main(String[] args)\n{\n\tObject obj = new Object()\n\t{\n\t\tpublic int hashCode()\n\t\t{\n\t\t\treturn 42;\n\t\t}\n\t};\n\tSystem.out.println(obj.hashCode());\n}","This code is an example of an anonymous inner class. They can be declared to extend another class or implement a single interface. Since they have no name you can not use the ''new'' keyword on them./n/nIn this case the annoynous class is extending the Object class. Within the {} you place the methods you want for that class. After this class has been declared its methods can be used by that object in the usual way e.g. objectname.annoymousClassMethod().","Simulate the program by identifying the different kinds of variables that can be used in a class."));
//		db.addSimulation_quiz(new Simulation_quizHandler(5, getTimeStomp(), "101", "This is a sample program using an if-else-if statement\nWhat will be the output of the program?\n\npublic class If2\n{\n\tstatic boolean b1, b2;\n\tpublic static void main(String [] args)\n\t{\n\t\tint x = 0;\n\t\tif ( !b1 ) /* Line 7 */\n\t\t{\n\t\t\tif ( !b2 ) /* Line 9 */\n\t\t\t{\n\t\t\t\tb1 = true;\n\t\t\t\tx++;\n\t\t\t\tif ( 5 > 6 )\n\t\t\t\t{\n\t\t\t\tx++;\n\t\t\t\t}\n\t\t\t\tif ( !b1 )\n\t\t\t\tx = x + 10;\n\t\t\t\telse if ( b2 = true ) /* Line 19 */\n\t\t\t\tx = x + 100;\n\t\t\t\telse if ( b1 | b2 ) /* Line 21 */\n\t\t\t\tx = x + 1000;\n\t\t\t}\n\t\t}\n\t\tSystem.out.println(x);\n\t}\n}","As instance variables, b1 and b2 are initialized to false. The if tests on lines 7 and 9 are successful so b1 is set to true and x is incremented. The next if test to succeed is on line 19 (note that the code is not testing to see if b2 is true, it is setting b2 to be true). Since line 19 was successful, subsequent else-ifs (line 21) will be skipped.","Formulate a solution using appropriate control structure."));
//		db.addSimulation_quiz(new Simulation_quizHandler(6, getTimeStomp(), "ABDCBDCB", "What will be the output of the program?\n\npublic class Delta\n{\n\tstatic boolean foo(char c)\n\t{\n\t\tSystem.out.print(c);\n\t\treturn true;\n\t}\n\tpublic static void main( String[] argv )\n\t{\n\t\tint i = 0;\n\t\tfor (foo('A'); foo('B') && (i < 2); foo('C'))\n\t\t{\n\t\t\ti++;\n\t\t\tfoo('D');\n\t\t}\n\t}\n}","'A' is only printed once at the very start as it is in the initialisation section of the for loop. The loop will only initialise that once.\n'B' is printed as it is part of the test carried out in order to run the loop.\n'D' is printed as it is in the loop.\n'C' is printed as it is in the increment section of the loop and will 'increment' only at the end of each loop. Here ends the first loop. Again 'B' is printed as part of the loop test.\n'D' is printed as it is in the loop.\n'C' is printed as it 'increments' at the end of each loop.\nAgain 'B' is printed as part of the loop test. At this point the test fails because the other part of the test (i < 2) is no longer true. i has been increased in value by 1 for each loop with the line: i++;\nThis results in a printout of ABDCBDCB","Generate a programming solutions using For loop statements in a class inheritance."));
//		db.addSimulation_quiz(new Simulation_quizHandler(7, getTimeStomp(), "1", "What is the output of the program?\n\npublic class Test\n{\npublic static void main(String [] args)\n\t{\n\tint I = 0;\n\touter:\n\twhile (true)\n\t{\n\t\tI++;\n\t\tinner:\n\t\tfor (int j = 0; j < 10; j++)\n\t\t{\n\t\t\tI += j;\n\t\tif (j == 3)\n\t\t\tcontinue inner;\n\t\t\tbreak outer;\n\t\t}\n\t\t\tcontinue outer;\n\t}\n\tSystem.out.println(I);\n\t}\n}","The program flows as follows: I will be incremented after the while loop is entered, then I will be incremented (by zero) when the for loop is entered. The if statement evaluates to false, and the continue statement is never reached. The break statement tells the JVM to break out of the outer loop, at which point I is printed and the fragment is done.","Generate a programming solution using a While loop statements."));
//		db.addSimulation_quiz(new Simulation_quizHandler(8, getTimeStomp(), "j=4", "What will be the output of the program?\n\npublic class ExamQuestion8\n{\n\tstatic int j;\n\tstatic void methodA(int i)\n\t{\n\t\tboolean b;\n\t\tdo\n\t\t{\n\t\t\tb = i<10 | methodB(4); /* Line 9 */\n\t\t\tb = i<10 || methodB(8);  /* Line 10 */\n\t\t}while (!b);\n\t}\n\tstatic boolean methodB(int i)\n\t{\n\t\tj += i;\n\t\treturn true;\n\t\t}\n\tpublic static void main(String[] args)\n\t{\n\t\t\tmethodA(0);\n\t\t\tSystem.out.println( \"j = \" + j );\n\t}\n}","The lines to watch here are lines 9 & 10. Line 9 features the non-shortcut version of the OR operator so both of its operands will be evaluated and therefore methodB(4) is executed.\nHowever line 10 has the shortcut version of the OR operator and if the 1st of its operands evaluates to true (which in this case is true), then the 2nd operand isn't evaluated, so methodB(8) never gets called.\nThe loop is only executed once, b is initialized to false and is assigned true on line 9. Thus j = 4.","Generate a programming solution using a Do While loop statements."));
//		db.addSimulation_quiz(new Simulation_quizHandler(9, getTimeStomp(), "15 15", "What will be the output of the program?\n\nclass PassA\n{\n\tpublic static void main(String [] args)\n\t{\n\t\tPassA p = new PassA();\n\t\tp.start();\n\t}\n\n\tvoid start()\n\t{\n\t\tlong [] a1 = {3,4,5};\n\t\tlong [] a2 = fix(a1);\n\t\tSystem.out.print(a1[0] + a1[1] + a1[2] + \" \");\n\t\tSystem.out.println(a2[0] + a2[1] + a2[2]);\n\t}\n\n\tlong [] fix(long [] a3)\n\t{\n\t\ta3[1] = 7;\n\t\treturn a3;\n\t}\n}","The reference variables a1 and a3 refer to the same long array object. When the [1] element is updated in the fix() method, it is updating the array referred to by a1.\n\nThe reference variable a2 refers to the same array object.\n\nSo Output: 3+7+5+\" \"3+7+5\n\nOutput: 15 15 Because Numeric values will be added.","Formulate a programming solution to control store data using an Array. "));
		
		
		
		
		//////////////////////netbeans//////////////trial simukation//////////////////
		db.addSimulation_quiz2(new Simulation_quizHandler2(1, getTimeStomp(), "netbeans", "The Output is:\n\n\"netbeans\"","Expalantion NetBeans."));
		db.addSimulation_quiz2(new Simulation_quizHandler2(2, getTimeStomp(), "netbeans2", "The Output is:\n\n\"netbeans2\"","Expalantion NetBeans2."));
		db.addSimulation_quiz2(new Simulation_quizHandler2(3, getTimeStomp(), "netbeans3", "The Output is:\n\n\"netbeans3\"","Expalantion NetBeans3."));
		db.addSimulation_quiz2(new Simulation_quizHandler2(4, getTimeStomp(), "netbeans4", "The Output is:\n\n\"netbeans4\"","Expalantion NetBeans4."));
		db.addSimulation_quiz2(new Simulation_quizHandler2(5, getTimeStomp(), "netbeans5", "The Output is:\n\n\"netbeans5\"","Expalantion NetBeans5."));
		db.addSimulation_quiz2(new Simulation_quizHandler2(6, getTimeStomp(), "netbeans6", "The Output is:\n\n\"netbeans6\"","Expalantion NetBeans6."));
		db.addSimulation_quiz2(new Simulation_quizHandler2(7, getTimeStomp(), "netbeans7", "The Output is:\n\n\"netbeans7\"","Expalantion NetBeans7."));
	
	}

	
	
}
