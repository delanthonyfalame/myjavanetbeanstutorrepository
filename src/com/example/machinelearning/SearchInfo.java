package com.example.machinelearning;

import java.util.ArrayList;
import java.util.Locale;

import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.machinelearning.Database.Database;
import com.example.machinelearning.Database.DictionaryHandler;

public class SearchInfo extends SwitchFragmentParent implements TextToSpeech.OnInitListener{
	protected static final String TAG = "Search Info";
	private View view;
	ArrayList<DictionaryHandler> dictionary = new ArrayList<DictionaryHandler>();
	private int termID;
	TextToSpeech textToSpeech;
	//ImageView speak;
	
	TextView title, syllable, content;
	ImageView stop, read;
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.search_info, container, false);
		textToSpeech = new TextToSpeech(getActivity(), this);
		db = new Database(getActivity());
		Bundle bundle = this.getArguments();
		termID = bundle.getInt("dictionary");
		setWidgets();
		return view;
	}

	private void setWidgets() {
		for(DictionaryHandler diction : db.getAllDictionary())
			Log.i(TAG, "dictionary id : " + diction.getBase_id()+ " == "  + termID);

		//get data from database base on id
		DictionaryHandler term = db.getDictionary(termID);
		stop = (ImageView) view.findViewById(R.id.stop);
		read = (ImageView) view.findViewById(R.id.read);
		
		
		  // button on click event
	    stop.setOnClickListener(new View.OnClickListener() {
	 
	    @Override
	    public void onClick(View arg0) {
	    textToSpeech.stop();
	    			
	        }
	    }); 		
		
		  // button on click event
      read.setOnClickListener(new View.OnClickListener() {

          @Override
          public void onClick(View arg0) {
              speakOut();
          }
                    
      });
      
		title = (TextView) view.findViewById(R.id.term);
		syllable = (TextView) view.findViewById(R.id.syllable);
		content = (TextView) view.findViewById(R.id.content);
		//speak = (ImageView) view.findViewById(R.id.speak);
		title.setText(term.getName());
		syllable.setText(term.getSyllables());
		content.setText(term.getMeaning());
		
		

	}
	
	
	
	

	@Override
	public void onDestroy() {
		if (textToSpeech != null) {
			textToSpeech.stop();
			textToSpeech.shutdown();
		}
		super.onDestroy();

	}

	@Override
	public void onInit(int status) {

        if (status == TextToSpeech.SUCCESS) {
 
            int result = textToSpeech.setLanguage(Locale.US);
            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "This Language is not supported");
            } else {
                read.setEnabled(true);
                speakOut();
            }
 
        } else {
            Log.e("TTS", "Initilization Failed!");
        }
 		
	}
	
	  private void speakOut() {
		  
	        String text = title.getText().toString() + " " + content.getText().toString();
	 
	        textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null);
	    }
}
