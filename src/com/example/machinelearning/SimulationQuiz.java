package com.example.machinelearning;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnDismissListener;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.machinelearning.Adapter.QuizScoreAdapter;
import com.example.machinelearning.Adapter.SimulationScoreAdapter;
import com.example.machinelearning.Adapter.SimulatorAdapter;
import com.example.machinelearning.Database.AnswerHandler;
import com.example.machinelearning.Database.Database;
import com.example.machinelearning.Database.QuizHandler;
import com.example.machinelearning.Database.QuizScoreHandler;
import com.example.machinelearning.Database.Quiz_resultHandler;
import com.example.machinelearning.Database.SimulationAnswerHandler;
import com.example.machinelearning.Database.Simulation_quizHandler;
import com.example.machinelearning.Database.StatisticsHandler;
import com.example.machinelearning.Utilities.GlobalVariables;

public class SimulationQuiz extends SwitchFragmentParent implements GlobalVariables{
	protected static final String TAG = "Answer";
	private View view;
	private int chapter;
	ArrayList<Simulation_quizHandler> simulation_quizHandlers = new ArrayList<Simulation_quizHandler>();
	ArrayList<SimulationAnswerHandler> simulationAnswer = new ArrayList<SimulationAnswerHandler>();
	private ListView list;
	Simulation_quizHandler simulationData;
	SimulatorAdapter adapter;
	int totalScore;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.activity_simulation, container, false);
		db = new Database(getSherlockActivity());
		Bundle bundle = this.getArguments();
		chapter = bundle.getInt("chapter");
		Log.i(TAG, "chapter : " + chapter);
		simulationData = db.getSimulation_quiz(chapter);
		simulation_quizHandlers = db.getAllSimulation_quizByChapter(chapter);
		Log.i(TAG, "all chapter simulation : " + simulation_quizHandlers.size());
		setWidget();
		return view;
	}

	private void setWidget() {
		list = (ListView) view.findViewById(R.id.list);
		adapter = new SimulatorAdapter(getActivity(), simulation_quizHandlers);
		list.setItemsCanFocus(true);
		list.setAdapter(adapter);

		//		TextView simulationQuestion = (TextView) view.findViewById(R.id.simulation_question);
		//		TextView ilo = (TextView) view.findViewById(R.id.ilo2);
		//		final EditText answer = (EditText) view.findViewById(R.id.answer);
		//		
		//		simulationQuestion.setText(simulationData.getQuestion());
		//		ilo.setText(simulationData.getIlo());


		view.findViewById(R.id.finalize).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//				confirmation(answer.getText().toString());
				totalScore = calculteScore();
				Log.i(TAG, "total score : " + totalScore);
				showScore(totalScore);
			}
		});
	}

	protected int calculteScore() {
		int totalScore = 0;
		int count = 1;
		for(Simulation_quizHandler item : adapter.getSimulationData()){
			boolean isCorrect = false;
			if(item.getAnswer().equals(item.getAnswerContainer())){
				Log.i(TAG, "correct!");
				isCorrect = true;
				totalScore+= 25;
			}
			simulationAnswer.add(new SimulationAnswerHandler(item, count, isCorrect));
			count++;
		}
		Log.i(TAG, "score is : " + totalScore);
		return totalScore;
	}

	
	protected int getPercentage(int total) {
		int percentage = (int) ((( (double)total) / ((double)100 ) * 50) + 50);
		return (int) percentage;
	}
	
	private static int countLines(String str){
		String[] lines = str.split("\r\n|\r|\n");
		return  lines.length;
	}

	//	void confirmation(final String answer){
	//		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
	//			@Override
	//			public void onClick(DialogInterface dialog, int which) {
	//				switch (which){
	//				case DialogInterface.BUTTON_POSITIVE:
	//					if(answer.equals(simulationData.getAnswer())){
	//						Log.i(TAG, "match!");
	//						Toast.makeText(getActivity(), "Your answer is correct!\n\nYou got a passing grade of 100", Toast.LENGTH_LONG).show();
	//						db.addStatistics(new StatisticsHandler(chapter, getDate(), chapter, 100, JAVA_EXC, 100));
	//					}else{
	//						Toast.makeText(getActivity(), "Your answer is incorrect!\n\nYou got a failing grade of 50", Toast.LENGTH_LONG).show();
	//						db.addStatistics(new StatisticsHandler(chapter, getDate(), chapter, 50, JAVA_EXC, 50));
	//					}
	//					simulationAnswer(answer);
	//
	//					//switchFrag.goBack(new JavaQuiz());
	//					break;
	//				case DialogInterface.BUTTON_NEGATIVE:
	//					switchFrag.goBack(new JavaLesson());
	//					break;
	//				}
	//			}
	//		};
	//		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
	//		builder.setTitle("Alert").setMessage("Are you sure you want to finish the Quiz?").setPositiveButton("Yes", dialogClickListener)
	//		.setNegativeButton("No", dialogClickListener).show();
	//	}

	public void simulationAnswer(String yourAnswer){
		final Dialog dialog = new Dialog(getActivity());
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.identification_result);
		TextView question = (TextView) dialog.findViewById(R.id.question);
		TextView correctAnswer = (TextView) dialog.findViewById(R.id.correct_answer);
		TextView userAnswer = (TextView) dialog.findViewById(R.id.your_answer);
		TextView explanation = (TextView) dialog.findViewById(R.id.explanation);
		//TextView ilo = (TextView) dialog.findViewById(R.id.ilo2);
		question.setText(simulationData.getQuestion());
		correctAnswer.setText(simulationData.getAnswer());
		explanation.setText(simulationData.getExplanation());
		//ilo.setText(simulationData.getIlo());
		userAnswer.setText(yourAnswer);

		dialog.findViewById(R.id.done).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
				switchFrag.goBack(new JavaLesson());
			}
		});
		dialog.setCancelable(false);
		dialog.setOnDismissListener(new OnDismissListener() {

			@Override
			public void onDismiss(DialogInterface dialog) {
				switchFrag.goBack(new JavaLesson());
			}	
		});
		dialog.show();
	}


	protected void showScore(final int totalScore2) {
		final Dialog dialog = new Dialog(getActivity());
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setCancelable(false);
		dialog.getWindow().setBackgroundDrawableResource(android.R.color.white);
		dialog.setContentView(R.layout.simulation_score);
		dialog.getWindow().setLayout(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		ListView simulationList = (ListView) dialog.findViewById(R.id.list);
		SimulationScoreAdapter simulationAdapter = new SimulationScoreAdapter(getActivity(), simulationAnswer);
		simulationList.setAdapter(simulationAdapter);

		TextView scoreInfo = (TextView) dialog.findViewById(R.id.score_info);
		if(totalScore2 > 50){
			//scoreInfo.setText("EXERCISE:\nPROGRAM SIMULATION RESULTS");
			scoreInfo.setText("Your Score is " + totalScore2 + " over 100 item, equivalent to the rating of "+ getPercentage(totalScore2) +"% . You have satisfied the learning objectives of this chapter. Please take the Quiz to be able to proceed to the next Chapter.");
		}else{
			//scoreInfo.setText("EXERCISE:\nPROGRAM SIMULATION RESULTS");
			scoreInfo.setText("Your Score is "+ totalScore2 + " over 100 items, equivalent to the rating of "+getPercentage(totalScore2)  +"% . You did not satisfy the learning objectives of this chapter. Please study this chapter again. ");
			//			showAlertDialog(getActivity(), "Sorry, please try again...", "Your Score is "+ total + " over 10 items, equivalent to the rating of "+getPercentage(total) +". You did not satisfy the learning objectives of this chapter. Please study this chapter again. ", false);
		}



		dialog.findViewById(R.id.finalize).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				db.addStatistics(new StatisticsHandler(chapter, getDate(), chapter, totalScore2, JAVA_EXC, totalScore2));
				confirmation();
				dialog.dismiss();
			}
		});

		dialog.findViewById(R.id.practice).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Log.i(TAG, "link to compiler");
				String url = "http://ideone.com/";
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse(url));
				startActivity(i);
			}
		});

		dialog.show();

	}


	void confirmation(){
		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				switch (which){
				case DialogInterface.BUTTON_POSITIVE:
					ArrayList<QuizHandler> quizArray = db.getJavaAllQuizByChapter(chapter);
					if(quizArray != null){
						if(db.getStatsByChapterAndTypeCount(chapter, GlobalVariables.JAVA) <= 0){
							switchLesson.switchToQuiz(chapter, new Objective());
						}else{
							Toast.makeText(getActivity(), "You've already taken the quiz", Toast.LENGTH_LONG).show();
							switchFrag.goBack(new JavaLesson());
						}
					}
					else
						Toast.makeText(getActivity(), "No Exercise found in this chapter", Toast.LENGTH_SHORT).show();
					break;
				case DialogInterface.BUTTON_NEGATIVE:
					switchFrag.goBack(new JavaLesson());
					break;
				}
			}
		};
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Reminder").setMessage("The next assessment is a multiple choice quiz.\n\nInstruction:\n\nThe quiz is a randomized multiple choices that is consist of 10 questions.\n- Read the questions and choose your answer carefully.\n- You will not be able to proceed to the next question until you have chosen an answer to the present question.\n\nPlease tap the \"Yes\" button to proceed on taking the quiz, an the no button to go back to the list of chapters.").setPositiveButton("Yes", dialogClickListener)
		.setNegativeButton("No", dialogClickListener).show();

	}




}
