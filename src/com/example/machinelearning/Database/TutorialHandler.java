package com.example.machinelearning.Database;

public class TutorialHandler {
	String  message, link, referrence;
	int video;
	public TutorialHandler(int video, String message, String link, String referrence) {
		this.video = video;
		this.message = message;
		this.link = link;
		this.referrence = referrence;
	}
	public int getVideo() {
		return video;
	}
	public void setVideo(int video) {
		this.video = video;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getReferrence() {
		return referrence;
	}
	public void setReferrence(String referrence) {
		this.referrence = referrence;
	}

}
