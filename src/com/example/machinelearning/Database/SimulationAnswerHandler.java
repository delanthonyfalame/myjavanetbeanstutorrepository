package com.example.machinelearning.Database;

public class SimulationAnswerHandler {
	Simulation_quizHandler simulation;
	int itemNumber;
	boolean isCorrect;
	public SimulationAnswerHandler(Simulation_quizHandler simulation, int itemNumber,boolean isCorrect ) {
		this.simulation = simulation;
		this.itemNumber = itemNumber;
		this.isCorrect = isCorrect;
	}
	public Simulation_quizHandler getSimulation() {
		return simulation;
	}
	public void setSimulation(Simulation_quizHandler simulation) {
		this.simulation = simulation;
	}
	public int getItemNumber() {
		return itemNumber;
	}
	public void setItemNumber(int itemNumber) {
		this.itemNumber = itemNumber;
	}
	public boolean isCorrect() {
		return isCorrect;
	}
	public void setCorrect(boolean isCorrect) {
		this.isCorrect = isCorrect;
	}
	
	
}
