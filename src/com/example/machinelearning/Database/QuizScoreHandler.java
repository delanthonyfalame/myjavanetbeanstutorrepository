package com.example.machinelearning.Database;

public class QuizScoreHandler {
	QuizHandler question;
	String answer;
	boolean isCorret;
	int position;
	String correctAnswer;
	
	public QuizScoreHandler(QuizHandler question, String answer, int position, boolean isCorrect, String correctAnswer) {
		this.question = question;
		this.answer = answer;
		this.isCorret = isCorrect;
		this.position = position;
		this.correctAnswer = correctAnswer;
	}
	public String getCorrectAnswer() {
		return correctAnswer;
	}
	public void setCorrectAnswer(String correctAnswer) {
		this.correctAnswer = correctAnswer;
	}
	public QuizHandler getQuestion() {
		return question;
	}

	public void setQuestion(QuizHandler question) {
		this.question = question;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public boolean isCorret() {
		return isCorret;
	}

	public void setCorret(boolean isCorret) {
		this.isCorret = isCorret;
	}
	
	public int getPosition() {
		return position;
	}
	
	public void setPosition(int position) {
		this.position = position;
	}
	
}
