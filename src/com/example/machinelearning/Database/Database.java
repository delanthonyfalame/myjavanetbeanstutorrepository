package com.example.machinelearning.Database;


import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class Database extends SQLiteOpenHelper{
	private static final String database_name = "machine_learning_db"; 
	private static final String table_java_lesson = "lesson_tbl"; 

	private static final String key_id = "id";
	private static final String key_base_id = "base_id";
	private static final String key_date_entry = "date_entry";

	public Database(Context context) { 
		super( context, database_name, null, 1); 
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_JAVA_LESSON_TABLE);
		db.execSQL(CREATE_JAVA_QUIZ_TABLE);
		db.execSQL(CREATE_NETBEANS_LESSON_TABLE);
		db.execSQL(CREATE_NETBEANS_QUIZ_TABLE);
		db.execSQL(CREATE_dictionary_TABLE);
		db.execSQL(CREATE_instruction_TABLE);
		db.execSQL(CREATE_quiz_result_TABLE);
		db.execSQL(CREATE_JAVA_CHAPTER_TABLE);
		db.execSQL(CREATE_statistics_TABLE);
		db.execSQL(CREATE_NETBEANS_CHAPTER_TABLE);
		db.execSQL(CREATE_simulation_quiz_TABLE);
		db.execSQL(CREATE_simulation_quiz2_TABLE);

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + table_java_lesson);
		db.execSQL("DROP TABLE IF EXISTS " + table_netbeans);
		db.execSQL("DROP TABLE IF EXISTS " + table_java_quiz);
		db.execSQL("DROP TABLE IF EXISTS " + table_netbeans_quiz);
		db.execSQL("DROP TABLE IF EXISTS " + table_dictionary);
		db.execSQL("DROP TABLE IF EXISTS " + table_instruction);
		db.execSQL("DROP TABLE IF EXISTS " + table_quiz_result);
		db.execSQL("DROP TABLE IF EXISTS " + table_netbeans_chapter);
		db.execSQL("DROP TABLE IF EXISTS " + table_statistics);
		db.execSQL("DROP TABLE IF EXISTS " + table_simulation_quiz);
		db.execSQL("DROP TABLE IF EXISTS " + table_simulation_quiz2);
		onCreate(db);
	}

	private static final String key_chapter = "chapter";
	private static final String key_title = "title";
	private static final String key_content = "content";
	private static final String key_referrence = "referrence";
	private static final String key_image = "image";
	private static final String key_chapter_title = "chapter_title";
	private static final String key_objective = "objective";

	String CREATE_JAVA_LESSON_TABLE = "CREATE TABLE " + table_java_lesson + " ( " + 
			key_id + " INTEGER PRIMARY KEY, " + 
			key_base_id + " INTEGER, " + 
			key_date_entry + " DATE, " + 
			key_chapter + " INTEGER, " + 
			key_title + " TEXT, " + 
			key_content + " TEXT, " + 
			key_referrence + " TEXT, " + 
			key_image + " TEXT, " + 
			key_chapter_title + " TEXT, " + 
			key_objective + " TEXT, " + 
			"UNIQUE ( " + key_base_id + ") ON CONFLICT REPLACE);" ;

	/** ===================== CRUD ================ **/
	public void addJavaLesson(LessonHandler lessonHandler) { 
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put ( key_base_id, lessonHandler.base_id);
		values.put ( key_date_entry, lessonHandler.date_entry);
		values.put ( key_chapter, lessonHandler.chapter);
		values.put ( key_title, lessonHandler.title);
		values.put ( key_content, lessonHandler.content);
		values.put ( key_referrence, lessonHandler.referrence);
		values.put ( key_image, lessonHandler.image);
		values.put	(key_chapter_title, lessonHandler.chapter_name);
		values.put	(key_objective, lessonHandler.objective);
		long i = db.insert( table_java_lesson, null, values);
		db.close();
	}

	/** ===================== GET SINGLE DATA ================ **/
	public LessonHandler getJavaLesson (long id){
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.query( table_java_lesson, new String[] { 
				key_id, key_base_id, key_date_entry, key_chapter, key_title, key_content,
				key_referrence, key_image, key_chapter_title, key_objective}, key_base_id + "=?", 
				new String[]{ String.valueOf(id) } , null, null, null, null);
		LessonHandler lesssonHandler = new LessonHandler();
		if(cursor.moveToFirst())
			lesssonHandler = new LessonHandler(
					cursor.getInt(0), 
					cursor.getInt(1), 
					cursor.getString(2), 
					cursor.getInt(3), 
					cursor.getString(4), 
					cursor.getString(5), 
					cursor.getString(6),
					cursor.getString(7),
					cursor.getString(8),
					cursor.getString(9)
					);
		db.close();
		return lesssonHandler; 
	}

	/** ===================== GET LESSON BY CHAPTER ======================**/
	public int getJavaLessonByChapter (int chapter){
		String selectQuery = "SELECT "+ key_base_id + " FROM " + table_java_lesson +" WHERE " + key_chapter + " = " + chapter + " ORDER BY " 
				+ key_base_id + " ASC " + " LIMIT 1 " ;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		int lastTransactionID  = 0;
		if(cursor.moveToFirst()){
			lastTransactionID = cursor.getInt(0);
		}
		db.close();
		return lastTransactionID;
	}
	/** ===================== GET LATEST TRANSACTION ID ================ **/
	public int getLastJavaLesson (){
		String selectQuery = "SELECT "+ key_base_id+ " FROM " + table_java_lesson + " ORDER BY "
				+ key_base_id + " DESC " + " LIMIT 1 " ;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		int lastTransactionID  = 0;
		if(cursor.moveToFirst()){
			lastTransactionID = cursor.getInt(0);
		}
		db.close();
		return lastTransactionID;
	}

	/** ===================== GET ALL CHAPTERS ================ **/
	public ArrayList<String> getAllJavaQuizChapter (){
		String selectQuery = "SELECT "+ key_chapter_title+ " FROM " + table_java_lesson + " ORDER BY "
				+ key_chapter + " ASC ";
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		ArrayList<String> chapters = new ArrayList<String>();
		if(cursor.moveToFirst()){
			do {
				chapters.add(cursor.getString(0));
			} while (cursor.moveToNext());

		}
		db.close();
		return chapters;
	}



	/** ===================== GET LATEST DATA ================ **/
	public LessonHandler getLatestJavaLesson (){
		String selectQuery = "SELECT * FROM " + table_java_lesson + " ORDER BY " 
				+ key_date_entry + " DESC " + " LIMIT 1 " ;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		LessonHandler lessonData = new LessonHandler();
		if(cursor.moveToFirst())
			lessonData = new LessonHandler(
					cursor.getInt(0), 
					cursor.getInt(1), 
					cursor.getString(2), 
					cursor.getInt(3), 
					cursor.getString(4), 
					cursor.getString(5), 
					cursor.getString(6),
					cursor.getString(7),
					cursor.getString(8),
					cursor.getString(9)
					
					);
		else
			return null;
		db.close();
		return lessonData;
	}

	/** ===================== GET ALL DATA ================ **/
	public LessonHandler getJavaIDandChapter(int lesson, int chapter){
		LessonHandler userdataHandlerArray = null;
		String selectQuery = "SELECT * FROM " + table_java_lesson +  " WHERE " + key_base_id + " = " + lesson + " AND " + key_chapter + " = " 
				+ chapter +   " ORDER BY " + key_date_entry; 
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			userdataHandlerArray =  new LessonHandler();
			userdataHandlerArray.setId(cursor.getInt(0)); 
			userdataHandlerArray.setBase_id(cursor.getInt(1)); 
			userdataHandlerArray.setDate_entry(cursor.getString(2)); 
			userdataHandlerArray.setChapter(cursor.getInt(3));
			userdataHandlerArray.setTitle(cursor.getString(4));
			userdataHandlerArray.setContent(cursor.getString(5));
			userdataHandlerArray.setReferrence(cursor.getString(6));
			userdataHandlerArray.setImage(cursor.getString(7));
			userdataHandlerArray.setChapter_name(cursor.getString(8));
			userdataHandlerArray.setObjective(cursor.getString(9));
		} 
		db.close();
		return userdataHandlerArray;
	}

	/** ===================== GET ALL DATA ================ **/
	public int getMaxValueJava( int chapter){
		//		SELECT MAX(Price) AS HighestPrice FROM Products;
		String selectQuery = "SELECT MAX ( " + key_base_id + " )FROM " + table_java_lesson +  " WHERE "  + key_chapter + " = " 
				+ chapter +   " ORDER BY " + key_date_entry; 
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		boolean isThisLast = false;
		int maxValue = 0;
		if (cursor.moveToFirst()) {
			maxValue = cursor.getInt(0);
		} 
		db.close();
		return maxValue;
	}

	/** ===================== GET ALL DATA ================ **/
	public int getJavaNextLesson(int lesson, int chapter){
		String selectQuery = "SELECT " + key_base_id + " FROM " + table_java_lesson +  " WHERE " + key_base_id + " > " + lesson + " AND " + key_chapter + " = " 
				+ chapter +  " ORDER BY " + key_base_id + " ASC LIMIT 1" ; 
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		int nextLesson = 0;
		if (cursor.moveToFirst()) {
			nextLesson = cursor.getInt(0);
		} 
		db.close();
		return nextLesson;
	}


	/** ===================== GET ALL DATA ================ **/
	public ArrayList< LessonHandler >getAllJavaLessons (){
		ArrayList< LessonHandler> userdataList = new ArrayList< LessonHandler>();
		String selectQuery = "SELECT * FROM " + table_java_lesson + " ORDER BY " + key_base_id + " ASC"; 
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				LessonHandler userdataHandlerArray = new LessonHandler();
				userdataHandlerArray.setId(cursor.getInt(0)); 
				userdataHandlerArray.setBase_id(cursor.getInt(1)); 
				userdataHandlerArray.setDate_entry(cursor.getString(2)); 
				userdataHandlerArray.setChapter(cursor.getInt(3));
				userdataHandlerArray.setTitle(cursor.getString(4));
				userdataHandlerArray.setContent(cursor.getString(5));
				userdataHandlerArray.setReferrence(cursor.getString(6));
				userdataHandlerArray.setImage(cursor.getString(7));
				userdataHandlerArray.setChapter_name(cursor.getString(8));
				userdataHandlerArray.setObjective(cursor.getString(9));
				userdataList.add(userdataHandlerArray);
			} while (cursor.moveToNext());
		}
		Log.i(TAG, "size : " + cursor.getCount());
		db.close();
		return userdataList;
	}

	/** ===================== UPDATE DATA ================ **/
	public int updateJavaLesson(LessonHandler lessonArray) {
		SQLiteDatabase db = this.getWritableDatabase(); 
		ContentValues values = new ContentValues(); 
		values.put ( key_id, lessonArray.id);
		values.put ( key_base_id, lessonArray.base_id);
		values.put ( key_date_entry, lessonArray.date_entry);
		values.put ( key_chapter, lessonArray.chapter);
		values.put ( key_title, lessonArray.title);
		values.put ( key_content, lessonArray.content);
		values.put ( key_referrence, lessonArray.referrence);
		values.put ( key_image, lessonArray.image);
		values.put ( key_chapter_title, lessonArray.chapter_name);
		values.put ( key_objective, lessonArray.objective);
		return db.update( table_java_lesson, values, key_base_id + " =? " ,
				new String []{ String.valueOf(lessonArray.getId())});
	}

	/** ===================== DELETE DATA ================ **/
	public void deleteJavaLesson(int id){
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(table_java_lesson, key_base_id + " = ? ", new String [] { String.valueOf(id) });
		db.close();
	}
	/** ===================== GET COUNT ================ **/
	public int getJavaLessonCount(){
		String countQuery = "SELECT * FROM " + table_java_lesson;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		int count = cursor.getCount();
		db.close();
		return count;
	}

	/**============== QUIZ TABLE ==============**/
	private static final String table_java_quiz = "java_quiz_tbl"; 

	private static final String key_lesson = "lesson";
	private static final String key_mark = "mark";
	private static final String key_answer = "answer";
	private static final String key_dummy1 = "dummy1";
	private static final String key_dummy2 = "dummy2";
	private static final String key_dummy3 = "dummy3";
	private static final String key_question = "question";
	private static final String key_explanation = "explanation";
	private static final String key_ilo = "ilo";
	private static final String key_hint = "hint";
	private static final String TAG = "database";


	String CREATE_JAVA_QUIZ_TABLE = "CREATE TABLE " + table_java_quiz + " ( " + 
			key_id + " INTEGER PRIMARY KEY, " + 
			key_base_id + " INTEGER, " + 
			key_date_entry + " DATE, " + 
			key_lesson + " INTEGER, " + 
			key_chapter + " INTEGER, " + 
			key_mark + " INTEGER, " + 
			key_answer + " TEXT, " + 
			key_dummy1 + " TEXT, " + 
			key_dummy2 + " TEXT, " + 
			key_dummy3 + " TEXT, " + 
			key_question + " TEXT, " + 
			key_title + " TEXT, " + 
			key_hint + " TEXT, " + 
			key_image + " TEXT, " + 
			"UNIQUE ( " + key_base_id + ") ON CONFLICT REPLACE);" ;



	/** ===================== CRUD ================ **/
	public int addJavaQuiz(QuizHandler quizData) { 
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put ( key_base_id, quizData.base_id);
		values.put ( key_date_entry, quizData.date_entry);
		values.put ( key_lesson, quizData.lesson);
		values.put ( key_chapter, quizData.chapter);
		values.put ( key_mark, quizData.mark);
		values.put ( key_answer, quizData.answer);
		values.put ( key_dummy1, quizData.dummy1);
		values.put ( key_dummy2, quizData.dummy2);
		values.put ( key_dummy3, quizData.dummy3);
		values.put ( key_question, quizData.question);
		values.put ( key_title, quizData.title);
		values.put ( key_hint, quizData.hint);
		values.put ( key_image, quizData.image);
		long i = db.insert( table_java_quiz, null, values);
		db.close();
		return (int) i;
	}

	/** ===================== GET SINGLE DATA ================ **/
	public QuizHandler getJavaQuiz (long id){
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.query( table_java_quiz, new String[] { 
				key_id, key_base_id, key_date_entry, key_lesson,  key_chapter, key_mark, key_answer, key_dummy1,
				key_dummy2, key_dummy3, key_question, key_title, key_hint, key_image}, key_base_id + "=?", 
				new String[]{ String.valueOf(id) } , null, null, null, null);
		QuizHandler quizHandler = new QuizHandler();
		if(cursor.moveToFirst())
			quizHandler = new QuizHandler(
					cursor.getInt(0), 
					cursor.getInt(1), 
					cursor.getString(2), 
					cursor.getInt(3), 
					cursor.getInt(4), 
					cursor.getInt(5), 
					cursor.getString(6),
					cursor.getString(7),
					cursor.getString(8),
					cursor.getString(9),
					cursor.getString(10),
					cursor.getString(11),
					cursor.getString(12),
					cursor.getString(13)
					);
		db.close();
		return quizHandler; 
	}

	/** ===================== GET LATEST TRANSACTION ID ================ **/
	public int getJavaLastQuiz (){
		String selectQuery = "SELECT "+ key_base_id+ " FROM " + table_java_quiz + " ORDER BY "
				+ key_base_id + " DESC " + " LIMIT 1 " ;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		int lastTransactionID  = 0;
		if(cursor.moveToFirst()){
			lastTransactionID = cursor.getInt(0);
		}
		db.close();
		return lastTransactionID;
	}

	/** ===================== GET LATEST DATA ================ **/
	public QuizHandler getJavaLatestQuiz (){
		String selectQuery = "SELECT * FROM " + table_java_quiz + " ORDER BY " 
				+ key_date_entry + " DESC " + " LIMIT 1 " ;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		QuizHandler quizData = new QuizHandler();
		if(cursor.moveToFirst())
			quizData = new QuizHandler(
					cursor.getInt(0), 
					cursor.getInt(1), 
					cursor.getString(2), 
					cursor.getInt(3), 
					cursor.getInt(4), 
					cursor.getInt(5), 
					cursor.getString(6),
					cursor.getString(7),
					cursor.getString(8),
					cursor.getString(9),
					cursor.getString(10),
					cursor.getString(11),
					cursor.getString(12),
					cursor.getString(13)
					);
		else
			return null;
		db.close();
		return quizData;
	}


	/** ===================== GET ALL DATA ================ **/
	public ArrayList< QuizHandler >getJavaAllQuiz (){
		ArrayList< QuizHandler> userdataList = new ArrayList< QuizHandler>();
		String selectQuery = "SELECT * FROM " + table_java_quiz + " ORDER BY " + key_date_entry; 
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		Log.i(TAG, "cursor count " + cursor.getCount());
		if (cursor.moveToFirst()) {
			do {
				QuizHandler quizDataHandler = new QuizHandler();
				quizDataHandler.setId(cursor.getInt(0)); 
				quizDataHandler.setBase_id(cursor.getInt(1)); 
				quizDataHandler.setDate_entry(cursor.getString(2)); 
				quizDataHandler.setLesson(cursor.getInt(3));
				quizDataHandler.setChapter(cursor.getInt(4));
				quizDataHandler.setMark(cursor.getInt(5));
				quizDataHandler.setAnswer(cursor.getString(6));
				quizDataHandler.setDummy1(cursor.getString(7));
				quizDataHandler.setDummy2(cursor.getString(8));
				quizDataHandler.setDummy3(cursor.getString(9));
				quizDataHandler.setQuestion(cursor.getString(10));
				quizDataHandler.setTitle(cursor.getString(11));
				quizDataHandler.setHint(cursor.getString(12));
				quizDataHandler.setImage(cursor.getString(13));
				userdataList.add(quizDataHandler);
			} while (cursor.moveToNext());
		}
		db.close();
		return userdataList;
	}

	/** ===================== GET ALL QUIZ BY CHAPTER ================ **/
	public ArrayList< QuizHandler >getJavaAllQuizByChapter (int chapter){
		ArrayList< QuizHandler> userdataList = null;
		String selectQuery = "SELECT * FROM " + table_java_quiz  + " WHERE " +key_chapter + " = " + chapter + " ORDER BY " + key_date_entry + " LIMIT 10"; 
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		Log.i(TAG, "cursor count " + cursor.getCount());
		if (cursor.moveToFirst()) {
			userdataList = new ArrayList<QuizHandler>();
			do {
				QuizHandler quizDataHandler = new QuizHandler();
				quizDataHandler.setId(cursor.getInt(0)); 
				quizDataHandler.setBase_id(cursor.getInt(1)); 
				quizDataHandler.setDate_entry(cursor.getString(2)); 
				quizDataHandler.setLesson(cursor.getInt(3));
				quizDataHandler.setChapter(cursor.getInt(4));
				quizDataHandler.setMark(cursor.getInt(5));
				quizDataHandler.setAnswer(cursor.getString(6));
				quizDataHandler.setDummy1(cursor.getString(7));
				quizDataHandler.setDummy2(cursor.getString(8));
				quizDataHandler.setDummy3(cursor.getString(9));
				quizDataHandler.setQuestion(cursor.getString(10));
				quizDataHandler.setTitle(cursor.getString(11));
				quizDataHandler.setHint(cursor.getString(12));
				quizDataHandler.setImage(cursor.getString(13));
				userdataList.add(quizDataHandler);
			} while (cursor.moveToNext());
		}
		db.close();
		return userdataList;
	}


	/** ===================== UPDATE DATA ================ **/
	public int updateJavaQuiz(QuizHandler lessonArray) {
		SQLiteDatabase db = this.getWritableDatabase(); 
		ContentValues values = new ContentValues(); 
		values.put ( key_id, lessonArray.id);
		values.put ( key_base_id, lessonArray.base_id);
		values.put ( key_date_entry, lessonArray.date_entry);
		values.put ( key_lesson, lessonArray.lesson);
		values.put ( key_chapter, lessonArray.chapter);
		values.put ( key_mark, lessonArray.mark);
		values.put ( key_answer, lessonArray.answer);
		values.put ( key_dummy1, lessonArray.dummy1);
		values.put ( key_dummy2, lessonArray.dummy2);
		values.put ( key_dummy3, lessonArray.dummy3);
		values.put ( key_question, lessonArray.question);
		values.put ( key_title, lessonArray.title);
		values.put ( key_hint, lessonArray.hint);
		values.put ( key_image, lessonArray.image);
		return db.update( table_java_quiz, values, key_base_id + " =? " ,
				new String []{ String.valueOf(lessonArray.getId())});
	}

	/** ===================== DELETE DATA ================ **/
	public void deleteJavaQuiz(int id){
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(table_java_quiz, key_base_id + " = ? ", new String [] { String.valueOf(id) });
		db.close();
	}
	/** ===================== GET COUNT ================ **/
	public int getJavaQuizCount(){
		String countQuery = "SELECT * FROM " + table_java_quiz;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		int count = cursor.getCount();
		db.close();
		return count;
	}


	/** ================================================================================================================
	 * NETBEANS
	    ================================================================================================================*/

	private static final String table_netbeans = "netbeans_tbl"; 
	String CREATE_NETBEANS_LESSON_TABLE = "CREATE TABLE " + table_netbeans + " ( " + 
			key_id + " INTEGER PRIMARY KEY, " + 
			key_base_id + " INTEGER, " + 
			key_date_entry + " DATE, " + 
			key_chapter + " INTEGER, " + 
			key_title + " TEXT, " + 
			key_content + " TEXT, " + 
			key_referrence + " TEXT, " + 
			key_image + " TEXT, " + 
			key_chapter_title + " TEXT, " + 
			key_objective + " TEXT, " + 
			"UNIQUE ( " + key_base_id + ") ON CONFLICT REPLACE);" ;

	/** ===================== CRUD ================ **/
	public void addNetBeansLesson(LessonHandler lessonHandler) { 
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put ( key_base_id, lessonHandler.base_id);
		values.put ( key_date_entry, lessonHandler.date_entry);
		values.put ( key_chapter, lessonHandler.chapter);
		values.put ( key_title, lessonHandler.title);
		values.put ( key_content, lessonHandler.content);
		values.put ( key_referrence, lessonHandler.referrence);
		values.put ( key_image, lessonHandler.image);
		values.put	(key_chapter_title, lessonHandler.chapter_name);
		values.put	(key_objective, lessonHandler.objective);
		long i = db.insert( table_netbeans, null, values);
		db.close();
	}

	/** ===================== GET SINGLE DATA ================ **/
	public LessonHandler getNetBeansLesson (long id){
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.query( table_netbeans, new String[] { 
				key_id, key_base_id, key_date_entry, key_chapter, key_title, key_content,
				key_referrence, key_image, key_chapter_title, key_objective}, key_base_id + "=?", 
				new String[]{ String.valueOf(id) } , null, null, null, null);
		LessonHandler lesssonHandler = new LessonHandler();
		if(cursor.moveToFirst())
			lesssonHandler = new LessonHandler(
					cursor.getInt(0), 
					cursor.getInt(1), 
					cursor.getString(2), 
					cursor.getInt(3), 
					cursor.getString(4), 
					cursor.getString(5), 
					cursor.getString(6),
					cursor.getString(7),
					cursor.getString(8),
					cursor.getString(9)
					);
		db.close();
		return lesssonHandler; 
	}
	/** ===================== GET LESSON BY CHAPTER ======================**/
	public int getNetBeansLessonByChapter (int chapter){
		String selectQuery = "SELECT "+ key_base_id + " FROM " + table_netbeans +" WHERE " + key_chapter + " = " + chapter + " ORDER BY " 
				+ key_base_id + " ASC " + " LIMIT 1 " ;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		int lastTransactionID  = 0;
		if(cursor.moveToFirst()){
			lastTransactionID = cursor.getInt(0);
		}
		db.close();
		return lastTransactionID;
	}
	/** ===================== GET ALL DATA ================ **/
	public int getNetBeansNextLesson(int lesson, int chapter){
		String selectQuery = "SELECT " + key_base_id + " FROM " + table_netbeans +  " WHERE " + key_base_id + " > " + lesson + " AND " + key_chapter + " = " 
				+ chapter +  " ORDER BY " + key_base_id + " ASC LIMIT 1" ; 
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		int nextLesson = 0;
		if (cursor.moveToFirst()) {
			nextLesson = cursor.getInt(0);
		} 
		db.close();
		return nextLesson;
	}

	/** ===================== GET LATEST TRANSACTION ID ================ **/
	public int getLastNetBeansLesson (){
		String selectQuery = "SELECT "+ key_base_id+ " FROM " + table_netbeans + " ORDER BY "
				+ key_base_id + " DESC " + " LIMIT 1 " ;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		int lastTransactionID  = 0;
		if(cursor.moveToFirst()){
			lastTransactionID = cursor.getInt(0);
		}
		db.close();
		return lastTransactionID;
	}

	/** ===================== GET ALL CHAPTERS ================ **/
	public ArrayList<String> getAllNetBeansQuizChapter (){
		String selectQuery = "SELECT "+ key_chapter_title+ " FROM " + table_netbeans + " ORDER BY "
				+ key_chapter + " ASC ";
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		ArrayList<String> chapters = new ArrayList<String>();
		if(cursor.moveToFirst()){
			do {
				chapters.add(cursor.getString(0));
			} while (cursor.moveToNext());

		}
		db.close();
		return chapters;
	}


	/** ===================== GET LATEST DATA ================ **/
	public LessonHandler getLatestNetBeansLesson (){
		String selectQuery = "SELECT * FROM " + table_netbeans + " ORDER BY " 
				+ key_date_entry + " DESC " + " LIMIT 1 " ;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		LessonHandler lessonData = new LessonHandler();
		if(cursor.moveToFirst())
			lessonData = new LessonHandler(
					cursor.getInt(0), 
					cursor.getInt(1), 
					cursor.getString(2), 
					cursor.getInt(3), 
					cursor.getString(4), 
					cursor.getString(5), 
					cursor.getString(6),
					cursor.getString(7),
					cursor.getString(8),
					cursor.getString(9)
					);
		else
			return null;
		db.close();
		return lessonData;
	}

	/** ===================== GET ALL DATA ================ **/
	public LessonHandler getNetBeansIDandChapter(int lesson, int chapter){
		LessonHandler userdataHandlerArray = null;
		String selectQuery = "SELECT * FROM " + table_netbeans +  " WHERE " + key_base_id + " = " + lesson + " AND " + key_chapter + " = " 
				+ chapter +   " ORDER BY " + key_date_entry; 
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			userdataHandlerArray = new LessonHandler();
			userdataHandlerArray.setId(cursor.getInt(0)); 
			userdataHandlerArray.setBase_id(cursor.getInt(1)); 
			userdataHandlerArray.setDate_entry(cursor.getString(2)); 
			userdataHandlerArray.setChapter(cursor.getInt(3));
			userdataHandlerArray.setTitle(cursor.getString(4));
			userdataHandlerArray.setContent(cursor.getString(5));
			userdataHandlerArray.setReferrence(cursor.getString(6));
			userdataHandlerArray.setImage(cursor.getString(7));
			userdataHandlerArray.setChapter_name(cursor.getString(8));
			userdataHandlerArray.setObjective(cursor.getString(9));
		} 
		db.close();
		return userdataHandlerArray;
	}
	/** ===================== GET ALL DATA ================ **/
	public int getMaxValueNetBeans( int chapter){
		//		SELECT MAX(Price) AS HighestPrice FROM Products;
		String selectQuery = "SELECT MAX ( " + key_base_id + " )FROM " + table_netbeans +  " WHERE "  + key_chapter + " = " 
				+ chapter +   " ORDER BY " + key_date_entry; 
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		boolean isThisLast = false;
		int maxValue = 0;
		if (cursor.moveToFirst()) {
			maxValue = cursor.getInt(0);
		} 
		db.close();
		return maxValue;
	}
	/** ===================== GET ALL DATA ================ **/
	public ArrayList< LessonHandler >getAllNetBeansLessons (){
		ArrayList< LessonHandler> userdataList = new ArrayList< LessonHandler>();
		String selectQuery = "SELECT * FROM " + table_netbeans + " ORDER BY " + key_date_entry; 
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				LessonHandler userdataHandlerArray = new LessonHandler();
				userdataHandlerArray.setId(cursor.getInt(0)); 
				userdataHandlerArray.setBase_id(cursor.getInt(1)); 
				userdataHandlerArray.setDate_entry(cursor.getString(2)); 
				userdataHandlerArray.setChapter(cursor.getInt(3));
				userdataHandlerArray.setTitle(cursor.getString(4));
				userdataHandlerArray.setContent(cursor.getString(5));
				userdataHandlerArray.setReferrence(cursor.getString(6));
				userdataHandlerArray.setImage(cursor.getString(7));
				userdataHandlerArray.setChapter_name(cursor.getString(8));
				userdataHandlerArray.setObjective(cursor.getString(9));
			} while (cursor.moveToNext());
		}
		db.close();
		return userdataList;
	}

	/** ===================== UPDATE DATA ================ **/
	public int updateNetBeansLesson(LessonHandler lessonArray) {
		SQLiteDatabase db = this.getWritableDatabase(); 
		ContentValues values = new ContentValues(); 
		values.put ( key_id, lessonArray.id);
		values.put ( key_base_id, lessonArray.base_id);
		values.put ( key_date_entry, lessonArray.date_entry);
		values.put ( key_chapter, lessonArray.chapter);
		values.put ( key_title, lessonArray.title);
		values.put ( key_content, lessonArray.content);
		values.put ( key_referrence, lessonArray.referrence);
		values.put ( key_image, lessonArray.image);
		values.put ( key_chapter_title, lessonArray.chapter_name);
		values.put ( key_objective, lessonArray.objective);
		return db.update( table_netbeans, values, key_base_id + " =? " ,
				new String []{ String.valueOf(lessonArray.getId())});
	}

	/** ===================== DELETE DATA ================ **/
	public void deleteNetBeansLesson(int id){
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(table_netbeans, key_base_id + " = ? ", new String [] { String.valueOf(id) });
		db.close();
	}
	/** ===================== GET COUNT ================ **/
	public int getNetBeansLessonCount(){
		String countQuery = "SELECT * FROM " + table_netbeans;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		int count = cursor.getCount();
		db.close();
		return count;
	}

	/**============== QUIZ TABLE ==============**/
	private static final String table_netbeans_quiz = "netbeans_quiz_tbl"; 



	String CREATE_NETBEANS_QUIZ_TABLE = "CREATE TABLE " + table_netbeans_quiz + " ( " + 
			key_id + " INTEGER PRIMARY KEY, " + 
			key_base_id + " INTEGER, " + 
			key_date_entry + " DATE, " + 
			key_lesson + " INTEGER, " + 
			key_chapter + " INTEGER, " + 
			key_mark + " INTEGER, " + 
			key_answer + " TEXT, " + 
			key_dummy1 + " TEXT, " + 
			key_dummy2 + " TEXT, " + 
			key_dummy3 + " TEXT, " + 
			key_question + " TEXT, " + 
			key_title + " TEXT, " + 
			key_hint + " TEXT, " + 
			key_image + " TEXT, " + 
			"UNIQUE ( " + key_base_id + ") ON CONFLICT REPLACE);" ;



	/** ===================== CRUD ================ **/
	public int addNetBeansQuiz(QuizHandler quizData) { 
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put ( key_base_id, quizData.base_id);
		values.put ( key_date_entry, quizData.date_entry);
		values.put ( key_lesson, quizData.lesson);
		values.put ( key_chapter, quizData.chapter);
		values.put ( key_mark, quizData.mark);
		values.put ( key_answer, quizData.answer);
		values.put ( key_dummy1, quizData.dummy1);
		values.put ( key_dummy2, quizData.dummy2);
		values.put ( key_dummy3, quizData.dummy3);
		values.put ( key_question, quizData.question);
		values.put ( key_title, quizData.title);
		values.put ( key_hint, quizData.hint);
		values.put ( key_image, quizData.image);
		long i = db.insert( table_netbeans_quiz, null, values);
		db.close();
		return (int) i;
	}

	/** ===================== GET SINGLE DATA ================ **/
	public QuizHandler getNetBeansQuiz (long id){
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.query( table_netbeans_quiz, new String[] { 
				key_id, key_base_id, key_date_entry, key_lesson,  key_chapter, key_mark, key_answer, key_dummy1,
				key_dummy2, key_dummy3, key_question, key_title, key_hint, key_image}, key_base_id + "=?", 
				new String[]{ String.valueOf(id) } , null, null, null, null);
		QuizHandler quizHandler = new QuizHandler();
		if(cursor.moveToFirst())
			quizHandler = new QuizHandler(
					cursor.getInt(0), 
					cursor.getInt(1), 
					cursor.getString(2), 
					cursor.getInt(3), 
					cursor.getInt(4), 
					cursor.getInt(5), 
					cursor.getString(6),
					cursor.getString(7),
					cursor.getString(8),
					cursor.getString(9),
					cursor.getString(10),
					cursor.getString(11),
					cursor.getString(12),
					cursor.getString(13)
					);
		db.close();
		return quizHandler; 
	}

	/** ===================== GET LATEST TRANSACTION ID ================ **/
	public int getNetBeansLastQuiz (){
		String selectQuery = "SELECT "+ key_base_id+ " FROM " + table_netbeans_quiz + " ORDER BY "
				+ key_base_id + " DESC " + " LIMIT 1 " ;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		int lastTransactionID  = 0;
		if(cursor.moveToFirst()){
			lastTransactionID = cursor.getInt(0);
		}
		db.close();
		return lastTransactionID;
	}

	/** ===================== GET LATEST DATA ================ **/
	public QuizHandler getNetBeansLatestQuiz (){
		String selectQuery = "SELECT * FROM " + table_netbeans_quiz + " ORDER BY " 
				+ key_date_entry + " DESC " + " LIMIT 1 " ;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		QuizHandler quizData = new QuizHandler();
		if(cursor.moveToFirst())
			quizData = new QuizHandler(
					cursor.getInt(0), 
					cursor.getInt(1), 
					cursor.getString(2), 
					cursor.getInt(3), 
					cursor.getInt(4), 
					cursor.getInt(5), 
					cursor.getString(6),
					cursor.getString(7),
					cursor.getString(8),
					cursor.getString(9),
					cursor.getString(10),
					cursor.getString(11),
					cursor.getString(12),
					cursor.getString(13)
					);
		else
			return null;
		db.close();
		return quizData;
	}


	/** ===================== GET ALL DATA ================ **/
	public ArrayList< QuizHandler >getNetBeansAllQuiz (){
		ArrayList< QuizHandler> userdataList = new ArrayList< QuizHandler>();
		String selectQuery = "SELECT * FROM " + table_netbeans_quiz + " ORDER BY " + key_date_entry; 
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		Log.i(TAG, "cursor count " + cursor.getCount());
		if (cursor.moveToFirst()) {
			do {
				QuizHandler quizDataHandler = new QuizHandler();
				quizDataHandler.setId(cursor.getInt(0)); 
				quizDataHandler.setBase_id(cursor.getInt(1)); 
				quizDataHandler.setDate_entry(cursor.getString(2)); 
				quizDataHandler.setLesson(cursor.getInt(3));
				quizDataHandler.setChapter(cursor.getInt(4));
				quizDataHandler.setMark(cursor.getInt(5));
				quizDataHandler.setAnswer(cursor.getString(6));
				quizDataHandler.setDummy1(cursor.getString(7));
				quizDataHandler.setDummy2(cursor.getString(8));
				quizDataHandler.setDummy3(cursor.getString(9));
				quizDataHandler.setQuestion(cursor.getString(10));
				quizDataHandler.setTitle(cursor.getString(11));
				quizDataHandler.setHint(cursor.getString(12));
				quizDataHandler.setImage(cursor.getString(13));
				userdataList.add(quizDataHandler);
			} while (cursor.moveToNext());
		}
		db.close();
		return userdataList;
	}

	/** ===================== GET ALL QUIZ BY CHAPTER ================ **/
	public ArrayList< QuizHandler >getNetBeansAllQuizByChapter (int chapter){
		ArrayList< QuizHandler> userdataList = null;
		String selectQuery = "SELECT * FROM " + table_netbeans_quiz  + " WHERE " +key_chapter + " = " + chapter + " ORDER BY " + key_date_entry + " LIMIT 10"; 
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		Log.i(TAG, "cursor count " + cursor.getCount());
		if (cursor.moveToFirst()) {
			userdataList = new ArrayList<QuizHandler>();
			do {
				QuizHandler quizDataHandler = new QuizHandler();
				quizDataHandler.setId(cursor.getInt(0)); 
				quizDataHandler.setBase_id(cursor.getInt(1)); 
				quizDataHandler.setDate_entry(cursor.getString(2)); 
				quizDataHandler.setLesson(cursor.getInt(3));
				quizDataHandler.setChapter(cursor.getInt(4));
				quizDataHandler.setMark(cursor.getInt(5));
				quizDataHandler.setAnswer(cursor.getString(6));
				quizDataHandler.setDummy1(cursor.getString(7));
				quizDataHandler.setDummy2(cursor.getString(8));
				quizDataHandler.setDummy3(cursor.getString(9));
				quizDataHandler.setQuestion(cursor.getString(10));
				quizDataHandler.setTitle(cursor.getString(11));
				quizDataHandler.setHint(cursor.getString(12));
				quizDataHandler.setImage(cursor.getString(13));
				userdataList.add(quizDataHandler);
			} while (cursor.moveToNext());
		}
		db.close();
		return userdataList;
	}

	/** ===================== UPDATE DATA ================ **/
	public int updateNetBeansQuiz(QuizHandler lessonArray) {
		SQLiteDatabase db = this.getWritableDatabase(); 
		ContentValues values = new ContentValues(); 
		values.put ( key_id, lessonArray.id);
		values.put ( key_base_id, lessonArray.base_id);
		values.put ( key_date_entry, lessonArray.date_entry);
		values.put ( key_lesson, lessonArray.lesson);
		values.put ( key_chapter, lessonArray.chapter);
		values.put ( key_mark, lessonArray.mark);
		values.put ( key_answer, lessonArray.answer);
		values.put ( key_dummy1, lessonArray.dummy1);
		values.put ( key_dummy2, lessonArray.dummy2);
		values.put ( key_dummy3, lessonArray.dummy3);
		values.put ( key_question, lessonArray.question);
		values.put ( key_title, lessonArray.title);
		values.put ( key_hint, lessonArray.hint);
		values.put ( key_image, lessonArray.image);
		return db.update( table_netbeans_quiz, values, key_base_id + " =? " ,
				new String []{ String.valueOf(lessonArray.getId())});
	}

	/** ===================== DELETE DATA ================ **/
	public void deleteNetBeansQuiz(int id){
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(table_netbeans_quiz, key_base_id + " = ? ", new String [] { String.valueOf(id) });
		db.close();
	}
	/** ===================== GET COUNT ================ **/
	public int getNetBeansQuizCount(){
		String countQuery = "SELECT * FROM " + table_netbeans_quiz;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		int count = cursor.getCount();
		db.close();
		return count;
	}

	/** =================== DICTIONARY =================== **/
	private static final String table_dictionary = "dictionary"; 

	private static final String key_meaning = "meaning";
	private static final String key_syllables = "syllables";
	private static final String key_name = "name";


	String CREATE_dictionary_TABLE = "CREATE TABLE " + table_dictionary+ " ( " + 
			key_id + " INTEGER PRIMARY KEY, " + 
			key_base_id + " INTEGER, " + 
			key_date_entry + " TEXT, " + 
			key_meaning + " TEXT, " + 
			key_syllables + " TEXT, " + 
			key_name + " TEXT, " + 
			"UNIQUE ( " + key_base_id + ") ON CONFLICT REPLACE);" ;

	/** ===================== CRUD ================ **/
	public void addDictionary(DictionaryHandler dictionaryHandlerArray) { 
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put ( key_base_id, dictionaryHandlerArray.base_id);
		values.put ( key_date_entry, dictionaryHandlerArray.date_entry);
		values.put ( key_meaning, dictionaryHandlerArray.meaning);
		values.put ( key_syllables, dictionaryHandlerArray.syllables);
		values.put ( key_name, dictionaryHandlerArray.name);
		db.insert( table_dictionary, null, values);
		db.close();
	}
	/** ===================== GET SINGLE DATA ================ **/
	public DictionaryHandler getDictionary (long id){
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.query( table_dictionary, new String[] { 
				key_id, key_base_id, key_date_entry, key_meaning, key_syllables, key_name}, key_base_id + "=?", 
				new String[]{ String.valueOf(id) } , null, null, null, null);
		DictionaryHandler dictionaryHandlerArray = new DictionaryHandler();
		if(cursor.moveToFirst())
			dictionaryHandlerArray = new DictionaryHandler(
					cursor.getInt(0), 
					cursor.getInt(1), 
					cursor.getString(2), 
					cursor.getString(3), 
					cursor.getString(4), 
					cursor.getString(5)
					);
		db.close();
		return dictionaryHandlerArray; 
	}

	/** ===================== GET LATEST DATA ================ **/
	public DictionaryHandler getLatestDictionary (){
		String selectQuery = "SELECT * FROM " + table_dictionary + " ORDER BY " 
				+ key_date_entry + " DESC " + " LIMIT 1 " ;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		DictionaryHandler dictionaryHandlerArray = new DictionaryHandler();
		if(cursor.moveToFirst())
			dictionaryHandlerArray = new DictionaryHandler(
					cursor.getInt(0), 
					cursor.getInt(1), 
					cursor.getString(2), 
					cursor.getString(3), 
					cursor.getString(4), 
					cursor.getString(5)
					);
		else
			return null;
		db.close();
		return dictionaryHandlerArray;
	}

	/** ===================== GET ALL DATA ================ **/
	public ArrayList< DictionaryHandler >getAllDictionary (){
		ArrayList< DictionaryHandler> dictionaryList = new ArrayList< DictionaryHandler>();
		String selectQuery = "SELECT * FROM " + table_dictionary + " ORDER BY " + key_date_entry; 
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				DictionaryHandler dictionaryHandlerArray = new DictionaryHandler();
				dictionaryHandlerArray.setId(cursor.getInt(0)); 
				dictionaryHandlerArray.setBase_id(cursor.getInt(1)); 
				dictionaryHandlerArray.setDate_entry(cursor.getString(2)); 
				dictionaryHandlerArray.setMeaning(cursor.getString(3)); 
				dictionaryHandlerArray.setSyllables(cursor.getString(4)); 
				dictionaryHandlerArray.setName(cursor.getString(5)); 
				dictionaryList.add(dictionaryHandlerArray);
			} while (cursor.moveToNext());
		}

		db.close();
		return dictionaryList;
	}
	/** ===================== UPDATE DATA ================ **/
	public int updateDictionary(DictionaryHandler dictionaryHandlerArray) {
		SQLiteDatabase db = this.getWritableDatabase(); 
		ContentValues values = new ContentValues(); 
		values.put ( key_id, dictionaryHandlerArray.id);
		values.put ( key_base_id, dictionaryHandlerArray.base_id);
		values.put ( key_date_entry, dictionaryHandlerArray.date_entry);
		values.put ( key_meaning, dictionaryHandlerArray.meaning);
		values.put ( key_syllables, dictionaryHandlerArray.syllables);
		values.put ( key_name, dictionaryHandlerArray.name);
		return db.update( table_dictionary, values, key_base_id + " =? " ,
				new String []{ String.valueOf(dictionaryHandlerArray.getId())});
	}
	/** ===================== DELETE DATA ================ **/
	public void deleteDictionary(int id){
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(table_dictionary, key_id + " = ? ", new String [] { String.valueOf(id) });
		db.close();
	}
	/** ===================== GET COUNT ================ **/
	public int getDictionaryCount(){
		String countQuery = "SELECT * FROM " + table_dictionary;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		int count = cursor.getCount();
		cursor.close();
		return count;
	}

	/** ================== INSTRUCTIONS ================**/
	private static final String table_instruction = "instruction"; 
	private static final String key_header = "header";


	String CREATE_instruction_TABLE = "CREATE TABLE " + table_instruction+ " ( " + 
			key_id + " INTEGER PRIMARY KEY, " + 
			key_base_id + " INTEGER, " + 
			key_date_entry + " TEXT, " + 
			key_header + " TEXT, " + 
			key_content + " TEXT, " + 
			key_title + " TEXT, " + 
			"UNIQUE ( " + key_base_id + ") ON CONFLICT REPLACE);" ;


	/** ===================== CRUD ================ **/
	public void addInstruction(InstructionHandler instructionHandlerArray) { 
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put ( key_base_id, instructionHandlerArray.base_id);
		values.put ( key_date_entry, instructionHandlerArray.date_entry);
		values.put ( key_header, instructionHandlerArray.header);
		values.put ( key_content, instructionHandlerArray.content);
		values.put ( key_title, instructionHandlerArray.title);
		db.insert( table_instruction, null, values);
		db.close();
	}
	/** ===================== GET SINGLE DATA ================ **/
	public InstructionHandler getInstruction (long id){
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.query( table_instruction, new String[] { 
				key_id, key_base_id, key_date_entry, key_header, key_content, key_title}, key_base_id + "=?", 
				new String[]{ String.valueOf(id) } , null, null, null, null);
		InstructionHandler instructionHandlerArray = new InstructionHandler();
		if(cursor.moveToFirst())
			instructionHandlerArray = new InstructionHandler(
					cursor.getInt(0), 
					cursor.getInt(1), 
					cursor.getString(2), 
					cursor.getString(3), 
					cursor.getString(4), 
					cursor.getString(5)
					);
		db.close();
		return instructionHandlerArray; 
	}

	/** ===================== GET LATEST DATA ================ **/
	public InstructionHandler getLatestInstruction (){
		String selectQuery = "SELECT * FROM " + table_instruction + " ORDER BY " 
				+ key_date_entry + " DESC " + " LIMIT 1 " ;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		InstructionHandler instructionHandlerArray = new InstructionHandler();
		if(cursor.moveToFirst())
			instructionHandlerArray = new InstructionHandler(
					cursor.getInt(0), 
					cursor.getInt(1), 
					cursor.getString(2), 
					cursor.getString(3), 
					cursor.getString(4), 
					cursor.getString(5)
					);
		else
			return null;
		db.close();
		return instructionHandlerArray;
	}

	/** ===================== GET ALL DATA ================ **/
	public ArrayList< InstructionHandler >getAllInstruction (){
		ArrayList< InstructionHandler> instructionList = new ArrayList< InstructionHandler>();
		String selectQuery = "SELECT * FROM " + table_instruction + " ORDER BY " + key_date_entry; 
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				InstructionHandler instructionHandlerArray = new InstructionHandler();
				instructionHandlerArray.setId(cursor.getInt(0)); 
				instructionHandlerArray.setBase_id(cursor.getInt(1)); 
				instructionHandlerArray.setDate_entry(cursor.getString(2)); 
				instructionHandlerArray.setHeader(cursor.getString(3)); 
				instructionHandlerArray.setContent(cursor.getString(4)); 
				instructionHandlerArray.setTitle(cursor.getString(5)); 
				instructionList.add(instructionHandlerArray);
			} while (cursor.moveToNext());
		}
		db.close();
		return instructionList;
	}
	/** ===================== UPDATE DATA ================ **/
	public int updateInstruction(InstructionHandler instructionHandlerArray) {
		SQLiteDatabase db = this.getWritableDatabase(); 
		ContentValues values = new ContentValues(); 
		values.put ( key_id, instructionHandlerArray.id);
		values.put ( key_base_id, instructionHandlerArray.base_id);
		values.put ( key_date_entry, instructionHandlerArray.date_entry);
		values.put ( key_header, instructionHandlerArray.header);
		values.put ( key_content, instructionHandlerArray.content);
		values.put ( key_title, instructionHandlerArray.title);
		return db.update( table_instruction, values, key_base_id + " =? " ,
				new String []{ String.valueOf(instructionHandlerArray.getId())});
	}
	/** ===================== DELETE DATA ================ **/
	public void deleteInstruction(int id){
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(table_instruction, key_id + " = ? ", new String [] { String.valueOf(id) });
		db.close();
	}
	/** ===================== GET COUNT ================ **/
	public int getInstructionCount(){
		String countQuery = "SELECT * FROM " + table_instruction;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		cursor.close();
		int count =  cursor.getCount();
		return count;
	}


	/** ========================== QUIZ RESULT TABLE ==========================**/
	private static final String table_quiz_result = "quiz_result"; 
	private static final String key_quiz = "quiz";
	private static final String key_result = "result";


	String CREATE_quiz_result_TABLE = "CREATE TABLE " + table_quiz_result+ " ( " + 
			key_id + " INTEGER PRIMARY KEY, " + 
			key_base_id + " INTEGER, " + 
			key_date_entry + " TEXT, " + 
			key_quiz + " INTEGER, " + 
			key_answer + " TEXT, " + 
			key_result + " INTEGER );" ;

	/** ===================== CRUD ================ **/
	public void addQuiz_result(Quiz_resultHandler quiz_resultHandlerArray) { 
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put ( key_base_id, quiz_resultHandlerArray.base_id);
		values.put ( key_date_entry, quiz_resultHandlerArray.date_entry);
		values.put ( key_quiz, quiz_resultHandlerArray.quiz);
		values.put ( key_answer, quiz_resultHandlerArray.answer);
		values.put ( key_result, quiz_resultHandlerArray.result);
		db.insert( table_quiz_result, null, values);
		db.close();
	}
	/** ===================== GET SINGLE DATA ================ **/
	public Quiz_resultHandler getQuiz_result (long id){
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.query( table_quiz_result, new String[] { 
				key_id, key_base_id, key_date_entry, key_quiz, key_answer, key_result}, key_id + "=?", 
				new String[]{ String.valueOf(id) } , null, null, null, null);
		Quiz_resultHandler quiz_resultHandlerArray = new Quiz_resultHandler();
		if(cursor.moveToFirst())
			quiz_resultHandlerArray = new Quiz_resultHandler(
					cursor.getInt(0), 
					cursor.getInt(1), 
					cursor.getString(2), 
					cursor.getInt(3), 
					cursor.getString(4), 
					cursor.getInt(5)
					);
		db.close();
		return quiz_resultHandlerArray; 
	}

	/** ===================== GET LATEST DATA ================ **/
	public Quiz_resultHandler getLatestQuiz_result (){
		String selectQuery = "SELECT * FROM " + table_quiz_result + " ORDER BY " 
				+ key_date_entry + " DESC " + " LIMIT 1 " ;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		Quiz_resultHandler quiz_resultHandlerArray = new Quiz_resultHandler();
		if(cursor.moveToFirst())
			quiz_resultHandlerArray = new Quiz_resultHandler(
					cursor.getInt(0), 
					cursor.getInt(1), 
					cursor.getString(2), 
					cursor.getInt(3), 
					cursor.getString(4), 
					cursor.getInt(5)
					);
		db.close();
		return quiz_resultHandlerArray;
	}

	/** ===================== GET ALL DATA ================ **/
	public ArrayList< Quiz_resultHandler >getAllQuiz_result (){
		ArrayList< Quiz_resultHandler> quiz_resultList = new ArrayList< Quiz_resultHandler>();
		String selectQuery = "SELECT * FROM " + table_quiz_result + " ORDER BY " + key_date_entry; 
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				Quiz_resultHandler quiz_resultHandlerArray = new Quiz_resultHandler();
				quiz_resultHandlerArray.setId(cursor.getInt(0)); 
				quiz_resultHandlerArray.setBase_id(cursor.getInt(1)); 
				quiz_resultHandlerArray.setDate_entry(cursor.getString(2)); 
				quiz_resultHandlerArray.setQuiz(cursor.getInt(3)); 
				quiz_resultHandlerArray.setAnswer(cursor.getString(4)); 
				quiz_resultHandlerArray.setResult(cursor.getInt(5)); 
				quiz_resultList.add(quiz_resultHandlerArray);
			} while (cursor.moveToNext());
		}

		db.close();
		return quiz_resultList;
	}
	/** ===================== UPDATE DATA ================ **/
	public int updateQuiz_result(Quiz_resultHandler quiz_resultHandlerArray) {
		SQLiteDatabase db = this.getWritableDatabase(); 
		ContentValues values = new ContentValues(); 
		values.put ( key_id, quiz_resultHandlerArray.id);
		values.put ( key_base_id, quiz_resultHandlerArray.base_id);
		values.put ( key_date_entry, quiz_resultHandlerArray.date_entry);
		values.put ( key_quiz, quiz_resultHandlerArray.quiz);
		values.put ( key_answer, quiz_resultHandlerArray.answer);
		values.put ( key_result, quiz_resultHandlerArray.result);
		return db.update( table_quiz_result, values, key_base_id + " =? " ,
				new String []{ String.valueOf(quiz_resultHandlerArray.getId())});
	}
	/** ===================== DELETE DATA ================ **/
	public void deleteQuiz_result(int id){
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(table_quiz_result, key_id + " = ? ", new String [] { String.valueOf(id) });
		db.close();
	}
	/** ===================== GET COUNT ================ **/
	public int getQuiz_resultCount(){
		String countQuery = "SELECT * FROM " + table_quiz_result;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		int count = cursor.getCount();
		db.close();
		return count;
	}

	/** ===================== CHAPTER ======================= **/

	private static final String table_java_chapter = "java_chapter"; 

	private static final String key_chatper_title = "chatper_title";
	private static final String key_chapter_no = "chapter_no";


	String CREATE_JAVA_CHAPTER_TABLE = "CREATE TABLE " + table_java_chapter+ " ( " + 
			key_id + " INTEGER PRIMARY KEY, " + 
			key_base_id + " INTEGER, " + 
			key_date_entry + " TEXT, " + 
			key_chatper_title + " TEXT, " + 
			key_chapter_no + " INTEGER, " + 
			key_objective + " TEXT, " + 
			"UNIQUE ( " + key_base_id + ") ON CONFLICT REPLACE);" ;


	/** ===================== CRUD ================ **/
	public void addJavaChapter(ChapterHandler chapterHandlerArray) { 
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put ( key_base_id, chapterHandlerArray.base_id);
		values.put ( key_date_entry, chapterHandlerArray.date_entry);
		values.put ( key_chatper_title, chapterHandlerArray.chatper_title);
		values.put ( key_chapter_no, chapterHandlerArray.chapter_no);
		values.put ( key_objective, chapterHandlerArray.objective);
		db.insert( table_java_chapter, null, values);
		db.close();
	}
	/** ===================== GET SINGLE DATA ================ **/
	public ChapterHandler getJavaChapter (long id){
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.query( table_java_chapter, new String[] { 
				key_id, key_base_id, key_date_entry, key_chatper_title, key_chapter_no, key_objective}, key_base_id + "=?", 
				new String[]{ String.valueOf(id) } , null, null, null, null);
		if(cursor != null)
			cursor.moveToFirst();
		ChapterHandler chapterHandlerArray = new ChapterHandler(
				cursor.getInt(0), 
				cursor.getInt(1), 
				cursor.getString(2), 
				cursor.getString(3), 
				cursor.getInt(4),
				cursor.getString(5)
				);
		db.close();
		return chapterHandlerArray; 
	}

	/** ===================== GET LATEST DATA ================ **/
	public ChapterHandler getJavaLatestChapter (){
		String selectQuery = "SELECT * FROM " + table_java_chapter + " ORDER BY " 
				+ key_date_entry + " DESC " + " LIMIT 1 " ;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if(cursor.getCount() == 0)
			return null;
		cursor.moveToFirst();
		ChapterHandler chapterHandlerArray = new ChapterHandler(
				cursor.getInt(0), 
				cursor.getInt(1), 
				cursor.getString(2), 
				cursor.getString(3), 
				cursor.getInt(4),
				cursor.getString(5)
				);
		db.close();
		return chapterHandlerArray;
	}

	/** ===================== GET ALL DATA ================ **/
	public ArrayList< ChapterHandler >getJavaAllChapter (){
		ArrayList< ChapterHandler> chapterList = new ArrayList< ChapterHandler>();
		String selectQuery = "SELECT * FROM " + table_java_chapter + " ORDER BY " + key_base_id + " ASC"; 
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				ChapterHandler chapterHandlerArray = new ChapterHandler();
				chapterHandlerArray.setId(cursor.getInt(0)); 
				chapterHandlerArray.setBase_id(cursor.getInt(1)); 
				chapterHandlerArray.setDate_entry(cursor.getString(2)); 
				chapterHandlerArray.setChatper_title(cursor.getString(3)); 
				chapterHandlerArray.setChapter_no(cursor.getInt(4)); 
				chapterHandlerArray.setObjective(cursor.getString(5)); 
				chapterList.add(chapterHandlerArray);
			} while (cursor.moveToNext());
		}

		db.close();
		return chapterList;
	}
	/** ===================== UPDATE DATA ================ **/
	public int updateJavaChapter(ChapterHandler chapterHandlerArray) {
		SQLiteDatabase db = this.getWritableDatabase(); 
		ContentValues values = new ContentValues(); 
		values.put ( key_id, chapterHandlerArray.id);
		values.put ( key_base_id, chapterHandlerArray.base_id);
		values.put ( key_date_entry, chapterHandlerArray.date_entry);
		values.put ( key_chatper_title, chapterHandlerArray.chatper_title);
		values.put ( key_chapter_no, chapterHandlerArray.chapter_no);
		values.put ( key_objective, chapterHandlerArray.objective);
		return db.update( table_java_chapter, values, key_base_id + " =? " ,
				new String []{ String.valueOf(chapterHandlerArray.getId())});
	}
	/** ===================== DELETE DATA ================ **/
	public void deleteJavaChapter(int id){
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(table_java_chapter, key_id + " = ? ", new String [] { String.valueOf(id) });
		db.close();
	}
	/** ===================== GET COUNT ================ **/
	public int getJavaChapterCount(){
		String countQuery = "SELECT * FROM " + table_java_chapter;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		db.close();
		return cursor.getCount();
	}



	private static final String table_netbeans_chapter = "netbeans_chapter"; 



	String CREATE_NETBEANS_CHAPTER_TABLE = "CREATE TABLE " + table_netbeans_chapter+ " ( " + 
			key_id + " INTEGER PRIMARY KEY, " + 
			key_base_id + " INTEGER, " + 
			key_date_entry + " TEXT, " + 
			key_chatper_title + " TEXT, " + 
			key_chapter_no + " INTEGER, " + 
			key_objective + " TEXT, " + 
			"UNIQUE ( " + key_base_id + ") ON CONFLICT REPLACE);" ;


	/** ===================== CRUD ================ **/
	public void addNetBeansChapter(ChapterHandler chapterHandlerArray) { 
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put ( key_base_id, chapterHandlerArray.base_id);
		values.put ( key_date_entry, chapterHandlerArray.date_entry);
		values.put ( key_chatper_title, chapterHandlerArray.chatper_title);
		values.put ( key_chapter_no, chapterHandlerArray.chapter_no);
		values.put ( key_chapter_no, chapterHandlerArray.chapter_no);
		values.put ( key_objective, chapterHandlerArray.objective);
		db.insert( table_netbeans_chapter, null, values);
		db.close();
	}
	/** ===================== GET SINGLE DATA ================ **/
	public ChapterHandler getNetBeansChapter (long id){
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.query( table_netbeans_chapter, new String[] { 
				key_id, key_base_id, key_date_entry, key_chatper_title, key_chapter_no, key_objective}, key_id + "=?", 
				new String[]{ String.valueOf(id) } , null, null, null, null);
		if(cursor != null)
			cursor.moveToFirst();
		ChapterHandler chapterHandlerArray = new ChapterHandler(
				cursor.getInt(0), 
				cursor.getInt(1), 
				cursor.getString(2), 
				cursor.getString(3), 
				cursor.getInt(4),
				cursor.getString(5)
				);
		db.close();
		return chapterHandlerArray; 
	}

	/** ===================== GET LATEST DATA ================ **/
	public ChapterHandler getNetBeansLatestChapter (){
		String selectQuery = "SELECT * FROM " + table_netbeans_chapter + " ORDER BY " 
				+ key_date_entry + " DESC " + " LIMIT 1 " ;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if(cursor.getCount() == 0)
			return null;
		cursor.moveToFirst();
		ChapterHandler chapterHandlerArray = new ChapterHandler(
				cursor.getInt(0), 
				cursor.getInt(1), 
				cursor.getString(2), 
				cursor.getString(3), 
				cursor.getInt(4),
				cursor.getString(5)
				);
		db.close();
		return chapterHandlerArray;
	}

	/** ===================== GET ALL DATA ================ **/
	public ArrayList< ChapterHandler >getNetBeansAllChapter (){
		ArrayList< ChapterHandler> chapterList = new ArrayList< ChapterHandler>();
		String selectQuery = "SELECT * FROM " + table_netbeans_chapter + " ORDER BY " + key_date_entry; 
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				ChapterHandler chapterHandlerArray = new ChapterHandler();
				chapterHandlerArray.setId(cursor.getInt(0)); 
				chapterHandlerArray.setBase_id(cursor.getInt(1)); 
				chapterHandlerArray.setDate_entry(cursor.getString(2)); 
				chapterHandlerArray.setChatper_title(cursor.getString(3)); 
				chapterHandlerArray.setChapter_no(cursor.getInt(4)); 
				chapterHandlerArray.setObjective(cursor.getString(5)); 
				chapterList.add(chapterHandlerArray);
			} while (cursor.moveToNext());
		}

		db.close();
		return chapterList;
	}
	/** ===================== UPDATE DATA ================ **/
	public int updateNetBeanshapter(ChapterHandler chapterHandlerArray) {
		SQLiteDatabase db = this.getWritableDatabase(); 
		ContentValues values = new ContentValues(); 
		values.put ( key_id, chapterHandlerArray.id);
		values.put ( key_base_id, chapterHandlerArray.base_id);
		values.put ( key_date_entry, chapterHandlerArray.date_entry);
		values.put ( key_chatper_title, chapterHandlerArray.chatper_title);
		values.put ( key_chapter_no, chapterHandlerArray.chapter_no);
		values.put ( key_objective, chapterHandlerArray.objective);
		return db.update( table_netbeans_chapter, values, key_base_id + " =? " ,
				new String []{ String.valueOf(chapterHandlerArray.getId())});
	}
	/** ===================== DELETE DATA ================ **/
	public void deleteNetBeansChapter(int id){
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(table_netbeans_chapter, key_id + " = ? ", new String [] { String.valueOf(id) });
		db.close();
	}
	/** ===================== GET COUNT ================ **/
	public int getNetBeansChapterCount(){
		String countQuery = "SELECT * FROM " + table_netbeans_chapter;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		db.close();
		return cursor.getCount();
	}



	/** =================== STATISTICS =====================**/

	private static final String table_statistics = "statistics"; 
	private static final String key_questions = "questions";
	private static final String key_percentage = "percentage";
	private static final String key_type = "type";
	private static final String key_score = "score";


	String CREATE_statistics_TABLE = "CREATE TABLE " + table_statistics+ " ( " + 
			key_id + " INTEGER PRIMARY KEY, " + 
			key_base_id + " INTEGER, " + 
			key_date_entry + " TEXT, " + 
			key_questions + " INTEGER, " + 
			key_percentage + " INTEGER, " + 
			key_type + " INTEGER, " + 
			key_score + " INTEGER );" ;

	/** ===================== CRUD ================ **/
	public void addStatistics(StatisticsHandler statisticsHandlerArray) { 
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put ( key_base_id, statisticsHandlerArray.base_id);
		values.put ( key_date_entry, statisticsHandlerArray.date_entry);
		values.put ( key_questions, statisticsHandlerArray.questions);
		values.put ( key_percentage, statisticsHandlerArray.percentage);
		values.put ( key_type, statisticsHandlerArray.type);
		values.put ( key_score, statisticsHandlerArray.score);
		db.insert( table_statistics, null, values);
		db.close();
	}
	/** ===================== GET SINGLE DATA ================ **/
	public StatisticsHandler getStatistics (long id){
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.query( table_statistics, new String[] { 
				key_id, key_base_id, key_date_entry, key_questions, key_percentage, key_type, key_score}, key_id + "=?", 
				new String[]{ String.valueOf(id) } , null, null, null, null);
		if(cursor != null)
			cursor.moveToFirst();
		StatisticsHandler statisticsHandlerArray = new StatisticsHandler(
				cursor.getInt(0), 
				cursor.getInt(1), 
				cursor.getString(2), 
				cursor.getInt(3), 
				cursor.getInt(4), 
				cursor.getInt(5), 
				cursor.getInt(6)
				);
		db.close();
		return statisticsHandlerArray; 
	}

	/** ===================== GET LATEST DATA ================ **/
	public StatisticsHandler getLatestStatistics (){
		String selectQuery = "SELECT * FROM " + table_statistics + " ORDER BY " 
				+ key_date_entry + " DESC " + " LIMIT 1 " ;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if(cursor.getCount() == 0)
			return null;
		cursor.moveToFirst();
		StatisticsHandler statisticsHandlerArray = new StatisticsHandler(
				cursor.getInt(0), 
				cursor.getInt(1), 
				cursor.getString(2), 
				cursor.getInt(3), 
				cursor.getInt(4), 
				cursor.getInt(5), 
				cursor.getInt(6)
				);
		db.close();
		return statisticsHandlerArray;
	}

	/** ===================== GET ALL DATA ================ **/
	public ArrayList< StatisticsHandler >getAllStatisticsGroupByID (){
		ArrayList< StatisticsHandler> statisticsList = new ArrayList< StatisticsHandler>();
		String selectQuery = "SELECT * FROM " + table_statistics + " ORDER BY " + key_date_entry; 
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				StatisticsHandler statisticsHandlerArray = new StatisticsHandler();
				statisticsHandlerArray.setId(cursor.getInt(0)); 
				statisticsHandlerArray.setBase_id(cursor.getInt(1)); 
				statisticsHandlerArray.setDate_entry(cursor.getString(2)); 
				statisticsHandlerArray.setQuestions(cursor.getInt(3)); 
				statisticsHandlerArray.setPercentage(cursor.getInt(4)); 
				statisticsHandlerArray.setType(cursor.getInt(5)); 
				statisticsHandlerArray.setScore(cursor.getInt(6)); 
				statisticsList.add(statisticsHandlerArray);
			} while (cursor.moveToNext());
		}

		db.close();
		return statisticsList;
	}
	/** ===================== GET ALL DATA ================ **/
	public ArrayList< StatisticsHandler >getAllStatistics (){
		ArrayList< StatisticsHandler> statisticsList = new ArrayList< StatisticsHandler>();
		String selectQuery = "SELECT * FROM " + table_statistics  + " ORDER BY " + key_date_entry; 
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				StatisticsHandler statisticsHandlerArray = new StatisticsHandler();
				statisticsHandlerArray.setId(cursor.getInt(0)); 
				statisticsHandlerArray.setBase_id(cursor.getInt(1)); 
				statisticsHandlerArray.setDate_entry(cursor.getString(2)); 
				statisticsHandlerArray.setQuestions(cursor.getInt(3)); 
				statisticsHandlerArray.setPercentage(cursor.getInt(4)); 
				statisticsHandlerArray.setType(cursor.getInt(5)); 
				statisticsHandlerArray.setScore(cursor.getInt(6)); 
				statisticsList.add(statisticsHandlerArray);
			} while (cursor.moveToNext());
		}
		db.close();
		return statisticsList;
	}


	/** ===================== GET ALL DATES ================ **/
	public StatisticsHandler getLatestStates (int chapter){
		StatisticsHandler statisticsList = new  StatisticsHandler();
		String selectQuery = "SELECT * FROM " + table_statistics + " WHERE " + key_base_id + " = " + chapter + " ORDER BY " + key_date_entry + " DESC LIMIT 1"  ; 
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			statisticsList = new StatisticsHandler(
					cursor.getInt(0), 
					cursor.getInt(1), 
					cursor.getString(2), 
					cursor.getInt(3), 
					cursor.getInt(4), 
					cursor.getInt(5), 
					cursor.getInt(6)
					);
		}
		db.close();
		return statisticsList;
	}
	/** ===================== GET CHAPTER ================ **/
	public boolean getStatsByChapterAndType (int chapter, int type){

		String selectQuery = "SELECT " + key_score + "  FROM " + table_statistics + " WHERE " + key_base_id + " = " + chapter + " AND " + key_type + " = " + type + " ORDER BY " + key_date_entry + " DESC LIMIT 1"  ; 
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		int score = 0;
		if (cursor.moveToFirst()) {
			score = cursor.getInt(0);
		}
		Log.i(TAG, "score " + score);
		boolean isPass = false;
		if(score > 0)
			isPass = true;
		db.close();
		return isPass;
	}


	/** ===================== GET CHAPTER ================ **/
	public int getStatsByChapterAndTypeCount (int chapter, int type){
		String selectQuery = "SELECT *  FROM " + table_statistics + " WHERE " + key_base_id + " = " + chapter + " AND " + key_type + " = " + type +" ORDER BY " + key_score + " DESC LIMIT 1"  ;
		Log.i(TAG, "querry : " + selectQuery);
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		int count  = cursor.getCount();
		db.close();
		return count;
	}


	/** ===================== UPDATE DATA ================ **/
	public int updateStatistics(StatisticsHandler statisticsHandlerArray) {
		SQLiteDatabase db = this.getWritableDatabase(); 
		ContentValues values = new ContentValues(); 
		values.put ( key_id, statisticsHandlerArray.id);
		values.put ( key_base_id, statisticsHandlerArray.base_id);
		values.put ( key_date_entry, statisticsHandlerArray.date_entry);
		values.put ( key_questions, statisticsHandlerArray.questions);
		values.put ( key_percentage, statisticsHandlerArray.percentage);
		values.put ( key_type, statisticsHandlerArray.type);
		values.put ( key_score, statisticsHandlerArray.score);
		return db.update( table_statistics, values, key_base_id + " =? " ,
				new String []{ String.valueOf(statisticsHandlerArray.getId())});
	}
	/** ===================== DELETE DATA ================ **/
	public void deleteStatistics(int id){
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(table_statistics, key_id + " = ? ", new String [] { String.valueOf(id) });
		db.close();
	}
	/** ===================== GET COUNT ================ **/
	public int getStatisticsCount(){
		String countQuery = "SELECT * FROM " + table_statistics;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		db.close();
		return cursor.getCount();
	}

	/** ===================== GET COUNT ================ **/
	public int getStatisticsCount(int chapter, int type){
		String countQuery = "SELECT * FROM " + table_statistics + " WHERE " + key_base_id +" = " + chapter + " AND " + key_type + " = " + type;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		int count = cursor.getCount();
		db.close();
		return count;
	}

	/** =================== SIMULATION =====================**/


	private static final String table_simulation_quiz = "simulation_quiz"; 


	String CREATE_simulation_quiz_TABLE = "CREATE TABLE " + table_simulation_quiz+ " ( " + 
			key_id + " INTEGER PRIMARY KEY, " + 
			key_base_id + " INTEGER, " + 
			key_date_entry + " TEXT, " + 
			key_answer + " TEXT, " + 
			key_question + " TEXT, " + 
			//------------//
			key_explanation + " TEXT, " +
			//------------//
			//------------//
			key_ilo + " TEXT);" ;
	
//	
//	key_ilo + " TEXT, " +
//	//------------//
//	"UNIQUE ( " + key_base_id + ") ON CONFLICT REPLACE);" ;

	/** ===================== CRUD ================ **/
	public void addSimulation_quiz(Simulation_quizHandler simulation_quizHandlerArray) { 
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put ( key_base_id, simulation_quizHandlerArray.base_id);
		values.put ( key_date_entry, simulation_quizHandlerArray.date_entry);
		values.put ( key_answer, simulation_quizHandlerArray.answer);
		values.put ( key_question, simulation_quizHandlerArray.question);
		//------------------//
		values.put ( key_explanation, simulation_quizHandlerArray.explanation);
		//------------------//
		//------------------//
		values.put ( key_ilo, simulation_quizHandlerArray.ilo);
		//------------------//

		db.insert( table_simulation_quiz, null, values);
		db.close();
	}
	/** ===================== GET SINGLE DATA ================ **/
	public Simulation_quizHandler getSimulation_quiz (long id){
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.query( table_simulation_quiz, new String[] { 
				//------//
				key_id, key_base_id, key_date_entry, key_answer, key_question, key_explanation, key_ilo}, key_base_id + "=?", 
				new String[]{ String.valueOf(id) } , null, null, null, null);
				//---//
		if(cursor != null)
			cursor.moveToFirst();
		Simulation_quizHandler simulation_quizHandlerArray = new Simulation_quizHandler(
				cursor.getInt(0), 
				cursor.getInt(1), 
				cursor.getString(2), 
				cursor.getString(3), 
				cursor.getString(4),
				//-----------//
				cursor.getString(5),
				//----------//
				//-----------//
				cursor.getString(6)
				//----------//
				);
		db.close();
		return simulation_quizHandlerArray; 
	}

	/** ===================== GET LATEST DATA ================ **/
	public Simulation_quizHandler getLatestSimulation_quiz (){
		String selectQuery = "SELECT * FROM " + table_simulation_quiz + " ORDER BY " 
				+ key_date_entry + " DESC " + " LIMIT 1 " ;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if(cursor.getCount() == 0)
			return null;
		cursor.moveToFirst();
		Simulation_quizHandler simulation_quizHandlerArray = new Simulation_quizHandler(
				cursor.getInt(0), 
				cursor.getInt(1), 
				cursor.getString(2), 
				cursor.getString(3), 
				cursor.getString(4),
				//-------//
				cursor.getString(5),
				//-------//
				//-------//
				cursor.getString(6)
				//-------//
				);
		db.close();
		return simulation_quizHandlerArray;
	}

	/** ===================== GET ALL DATA ================ **/
	public ArrayList< Simulation_quizHandler >getAllSimulation_quiz (){
		ArrayList< Simulation_quizHandler> simulation_quizList = new ArrayList< Simulation_quizHandler>();
		String selectQuery = "SELECT * FROM " + table_simulation_quiz + " ORDER BY " + key_date_entry; 
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				Simulation_quizHandler simulation_quizHandlerArray = new Simulation_quizHandler();
				simulation_quizHandlerArray.setId(cursor.getInt(0)); 
				simulation_quizHandlerArray.setBase_id(cursor.getInt(1)); 
				simulation_quizHandlerArray.setDate_entry(cursor.getString(2)); 
				simulation_quizHandlerArray.setAnswer(cursor.getString(3)); 
				simulation_quizHandlerArray.setQuestion(cursor.getString(4));
				//----------//
				simulation_quizHandlerArray.setExplanation(cursor.getString(5));
				//-----------//
				//----------//
				simulation_quizHandlerArray.setIlo(cursor.getString(6));
				//-----------//
				simulation_quizList.add(simulation_quizHandlerArray);
			} while (cursor.moveToNext());
		}

		db.close();
		return simulation_quizList;
	}
	/** ===================== GET ALL DATA ================ **/
	public ArrayList< Simulation_quizHandler >getAllSimulation_quizByChapter (int chapter){
		ArrayList< Simulation_quizHandler> simulation_quizList = new ArrayList< Simulation_quizHandler>();
		String selectQuery = "SELECT * FROM " + table_simulation_quiz + " WHERE " + key_base_id + " = " + chapter + " ORDER BY " + key_date_entry + " LIMIT 4"; 
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				Simulation_quizHandler simulation_quizHandlerArray = new Simulation_quizHandler();
				simulation_quizHandlerArray.setId(cursor.getInt(0)); 
				simulation_quizHandlerArray.setBase_id(cursor.getInt(1)); 
				simulation_quizHandlerArray.setDate_entry(cursor.getString(2)); 
				simulation_quizHandlerArray.setAnswer(cursor.getString(3)); 
				simulation_quizHandlerArray.setQuestion(cursor.getString(4));
				//----------//
				simulation_quizHandlerArray.setExplanation(cursor.getString(5));
				//-----------//
				//----------//
				simulation_quizHandlerArray.setIlo(cursor.getString(6));
				//-----------//
				simulation_quizList.add(simulation_quizHandlerArray);
			} while (cursor.moveToNext());
		}

		db.close();
		return simulation_quizList;
	}
	/** ===================== UPDATE DATA ================ **/
	public int updateSimulation_quiz(Simulation_quizHandler simulation_quizHandlerArray) {
		SQLiteDatabase db = this.getWritableDatabase(); 
		ContentValues values = new ContentValues(); 
		values.put ( key_id, simulation_quizHandlerArray.id);
		values.put ( key_base_id, simulation_quizHandlerArray.base_id);
		values.put ( key_date_entry, simulation_quizHandlerArray.date_entry);
		values.put ( key_answer, simulation_quizHandlerArray.answer);
		values.put ( key_question, simulation_quizHandlerArray.question);
		//-----------//
		values.put ( key_explanation, simulation_quizHandlerArray.explanation);
		//------//
		//-----------//
		values.put ( key_ilo, simulation_quizHandlerArray.ilo);
		//------//
		return db.update( table_simulation_quiz, values, key_base_id + " =? " ,
				new String []{ String.valueOf(simulation_quizHandlerArray.getId())});
	}
	/** ===================== DELETE DATA ================ **/
	public void deleteSimulation_quiz(int id){
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(table_simulation_quiz, key_id + " = ? ", new String [] { String.valueOf(id) });
		db.close();
	}
	/** ===================== GET COUNT ================ **/
	public int getSimulation_quizCount(){
		String countQuery = "SELECT * FROM " + table_simulation_quiz;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		int count = cursor.getCount();
		db.close();
		return count;
	}




//////////////////////////////NETBEANS SIMULATION/////////////////////
	
	/** =================== SIMULATION NETBEANS =====================**/


	private static final String table_simulation_quiz2 = "simulation_quiz2"; 


	String CREATE_simulation_quiz2_TABLE = "CREATE TABLE " + table_simulation_quiz2+ " ( " + 
			key_id + " INTEGER PRIMARY KEY, " + 
			key_base_id + " INTEGER, " + 
			key_date_entry + " TEXT, " + 
			key_answer + " TEXT, " + 
			key_question + " TEXT, " + 
			//------------//
			key_explanation + " TEXT, " +
			//------------//
			"UNIQUE ( " + key_base_id + ") ON CONFLICT REPLACE);" ;

	/** ===================== CRUD ================ **/
	public void addSimulation_quiz2(Simulation_quizHandler2 simulation_quizHandlerArray) { 
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put ( key_base_id, simulation_quizHandlerArray.base_id);
		values.put ( key_date_entry, simulation_quizHandlerArray.date_entry);
		values.put ( key_answer, simulation_quizHandlerArray.answer);
		values.put ( key_question, simulation_quizHandlerArray.question);
		//------------------//
		values.put ( key_explanation, simulation_quizHandlerArray.explanation);
		//------------------//
		db.insert( table_simulation_quiz2, null, values);
		db.close();
	}
	/** ===================== GET SINGLE DATA ================ **/
	public Simulation_quizHandler2 getSimulation_quiz2 (long id){
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.query( table_simulation_quiz2, new String[] { 
				//------//
				key_id, key_base_id, key_date_entry, key_answer, key_question, key_explanation}, key_base_id + "=?", 
				new String[]{ String.valueOf(id) } , null, null, null, null);
				//---//
		if(cursor != null)
			cursor.moveToFirst();
		Simulation_quizHandler2 simulation_quizHandlerArray = new Simulation_quizHandler2(
				cursor.getInt(0), 
				cursor.getInt(1), 
				cursor.getString(2), 
				cursor.getString(3), 
				cursor.getString(4),
				//-----------//
				cursor.getString(5)
				//----------//
				);
		db.close();
		return simulation_quizHandlerArray; 
	}

	/** ===================== GET LATEST DATA ================ **/
	public Simulation_quizHandler2 getLatestSimulation_quiz2 (){
		String selectQuery = "SELECT * FROM " + table_simulation_quiz2 + " ORDER BY " 
				+ key_date_entry + " DESC " + " LIMIT 1 " ;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if(cursor.getCount() == 0)
			return null;
		cursor.moveToFirst();
		Simulation_quizHandler2 simulation_quizHandlerArray = new Simulation_quizHandler2(
				cursor.getInt(0), 
				cursor.getInt(1), 
				cursor.getString(2), 
				cursor.getString(3), 
				cursor.getString(4),
				//-------//
				cursor.getString(5)
				//-------//
				);
		db.close();
		return simulation_quizHandlerArray;
	}

	/** ===================== GET ALL DATA ================ **/
	public ArrayList< Simulation_quizHandler2 >getAllSimulation_quiz2 (){
		ArrayList< Simulation_quizHandler2> simulation_quizList = new ArrayList< Simulation_quizHandler2>();
		String selectQuery = "SELECT * FROM " + table_simulation_quiz2 + " ORDER BY " + key_date_entry; 
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				Simulation_quizHandler2 simulation_quizHandlerArray = new Simulation_quizHandler2();
				simulation_quizHandlerArray.setId(cursor.getInt(0)); 
				simulation_quizHandlerArray.setBase_id(cursor.getInt(1)); 
				simulation_quizHandlerArray.setDate_entry(cursor.getString(2)); 
				simulation_quizHandlerArray.setAnswer(cursor.getString(3)); 
				simulation_quizHandlerArray.setQuestion(cursor.getString(4));
				//----------//
				simulation_quizHandlerArray.setExplanation(cursor.getString(5));
				//-----------//
				simulation_quizList.add(simulation_quizHandlerArray);
			} while (cursor.moveToNext());
		}

		db.close();
		return simulation_quizList;
	}
	/** ===================== UPDATE DATA ================ **/
	public int updateSimulation_quiz2(Simulation_quizHandler2 simulation_quizHandlerArray) {
		SQLiteDatabase db = this.getWritableDatabase(); 
		ContentValues values = new ContentValues(); 
		values.put ( key_id, simulation_quizHandlerArray.id);
		values.put ( key_base_id, simulation_quizHandlerArray.base_id);
		values.put ( key_date_entry, simulation_quizHandlerArray.date_entry);
		values.put ( key_answer, simulation_quizHandlerArray.answer);
		values.put ( key_question, simulation_quizHandlerArray.question);
		//-----------//
		values.put ( key_explanation, simulation_quizHandlerArray.explanation);
		//------//
		return db.update( table_simulation_quiz2, values, key_base_id + " =? " ,
				new String []{ String.valueOf(simulation_quizHandlerArray.getId())});
	}
	/** ===================== DELETE DATA ================ **/
	public void deleteSimulation_quiz2(int id){
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(table_simulation_quiz2, key_id + " = ? ", new String [] { String.valueOf(id) });
		db.close();
	}
	/** ===================== GET COUNT ================ **/
	public int getSimulation_quizCount2(){
		String countQuery = "SELECT * FROM " + table_simulation_quiz2;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		cursor.close();
		return cursor.getCount();
	}


}
