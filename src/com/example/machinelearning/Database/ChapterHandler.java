package com.example.machinelearning.Database;
public class ChapterHandler{
	private final static String TAG = "Chapter";
	int id; 
	int base_id; 
	String date_entry; 
	String chatper_title; 
	int chapter_no; 
	String objective;

	/**======================= Default Constructor ========================**/

	public ChapterHandler(){

	}

	public ChapterHandler(int id, int base_id, String date_entry, String chatper_title, int chapter_no, String objective){
		this.id = id;
		this.base_id = base_id;
		this.date_entry = date_entry;
		this.chatper_title = chatper_title;
		this.chapter_no = chapter_no;
		this.objective = objective;
	}

	public ChapterHandler( int base_id, String date_entry, String chatper_title, int chapter_no, String objective){
		this.base_id = base_id;
		this.date_entry = date_entry;
		this.chatper_title = chatper_title;
		this.chapter_no = chapter_no;
		this.objective = objective;
	}

	public int getId(){
		return id;
	}
	public void setId(int id){
		this.id = id;
	}

	public int getBase_id(){
		return base_id;
	}
	public void setBase_id(int base_id){
		this.base_id = base_id;
	}

	public String getDate_entry(){
		return date_entry;
	}
	public void setDate_entry(String date_entry){
		this.date_entry = date_entry;
	}

	public String getChatper_title(){
		return chatper_title;
	}
	public void setChatper_title(String chatper_title){
		this.chatper_title = chatper_title;
	}

	public int getChapter_no(){
		return chapter_no;
	}
	public void setChapter_no(int chapter_no){
		this.chapter_no = chapter_no;
	}

	public String getObjective() {
		return objective;
	}

	public void setObjective(String objective) {
		this.objective = objective;
	}
	
	
}