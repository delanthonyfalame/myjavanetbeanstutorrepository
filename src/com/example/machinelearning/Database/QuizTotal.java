package com.example.machinelearning.Database;

public class QuizTotal {
	int result, quiz;
	public QuizTotal(int quiz, int result) {
		this.quiz = quiz;
		this.result = result;
	}
	public int getResult() {
		return result;
	}
	public void setResult(int result) {
		this.result = result;
	}
	public int getQuiz() {
		return quiz;
	}
	public void setQuiz(int quiz) {
		this.quiz = quiz;
	}
	
	
}
