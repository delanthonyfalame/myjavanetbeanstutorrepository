package com.example.machinelearning.Database;
public class InstructionHandler{
	private final static String TAG = "Instruction";
	int id; 
	int base_id; 
	String date_entry; 
	String header; 
	String content; 
	String title; 

	/**======================= Default Constructor ========================**/

	public InstructionHandler(){

	}

	public InstructionHandler(int id, int base_id, String date_entry, String header, String content, String title){
		this.id = id;
		this.base_id = base_id;
		this.date_entry = date_entry;
		this.header = header;
		this.content = content;
		this.title = title;
	}

	public InstructionHandler( int base_id, String date_entry, String header, String content, String title){
		this.base_id = base_id;
		this.date_entry = date_entry;
		this.header = header;
		this.content = content;
		this.title = title;
	}

	public int getId(){
		return id;
	}
	public void setId(int id){
		this.id = id;
	}

	public int getBase_id(){
		return base_id;
	}
	public void setBase_id(int base_id){
		this.base_id = base_id;
	}

	public String getDate_entry(){
		return date_entry;
	}
	public void setDate_entry(String date_entry){
		this.date_entry = date_entry;
	}

	public String getHeader(){
		return header;
	}
	public void setHeader(String header){
		this.header = header;
	}

	public String getContent(){
		return content;
	}
	public void setContent(String content){
		this.content = content;
	}

	public String getTitle(){
		return title;
	}
	public void setTitle(String title){
		this.title = title;
	}
}