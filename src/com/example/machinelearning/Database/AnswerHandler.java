package com.example.machinelearning.Database;

public class AnswerHandler {
	QuizHandler quiz;
	String answer;
	
	public AnswerHandler(QuizHandler quiz, String answer) {
		this.answer = answer;
		this.quiz = quiz;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public QuizHandler getQuiz() {
		return quiz;
	}

	public void setQuiz(QuizHandler quiz) {
		this.quiz = quiz;
	}
	
	
	
}
