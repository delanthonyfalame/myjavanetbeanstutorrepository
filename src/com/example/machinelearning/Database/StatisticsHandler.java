package com.example.machinelearning.Database;

public class StatisticsHandler{
	private final static String TAG = "Statistics";
	int id; 
	int base_id; 
	String date_entry; 
	int questions; 
	int percentage; 
	int type; 
	int score; 

	/**======================= Default Constructor ========================**/

	public StatisticsHandler(){

	}

	public StatisticsHandler(int id, int base_id, String date_entry, int questions, int percentage, int type, int score){
		this.id = id;
		this.base_id = base_id;
		this.date_entry = date_entry;
		this.questions = questions;
		this.percentage = percentage;
		this.type = type;
		this.score = score;
	}

	public StatisticsHandler( int base_id, String date_entry, int questions, int percentage, int type, int score){
		this.base_id = base_id;
		this.date_entry = date_entry;
		this.questions = questions;
		this.percentage = percentage;
		this.type = type;
		this.score = score;
	}

	public int getId(){
		return id;
	}
	public void setId(int id){
		this.id = id;
	}

	public int getBase_id(){
		return base_id;
	}
	public void setBase_id(int base_id){
		this.base_id = base_id;
	}

	public String getDate_entry(){
		return date_entry;
	}
	public void setDate_entry(String date_entry){
		this.date_entry = date_entry;
	}

	public int getQuestions(){
		return questions;
	}
	public void setQuestions(int questions){
		this.questions = questions;
	}

	public int getPercentage(){
		return percentage;
	}
	public void setPercentage(int percentage){
		this.percentage = percentage;
	}

	public int getType(){
		return type;
	}
	public void setType(int type){
		this.type = type;
	}

	public int getScore(){
		return score;
	}
	public void setScore(int score){
		this.score = score;
	}
}