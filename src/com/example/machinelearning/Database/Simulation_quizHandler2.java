package com.example.machinelearning.Database;
public class Simulation_quizHandler2{
	private final static String TAG = "Simulation_quiz2";
	int id; 
	int base_id; 
	String date_entry; 
	String answer; 
	String question; 
	//--------//
	String explanation; 
	//-----//
	/**======================= Default Constructor ========================**/

	public Simulation_quizHandler2(){

	}

	public Simulation_quizHandler2(int id, int base_id, String date_entry, String answer, String question, String explanation){
		this.id = id;
		this.base_id = base_id;
		this.date_entry = date_entry;
		this.answer = answer;
		this.question = question;
		//-------//
		this.explanation = explanation;
		//----------//
	}

	public Simulation_quizHandler2( int base_id, String date_entry, String answer, String question, String explanation){
		this.base_id = base_id;
		this.date_entry = date_entry;
		this.answer = answer;
		this.question = question;
		//--//
		this.explanation = explanation;
		//---//
	}

	public int getId(){
		return id;
	}
	public void setId(int id){
		this.id = id;
	}

	public int getBase_id(){
		return base_id;
	}
	public void setBase_id(int base_id){
		this.base_id = base_id;
	}

	public String getDate_entry(){
		return date_entry;
	}
	public void setDate_entry(String date_entry){
		this.date_entry = date_entry;
	}

	public String getAnswer(){
		return answer;
	}
	public void setAnswer(String answer){
		this.answer = answer;
	}

	public String getQuestion(){
		return question;
	}
	public void setQuestion(String question){
		this.question = question;
	}
	////////////////////-------------------///////////////
	public String getExplanation(){
		return explanation;
	}
	public void setExplanation(String explanation){
		this.explanation = explanation;
	}
	////////////////----------------------////////////////
}