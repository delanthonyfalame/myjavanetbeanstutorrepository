package com.example.machinelearning.Database;

public class LessonHandler {
	int id, base_id, chapter;
	String title, content, referrence, date_entry, image, chapter_name, objective;
	public LessonHandler() {
		// TODO Auto-generated constructor stub
	}
	public LessonHandler(int id, int base_id, String date_entry, int chapter, String title, String content, String referrence, String image, String chapter_name, String objective) {
		this.id = id;
		this.base_id = base_id;
		this.chapter = chapter;
		this.title = title;
		this.content = content;
		this.referrence = referrence;
		this.date_entry = date_entry;
		this.image = image;
		this.objective =objective;
		this.chapter_name = chapter_name;
	}

	public LessonHandler(int base_id,  String date_entry, int chapter, String title, String content, String referrence, String image, String chapter_name, String objective) {
		this.base_id = base_id;
		this.chapter = chapter;
		this.objective = objective;
		this.title = title;
		this.content = content;
		this.referrence = referrence;
		this.date_entry = date_entry;
		this.image = image;
		this.chapter_name = chapter_name;
	}
	
	public String getChapter_name() {
		return chapter_name;
	}
	
	public void setChapter_name(String chapter_name) {
		this.chapter_name = chapter_name;
	}

	public int getChapter() {
		return chapter;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}


	public String getDate_entry() {
		return date_entry;
	}

	public void setDate_entry(String date_entry) {
		this.date_entry = date_entry;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getBase_id() {
		return base_id;
	}

	public void setBase_id(int base_id) {
		this.base_id = base_id;
	}
	public void setChapter(int chapter) {
		this.chapter = chapter;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getReferrence() {
		return referrence;
	}

	public void setReferrence(String referrence) {
		this.referrence = referrence;
	}
	public String getObjective() {
		return objective;
	}
	public void setObjective(String objective) {
		this.objective = objective;
	}

	
}
