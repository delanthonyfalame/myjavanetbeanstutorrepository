package com.example.machinelearning.Database;
public class Quiz_resultHandler{
	private final static String TAG = "Quiz_result";
	int id; 
	int base_id; 
	String date_entry; 
	int quiz; 
	String answer; 
	int result; 

	/**======================= Default Constructor ========================**/

	public Quiz_resultHandler(){

	}

	public Quiz_resultHandler(int id, int base_id, String date_entry, int quiz, String answer, int result){
		this.id = id;
		this.base_id = base_id;
		this.date_entry = date_entry;
		this.quiz = quiz;
		this.answer = answer;
		this.result = result;
	}

	public Quiz_resultHandler( int base_id, String date_entry, int quiz, String answer, int result){
		this.base_id = base_id;
		this.date_entry = date_entry;
		this.quiz = quiz;
		this.answer = answer;
		this.result = result;
	}

	public int getId(){
		return id;
	}
	public void setId(int id){
		this.id = id;
	}

	public int getBase_id(){
		return base_id;
	}
	public void setBase_id(int base_id){
		this.base_id = base_id;
	}

	public String getDate_entry(){
		return date_entry;
	}
	public void setDate_entry(String date_entry){
		this.date_entry = date_entry;
	}

	public int getQuiz(){
		return quiz;
	}
	public void setQuiz(int quiz){
		this.quiz = quiz;
	}

	public String getAnswer(){
		return answer;
	}
	public void setAnswer(String answer){
		this.answer = answer;
	}

	public int getResult(){
		return result;
	}
	public void setResult(int result){
		this.result = result;
	}
}