package com.example.machinelearning.Database;
public class Simulation_quizHandler{
	private final static String TAG = "Simulation_quiz";
	int id; 
	int base_id; 
	String date_entry; 
	String answer; 
	String question; 
	//--------//
	String explanation; 
	//-----//
	//--------//
	String ilo; 
		//-----//
	String answerContainer = "";
	/**======================= Default Constructor ========================**/

	public Simulation_quizHandler(){

	}

	public Simulation_quizHandler(int id, int base_id, String date_entry, String answer, String question, String explanation, String ilo){
		this.id = id;
		this.base_id = base_id;
		this.date_entry = date_entry;
		this.answer = answer;
		this.question = question;
		//-------//
		this.explanation = explanation;
		//----------//
		//-------//
				this.ilo = ilo;
				//----------/
				answerContainer = "";
	}

	public Simulation_quizHandler( int base_id, String date_entry, String answer, String question, String explanation, String ilo){
		this.base_id = base_id;
		this.date_entry = date_entry;
		this.answer = answer;
		this.question = question;
		//--//
		this.explanation = explanation;
		answerContainer = "";
		//---//
		//-------//
				this.ilo = ilo;
				//----------/
	}

	public int getId(){
		return id;
	}
	public void setId(int id){
		this.id = id;
	}

	public int getBase_id(){
		return base_id;
	}
	public void setBase_id(int base_id){
		this.base_id = base_id;
	}

	public String getDate_entry(){
		return date_entry;
	}
	public void setDate_entry(String date_entry){
		this.date_entry = date_entry;
	}

	public String getAnswer(){
		return answer;
	}
	public void setAnswer(String answer){
		this.answer = answer;
	}

	public String getQuestion(){
		return question;
	}
	public void setQuestion(String question){
		this.question = question;
	}
	////////////////////-------------------///////////////
	public String getExplanation(){
		return explanation;
	}
	public void setExplanation(String explanation){
		this.explanation = explanation;
	}
	////////////////----------------------////////////////
	////////////////////-------------------///////////////
	public String getIlo(){
		return ilo;
	}
	public void setIlo(String ilo){
		this.ilo = ilo;
	}
	////////////////----------------------////////////////

	public String getAnswerContainer() {
		return answerContainer;
	}

	public void setAnswerContainer(String answerContainer) {
		this.answerContainer = answerContainer;
	}
	
	
}