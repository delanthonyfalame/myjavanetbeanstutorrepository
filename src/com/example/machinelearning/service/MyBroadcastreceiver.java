package com.example.machinelearning.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;


public class MyBroadcastreceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent arg) {
    	Intent intent = new Intent(context, MyService.class);
    	context.startService(intent);
    	Log.i("Autostart", "started");
    }
}