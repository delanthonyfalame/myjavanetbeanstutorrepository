package com.example.machinelearning.service;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;


public class MyService extends Service{
	private static final String TAG = "RedStar Service";
	public static final long NOTIFY_INTERVAL = 10 * 1000 * 60;
	private Handler mHandler = new Handler();
	private Timer mTimer = null;

	public static ArrayList<Integer> updateNumNews =  new ArrayList<Integer>();
	public static ArrayList<Integer> updateNumDeal =  new ArrayList<Integer>();

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onCreate() {
		Log.i(TAG, "START SERVICE");
		try {
			if(mTimer != null) {
				mTimer.cancel();
			} else {
				mTimer = new Timer();
			}
			mTimer.scheduleAtFixedRate(new TimeDisplayTimerTask(), NOTIFY_INTERVAL, NOTIFY_INTERVAL);
		} catch (Exception e) {
			Log.i(TAG, "error on service class");
		}

	}
	class TimeDisplayTimerTask extends TimerTask {
		@Override
		public void run() {
			// run on another thread
			mHandler.post(new Runnable() {
				@Override
				public void run() {
					Log.i(TAG, "running");
				}
			}); 
		}
	}

	class downloadData extends AsyncTask<String, String, String>{

		protected String doInBackground(String... args) {
			return null;
		}
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
		}
	}


}