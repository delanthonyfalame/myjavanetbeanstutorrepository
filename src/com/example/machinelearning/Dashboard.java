package com.example.machinelearning;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;


public class Dashboard extends SwitchFragmentParent{
	protected static final String TAG = "Compiler";
	private View view;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.dashboard, container, false);
		// set all wi+dgets
		setWidgets();
		return view;
	}
	
	//set all buttons on click
	private void setWidgets() {
		//switch to netbeans lesson
		view.findViewById(R.id.netbeans).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				switchFrag.switchFragment(new NetBeans());
			}
		});
		//switch to java lessons
		view.findViewById(R.id.java).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				switchFrag.switchFragment(new Java());
			}
		});
		//switch to compiler
		view.findViewById(R.id.compiler).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
//				switchFrag.switchFragment(new );
				Log.i(TAG, "link to compiler");
				String url = "http://ideone.com/";
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse(url));
				startActivity(i);
			}
		});
		
		//switch to tutorials
		view.findViewById(R.id.tutorials).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				switchFrag.switchFragment(new Tutorials());
			}
		});
		//switch to  statistics
		view.findViewById(R.id.statistics).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				switchFrag.switchFragment(new Stats());
			}
		});
		//switch to search
		view.findViewById(R.id.search).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				switchFrag.switchFragment(new Search());
			}
		});
		//switch to about us
		view.findViewById(R.id.aboutus).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				switchFrag.switchFragment(new AboutUs());
			}
		});
		//switch to instruction
		view.findViewById(R.id.instructions).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				switchFrag.switchFragment(new Instructions());
			}
		});
		

	}

	
}
