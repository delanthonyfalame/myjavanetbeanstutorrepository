package com.example.machinelearning;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.machinelearning.Adapter.ChapterAdapter;
import com.example.machinelearning.Database.ChapterHandler;
import com.example.machinelearning.Database.Database;
import com.example.machinelearning.Database.LessonHandler;
import com.example.machinelearning.Utilities.GlobalVariables;

public class Java extends SwitchFragmentParent implements GlobalVariables{
	protected static final String TAG = "Java";
	private View view;
	ArrayList<ChapterHandler> javaChapters;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.java_chapter, container, false);
		db = new Database(getSherlockActivity());
		setWidgets();
		return view;
	}

	private void setWidgets() {
		javaChapters = new ArrayList<ChapterHandler>();
		javaChapters = db.getJavaAllChapter();
		for(ChapterHandler chapter : javaChapters)
			Log.i(TAG, "chapter  " + chapter.getBase_id());
		ArrayList<LessonHandler> lessons = db.getAllJavaLessons();
		Log.i(TAG, "lessons sizes " + lessons.size());

		ListView list = (ListView) view.findViewById(R.id.list);
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				int previous = position -1;
				int chapter = javaChapters.get(position).getBase_id();
				int lesson = db.getJavaLessonByChapter(chapter);
				if(previous >= 0){
					//check if pass on previous
					if(db.getStatsByChapterAndType( javaChapters.get(previous).getBase_id(), JAVA)){
						Log.i(TAG, "lesson number "   + lesson + " chapter " + chapter);
						if(lesson != 0)
							switchFrag.nextLesson(lesson, javaChapters.get(position).getBase_id() , new JavaLesson());
						else
							Toast.makeText(getActivity(), "No lesson found in this chapter", Toast.LENGTH_SHORT).show();
					}else
						Toast.makeText(getActivity(), "Sorry, you need to take the examination on your previous lesson to proceed", Toast.LENGTH_SHORT).show();
				}else{
					if(lesson != 0)
						switchFrag.nextLesson(lesson, javaChapters.get(position).getBase_id() , new JavaLesson());
					else
						Toast.makeText(getActivity(), "No lesson found in this chapter", Toast.LENGTH_SHORT).show();
				}
			}
		});
		list.setAdapter(new ChapterAdapter(getActivity(), javaChapters));
	
	}

}
