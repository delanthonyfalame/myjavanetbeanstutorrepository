package com.example.machinelearning.Utilities;
import java.util.HashMap;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;



@SuppressWarnings("unused")
public class SessionManager{
	// Shared Preferences
	SharedPreferences pref;
	SharedPreferences timePref;
	// Editor for Shared preferences
	Editor editor;
	Editor time;

	// Context
	Context _context;

	// Shared pref mode
	int PRIVATE_MODE = 0;

	// Sharedpref file name
	private static final String PREF_NAME = "Login";

	// All Shared Preferences Keys
	private static final String IS_LOGIN = "IsLoggedIn";

	// User name (make variable public to access from outside)
	public static final String KEY_USER_ID = "id";
	public static final String KEY_OUTLET_ID = "outlet";
	public static final String KEY_TIME = "id";

	private static final String TAG = "Session Manager Class";
	// Constructor
	@SuppressLint("CommitPrefEdits")
	public SessionManager(Context context){
		this._context = context;
		pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
		timePref = _context.getSharedPreferences("RedstarTimePref", PRIVATE_MODE);
		editor = pref.edit();
		time = timePref.edit();
	}

	/**
	 * Create login session
	 * */
	public void createLoginSession(int emailID, int outletID){
		Log.d(TAG, "Adding data "  + emailID + " outlet id " + outletID);
		editor.putBoolean(IS_LOGIN, true);
		editor.putInt(KEY_USER_ID, emailID);
		editor.putInt(KEY_OUTLET_ID, outletID);
		editor.commit();
//		Intent i = new Intent(_context, MainActivity.class);
//		i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//		i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//		_context.startActivity(i);
	}

	public void createTimeStamp(String timeStamp){
		time.clear();
		time.putString(KEY_TIME, timeStamp);
		Log.d(TAG, "Creating timestamp of deal " + timeStamp);
		time.commit();
	}
	/**
	 * Check login method wil check user login status
	 * If false it will redirect user to login page
	 * Else won't do anything
	 * */
	public void checkLogin(){
		if(this.isLoggedIn()){
//			Intent i = new Intent(_context, MainActivity.class);
//			Log.i(TAG, "User is already log in");
//			// get user data from session
//			i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//			i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//			_context.startActivity(i);
		}
	}
	/**
	 * Get stored session data
	 * */
	public HashMap<String, Integer> getUserDetails(){
		HashMap<String, Integer> user = new HashMap<String, Integer>();
		user.put(KEY_USER_ID, pref.getInt(KEY_USER_ID, 0));
		user.put(KEY_OUTLET_ID, pref.getInt(KEY_OUTLET_ID, 0));
		// return user
		return user;
	}

	public HashMap<String, String> getTimeStamp(){
		HashMap<String, String> timeStamp = new HashMap<String, String>();
		timeStamp.put(KEY_TIME, timePref.getString(KEY_TIME, null));
		Log.i(TAG, " timestamp : " + " deal : " + timePref.getString(KEY_TIME, null));
		return timeStamp;
	}

	/**
	 * Clear session details
	 * */
	public void logoutUser(){
		// Clearing all data from Shared Preferences
		editor.clear();
		time.clear();
		editor.putBoolean(IS_LOGIN, false);
		editor.commit();
//		Log.i(TAG, "Logout user");
//		Intent i = new Intent(_context, Login.class);
//		i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

		// Staring Login Activity
//		_context.startActivity(i);
	}
	public void clear(){
		// Clearing all data from Shared Preferences
		editor.clear();
		editor.commit();
		time.clear();
		time.commit();
		editor.putBoolean(IS_LOGIN, false);
	}

	public boolean isLoggedIn(){
		return pref.getBoolean(IS_LOGIN, false);
	}
}