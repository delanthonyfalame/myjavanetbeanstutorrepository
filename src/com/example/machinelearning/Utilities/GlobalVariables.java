package com.example.machinelearning.Utilities;

public interface GlobalVariables {
	public final static String JAVA_LESSON_URL = "http://hbascones.com/machinelearning/api/get_java_lesson/";
	public final static String JAVA_CHAPTER_URL = "http://hbascones.com/machinelearning/api/get_java_chapter/";
	public final static String NETBEANS_LESSON_URL = "http://hbascones.com/machinelearning/api/get_netbeans_lesson/";
	public final static String NETBEANS_CHAPTER_URL = "http://hbascones.com/machinelearning/api/get_netbeans_chapter/";
	public final static String JAVA_QUIZ_URL = "http://hbascones.com/machinelearning/api/get_java_quiz/";
	public final static String NETBEANS_QUIZ_URL = "http://hbascones.com/machinelearning/api/get_netbeans_quiz/";
	public final static String INSTRUCTION_URL = "http://hbascones.com/machinelearning/api/get_instruction/";
	public final static String DICTIONARY_URL = "http://hbascones.com/machinelearning/api/get_dictionary/";
	
	public final static int JAVA = 0;
	public final static int NETBEANS = 1;
	public final static int JAVA_EXC = 3;
	public final static int NETBEANS_EXC = 3;
	public static final int QUOTA = 5;
	
	
}
