package com.example.machinelearning;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;
import android.widget.Toast;

import com.example.machinelearning.Database.ChapterHandler;
import com.example.machinelearning.Database.Database;
import com.example.machinelearning.Database.DictionaryHandler;
import com.example.machinelearning.Database.InstructionHandler;
import com.example.machinelearning.Database.LessonHandler;
import com.example.machinelearning.Database.QuizHandler;
import com.example.machinelearning.JSON.JSONParser;
import com.example.machinelearning.Utilities.AlertDialogManager;
import com.example.machinelearning.Utilities.ConnectionDetector;
import com.example.machinelearning.Utilities.GlobalVariables;

public class Loading extends SwitchFragmentParent implements GlobalVariables{

	private static final String TAG = "Loading";
	public ProgressDialog pDialog = null;
	private JSONArray fetchData;
	private JSONParser jParser;
	private boolean isPause;
	//private WebView mWebView;
	AlertDialogManager alert ;


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.loading_page, container, false);

		//	 mWebView = (WebView) view.findViewById(R.id.webView1);
		//	 mWebView.getSettings().setPluginState(PluginState.ON);
		//	 mWebView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY); 
		//	 mWebView.setBackgroundColor(Color.parseColor("#000000"));
		//	 mWebView.loadUrl("file:///android_asset/y.swf");

		jParser = new JSONParser();
		db = new Database(getActivity());
		alert = new AlertDialogManager();
		cn = new ConnectionDetector(getActivity());
		setWidgets();
		return view;
	}

	private void setWidgets() {
		new CheckInternetConnection().execute();
	}



	class CheckInternetConnection extends AsyncTask<Double, Integer, Void> {
		boolean isConnection, isJavaLessonFetch, isJavaQuizFetch, isDictionaryFetch,
		isInstructionFetch, isJavaChapterFetch, isNetBeanChapterFetch, isNetbeanLessonFetch,
		isNetbeaQuizFetch;

		@Override
		protected Void doInBackground(Double... coordinates) {
			Log.i(TAG, " check internet connection");
			if(cn.isConnectingToInternet()){
				isJavaLessonFetch = getJavaLesson();
				isJavaQuizFetch = getJavaQuiz();
				isDictionaryFetch = getDictionary() ;
				isInstructionFetch = getInstruction();
				isJavaChapterFetch = getJAvaChapters();
				isNetBeanChapterFetch = getNetBeansChapters();
				isNetbeanLessonFetch = getNetBeansLesson();
				isNetbeaQuizFetch = getNetBeansQuiz();
				isConnection = true;
			}else{
				isConnection = false;
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			Log.i(TAG,  "LOADING FINISH /n lesson : " + db.getJavaLessonCount() + " quiz" + db.getJavaQuizCount());
			if(!isConnection && db.getJavaLessonCount() < 1)
				showAlertDialog(getActivity(), "Download Error", "No data found in your database.\nPlease Connect to internet to Download Data", false);
			else if(!isConnection){
				Toast.makeText(getActivity(), "No Connection found!", Toast.LENGTH_LONG).show();
				switchFrag.switchFragment(new Dashboard());
			}else{
				Toast.makeText(getActivity(), "Loading completed", Toast.LENGTH_LONG).show();
				switchFrag.switchFragment(new Dashboard());
			}
		}
	}


	void ShowAlert(String title, String content, final String positive, String neutral) {
		AlertDialog.Builder alert_box = new AlertDialog.Builder(getActivity());
		alert_box.setIcon(R.drawable.ic_launcher);
		alert_box.setMessage(content);
		alert_box.setTitle(title);
		alert_box.setPositiveButton(positive,
				new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				new CheckInternetConnection().execute();
			}
		});
		alert_box.setNeutralButton(neutral,
				new DialogInterface.OnClickListener() {


			@Override
			public void onClick(DialogInterface dialog, int which) {
				isPause = true;
				startActivity(new Intent(
						android.provider.Settings.ACTION_WIRELESS_SETTINGS));
			}
		});
		alert_box.show();
	}


	private boolean getJavaLesson() {
		boolean isFetchSuccessful = false;
		LessonHandler lastBranch =  new LessonHandler();
		lastBranch = db.getLatestJavaLesson();
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		/*** Save data to news and events database ***/
		try {
			Log.i(TAG, "get lesson");
			String newTime = "0000-00-00 00:00:00";
			if(lastBranch != null)
				newTime = lastBranch.getDate_entry().replace(" ", "/");

			params.add(new BasicNameValuePair("date_modified", newTime));
			Log.i(TAG, "params " + params.toString() );
			final String json = jParser.makeHttpRequest(JAVA_LESSON_URL, "POST", params);
			Log.i(TAG, "LESSON JSON : " + json);
			fetchData = new JSONArray(json);
			if(fetchData != null){
				for (int i = 0; i < fetchData.length(); i++) {
					JSONObject c = fetchData.getJSONObject(i);
					db.addJavaLesson(new LessonHandler(c.getInt("id"), c.getString("date_modified"), c.getInt("chapter_id"), c.getString("lesson_title"), 
							c.getString("content"), "referrence", c.getString("image"), c.getString("chapter_title"), ""));
					//JSON : [{"id":"1","chapter_id":"3","lesson_title":"Lesson I","content":"duhbw9uh89 h89wdhf8hw9h 9ughv9hevuiwdhviqu buigedvuiquiev duhbw9uh89 h89wdhf8hw9h 9ughv9hevuiwdhviqu buigedvuiquiev duhbw9uh89 h89wdhf8hw9h 9ughv9hevuiwdhviqu buigedvuiquiev duhbw9uh89 h89wdhf8hw9h 9ughv9hevuiwdhviqu buigedvuiquiev duhbw9uh89 h89wdhf8hw9h 9ughv9hevuiwdhviqu buigedvuiquiev duhbw9uh89 h89wdhf8hw9h 9ughv9hevuiwdhviqu buigedvuiquiev",
					//"image":"fcbwueivb iughduvq","date_modified":"2014-01-05 14:26:33","chapter_title":"Chapter 2"},{"id":"5","chapter_id":"1","lesson_title":"x","content":"x","image":"http:\/\/hbascones.com\/machinelearning\/uploads\/Capture23.PNG\/","date_modified":"2014-01-08 09:02:11","chapter_title":"Chapter 1"},{"id":"6","chapter_id":"1","lesson_title":"q","content":"q","image":"http:\/\/hbascones.com\/machinelearning\/uploads\/Capture24.PNG","date_modified":"2014-01-08 09:10:16","chapter_title":"Chapter 1"},{"id":"7","chapter_id":"1","lesson_title":"t","content":"t","image":"http:\/\/hbascones.com\/machinelearning\/uploads\/1376302_10201630738604758_2090767960_n.jpg","date_modified":"2014-01-08 09:14:24","chapter_title":"Chapter 1"}]
					Log.i(TAG, "data added");
					isFetchSuccessful = true;
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return isFetchSuccessful;
	}

	private boolean getJavaQuiz() {
		boolean isFetchSuccessful = false;
		QuizHandler lastQuiz =  new QuizHandler();
		lastQuiz = db.getJavaLatestQuiz();
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		/*** Save data to news and events database ***/
		try {
			Log.i(TAG, "get quiz");
			String newTime = "0000-00-00 00:00:00";
			if(lastQuiz != null)
				newTime = lastQuiz.getDate_entry().replace(" ", "/");

			params.add(new BasicNameValuePair("date_modified", newTime));
			Log.i(TAG, "params " + params.toString() );
			final String json = jParser.makeHttpRequest(JAVA_QUIZ_URL, "POST", params);
			Log.i(TAG, "QUIZ JSON : " + json);
			fetchData = new JSONArray(json);
			if(fetchData != null){
				for (int i = 0; i < fetchData.length(); i++) {
					JSONObject c = fetchData.getJSONObject(i);
					db.addJavaQuiz(new QuizHandler(c.getInt("id"), c.getString("date_modified"), 1, c.getInt("chapter_id")
							, 0, c.getString("answer"), c.getString("choices1"), c.getString("choices2"), c.getString("choices3"),
							c.getString("question"), c.getString("title"), c.getString("hint"), c.getString("image")));
					isFetchSuccessful = true;
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return isFetchSuccessful;
	}


	private boolean getNetBeansLesson() {
		boolean isFetchSuccessful = false;
		LessonHandler lastBranch =  new LessonHandler();
		lastBranch = db.getLatestNetBeansLesson();
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		/*** Save data to news and events database ***/
		try {
			Log.i(TAG, "get lesson");
			String newTime = "0000-00-00 00:00:00";
			if(lastBranch != null)
				newTime = lastBranch.getDate_entry().replace(" ", "/");

			params.add(new BasicNameValuePair("date_modified", newTime));
			Log.i(TAG, "params " + params.toString() );
			final String json = jParser.makeHttpRequest(NETBEANS_LESSON_URL, "POST", params);
			Log.i(TAG, "NETBEANS LESSON JSON : " + json);
			fetchData = new JSONArray(json);
			if(fetchData != null){
				for (int i = 0; i < fetchData.length(); i++) {
					JSONObject c = fetchData.getJSONObject(i);
					db.addNetBeansLesson(new LessonHandler(c.getInt("id"), c.getString("date_modified"), c.getInt("chapter_id"), c.getString("lesson_title"), 
							c.getString("content"), "referrence", c.getString("image"), c.getString("chapter_title"), ""));
					//JSON : [{"id":"1","chapter_id":"3","lesson_title":"Lesson I","content":"duhbw9uh89 h89wdhf8hw9h 9ughv9hevuiwdhviqu buigedvuiquiev duhbw9uh89 h89wdhf8hw9h 9ughv9hevuiwdhviqu buigedvuiquiev duhbw9uh89 h89wdhf8hw9h 9ughv9hevuiwdhviqu buigedvuiquiev duhbw9uh89 h89wdhf8hw9h 9ughv9hevuiwdhviqu buigedvuiquiev duhbw9uh89 h89wdhf8hw9h 9ughv9hevuiwdhviqu buigedvuiquiev duhbw9uh89 h89wdhf8hw9h 9ughv9hevuiwdhviqu buigedvuiquiev",
					//"image":"fcbwueivb iughduvq","date_modified":"2014-01-05 14:26:33","chapter_title":"Chapter 2"},{"id":"5","chapter_id":"1","lesson_title":"x","content":"x","image":"http:\/\/hbascones.com\/machinelearning\/uploads\/Capture23.PNG\/","date_modified":"2014-01-08 09:02:11","chapter_title":"Chapter 1"},{"id":"6","chapter_id":"1","lesson_title":"q","content":"q","image":"http:\/\/hbascones.com\/machinelearning\/uploads\/Capture24.PNG","date_modified":"2014-01-08 09:10:16","chapter_title":"Chapter 1"},{"id":"7","chapter_id":"1","lesson_title":"t","content":"t","image":"http:\/\/hbascones.com\/machinelearning\/uploads\/1376302_10201630738604758_2090767960_n.jpg","date_modified":"2014-01-08 09:14:24","chapter_title":"Chapter 1"}]
					Log.i(TAG, "data added");
					isFetchSuccessful = true;
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return isFetchSuccessful;
	}

	private boolean getNetBeansQuiz() {
		boolean isFetchSuccessful = false;
		QuizHandler lastQuiz =  new QuizHandler();
		lastQuiz = db.getNetBeansLatestQuiz();
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		/*** Save data to news and events database ***/
		try {
			Log.i(TAG, "get quiz");
			String newTime = "0000-00-00 00:00:00";
			if(lastQuiz != null)
				newTime = lastQuiz.getDate_entry().replace(" ", "/");

			params.add(new BasicNameValuePair("date_modified", newTime));
			Log.i(TAG, "params " + params.toString() );
			final String json = jParser.makeHttpRequest(NETBEANS_QUIZ_URL, "POST", params);
			Log.i(TAG, "NETBEANS QUIZ JSON : " + json);
			fetchData = new JSONArray(json);
			if(fetchData != null){
				for (int i = 0; i < fetchData.length(); i++) {
					JSONObject c = fetchData.getJSONObject(i);
					db.addNetBeansQuiz(new QuizHandler(c.getInt("id"), c.getString("date_modified"), 1, c.getInt("chapter_id")
							, 0, c.getString("answer"), c.getString("choices1"), c.getString("choices2"), c.getString("choices3"),
							c.getString("question"), c.getString("title"), c.getString("hint"), c.getString("image")));
					isFetchSuccessful = true;
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return isFetchSuccessful;
	}


	private boolean getDictionary() {
		boolean isFetchSuccessful = false;
		DictionaryHandler lastBranch =  new DictionaryHandler();
		lastBranch = db.getLatestDictionary();
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		/*** Save data to news and events database ***/
		try {
			String newTime = "0000-00-00 00:00:00";
			if(lastBranch != null)
				newTime = lastBranch.getDate_entry().replace(" ", "/");

			params.add(new BasicNameValuePair("date_modified", newTime));
			Log.i(TAG, "params " + params.toString() );
			final String json = jParser.makeHttpRequest(DICTIONARY_URL, "POST", params);
			Log.i(TAG, "JSON DICTIONARY : " + json);
			fetchData = new JSONArray(json);
			if(fetchData != null){
				for (int i = 0; i < fetchData.length(); i++) {
					JSONObject c = fetchData.getJSONObject(i);
					db.addDictionary(new DictionaryHandler(c.getInt("id"), c.getString("date_modified"),
							c.getString("meaning"), c.getString("pronounciation"), c.getString("word")));
					//					JSON DICTIONARY : [{"id":"1","word":"Word","meaning":"Meaning","pronounciation":"pronounciation","date_modified":"2014-01-05 15:55:03"}]
					Log.i(TAG, "data added");
					isFetchSuccessful = true;
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return isFetchSuccessful;
	}

	private boolean getInstruction() {
		boolean isFetchSuccessful = false;
		InstructionHandler lastBranch =  new InstructionHandler();
		lastBranch = db.getLatestInstruction();
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		/*** Save data to news and events database ***/
		try {
			String newTime = "0000-00-00 00:00:00";
			if(lastBranch != null)
				newTime = lastBranch.getDate_entry().replace(" ", "/");

			params.add(new BasicNameValuePair("date_modified", newTime));
			Log.i(TAG, "params " + params.toString() );
			final String json = jParser.makeHttpRequest(INSTRUCTION_URL, "POST", params);
			Log.i(TAG, "JSON INSTRUCTION: " + json);
			fetchData = new JSONArray(json);
			if(fetchData != null){
				for (int i = 0; i < fetchData.length(); i++) {
					JSONObject c = fetchData.getJSONObject(i);
					db.addInstruction(new InstructionHandler(c.getInt("id"), c.getString("date_modified"), c.getString("header"), 
							c.getString("content"), c.getString("title")));
					Log.i(TAG, "data added");
					isFetchSuccessful = true;
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return isFetchSuccessful;
	}


	private boolean getJAvaChapters() {
		boolean isFetchSuccessful = false;
		ChapterHandler lastBranch =  new ChapterHandler();
		lastBranch = db.getJavaLatestChapter();
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		/*** Save data to news and events database ***/
		try {
			String newTime = "0000-00-00 00:00:00";
			if(lastBranch != null)
				newTime = lastBranch.getDate_entry().replace(" ", "/");

			params.add(new BasicNameValuePair("date_modified", newTime));
			Log.i(TAG, "params " + params.toString() );
			final String json = jParser.makeHttpRequest(JAVA_CHAPTER_URL, "POST", params);
			Log.i(TAG, "JSON chapter: " + json);
			fetchData = new JSONArray(json);
			if(fetchData != null){
				for (int i = 0; i < fetchData.length(); i++) {
					JSONObject c = fetchData.getJSONObject(i);
					db.addJavaChapter(new ChapterHandler(c.getInt("id"), c.getString("date_modified"), c.getString("chapter_title"), c.getInt("chapter_no"), c.getString("objective")));
					Log.i(TAG, "data added");
					isFetchSuccessful = true;
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return isFetchSuccessful;
	}

	private boolean getNetBeansChapters() {
		boolean isFetchSuccessful = false;
		ChapterHandler lastBranch =  new ChapterHandler();
		lastBranch = db.getNetBeansLatestChapter();
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		/*** Save data to news and events database ***/
		try {
			String newTime = "0000-00-00 00:00:00";
			if(lastBranch != null)
				newTime = lastBranch.getDate_entry().replace(" ", "/");

			params.add(new BasicNameValuePair("date_modified", newTime));
			Log.i(TAG, "params " + params.toString() );
			final String json = jParser.makeHttpRequest(NETBEANS_CHAPTER_URL, "POST", params);
			Log.i(TAG, "JSON NETBEANS chapter: " + json);
			fetchData = new JSONArray(json);
			if(fetchData != null){
				for (int i = 0; i < fetchData.length(); i++) {
					JSONObject c = fetchData.getJSONObject(i);
					db.addNetBeansChapter(new ChapterHandler(c.getInt("id"), c.getString("date_modified"), c.getString("chapter_title"), c.getInt("chapter_no"), c.getString("objective")));
					Log.i(TAG, "data added");
					isFetchSuccessful = true;
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return isFetchSuccessful;
	}



	/** ===================== ALERT ===================**/
	public void showAlertDialog(Context context, String title, String message,
			Boolean status) {
		AlertDialog alertDialog = new AlertDialog.Builder(context).create();

		alertDialog.setTitle(title);
		alertDialog.setMessage(message);
		if(status != null)
			alertDialog.setIcon((status) ? R.drawable.success : R.drawable.warning);
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				new CheckInternetConnection().execute();
			}
		});
		alertDialog.show();
	}


}
