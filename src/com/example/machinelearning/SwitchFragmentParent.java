package com.example.machinelearning;

import java.text.SimpleDateFormat;

import android.app.Activity;
import android.os.Bundle;
import android.provider.ContactsContract.Contacts.Data;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;

import com.actionbarsherlock.app.SherlockFragment;
import com.example.machinelearning.Database.Database;
import com.example.machinelearning.Database.DictionaryHandler;
import com.example.machinelearning.Utilities.ConnectionDetector;
import com.example.machinelearning.imagedownloader.ImageLoader;

public class SwitchFragmentParent extends SherlockFragment implements AnimationListener{
	View view;
	ConnectionDetector cn;
	ImageLoader imageLoad;
	SwitchFragment switchFrag;
	SwitchToLesson switchLesson;
	Animation animate;
	Database db;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.netbeans_chapter, container, false);
		cn = new  ConnectionDetector(getActivity().getApplicationContext());
		return view;
	}


	public interface SwitchFragment{
		void switchFragment(Fragment fragment);
		void switchFragment(Fragment fragment, Bundle bundle);
		void nextLesson(int lesson, int chatper, Fragment fragment);
		void switchSearch(int dictionaryID , Fragment fragment );
		void switchInstruction(int instructionID, Fragment fragment);
//		void playVideo(String location);
		void goBack(Fragment fragment);
	}


	public interface SwitchToLesson{
		public void switchToLesson(int lessonNum, Fragment fragment);
		public void switchToQuiz(int chapter, Fragment fragment);
	}
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		switchFrag = (SwitchFragment) activity;
		switchLesson = (SwitchToLesson) activity;
	}
	
	
	// Text Animator
	@Override
	public void onAnimationEnd(Animation arg0) {
		
	}
	@Override
	public void onAnimationRepeat(Animation animation) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onAnimationStart(Animation animation) {
		// TODO Auto-generated method stub
		
	}
	
	public static String getDate(){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return sdf.format(new java.util.Date());
	}



}
