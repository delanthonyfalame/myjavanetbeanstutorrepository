package com.example.machinelearning;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;

import android.R.integer;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.WindowManager.LayoutParams;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.machinelearning.Adapter.MarkAdapter;
import com.example.machinelearning.Adapter.QuizScoreAdapter;
import com.example.machinelearning.Database.AnswerHandler;
import com.example.machinelearning.Database.Database;
import com.example.machinelearning.Database.MarkHandler;
import com.example.machinelearning.Database.QuizHandler;
import com.example.machinelearning.Database.QuizScoreHandler;
import com.example.machinelearning.Database.Quiz_resultHandler;
import com.example.machinelearning.Database.StatisticsHandler;
import com.example.machinelearning.Utilities.AlertDialogManager;
import com.example.machinelearning.Utilities.GlobalVariables;

public class NetBeansQuiz extends SwitchFragmentParent implements GlobalVariables{
	protected static final String TAG = "Quiz";
	private View view;
	int quizNumber  = 0;
	ArrayList<QuizHandler> quizArray = new ArrayList<QuizHandler>();
	ArrayList<QuizHandler> quizAnswers = new ArrayList<QuizHandler>();
	ArrayList<MarkHandler> mark = new ArrayList<MarkHandler>();
	QuizHandler currentQuiz = new QuizHandler();
	ArrayList<AnswerHandler> answers = new ArrayList<AnswerHandler>();
	private RadioButton selected; 
	int chapter;
	AlertDialogManager alert = new AlertDialogManager();

	RadioGroup choices;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.quiz2, container, false);
		db = new Database(getActivity());
		mark.clear();
		Bundle bundle = this.getArguments();
		chapter = bundle.getInt("chapter");
		quizArray = db.getNetBeansAllQuizByChapter(chapter);
		Collections.shuffle(quizArray);
		Log.i(TAG, "quiz array count " + quizArray.size());
		total = 0;
		for(QuizHandler quizData : quizArray)
			Log.i(TAG, "quiz data " + quizData.getAnswer() + " " + quizData.getQuestion());
		setWidget();
		return view;
	}
	TextView quiz;
	RadioButton choice1, choice2, choice3, choice4;
	private void setWidget() {
		//set TextViews
		quiz = (TextView) view.findViewById(R.id.quiz_question);

		quiz.setText(quizArray.get(quizNumber).getQuestion());
		choices = (RadioGroup) view.findViewById(R.id.choices);
		setChoices();
		// load the animation
		animate = AnimationUtils.loadAnimation(getSherlockActivity().getApplicationContext(),
				R.animator.bounce);

		// set animation listener
		animate.setAnimationListener(this);
		//set choices



		//set on click on mark button
		view.findViewById(R.id.mark_quiz).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Log.i(TAG, "mark");
				mark.add(new MarkHandler(quizNumber, quizArray.get(quizNumber).getBase_id()));
			}
		});

		//set on click on next button
		view.findViewById(R.id.next).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Log.i(TAG, "checkd  " + choices.getCheckedRadioButtonId() );

				//check if item is selected
				if(choice1.isChecked() || choice2.isChecked() || choice3.isChecked() ||choice4.isChecked()){
					Log.i(TAG, "quiz array "+  quizArray.size() + " quiz " + quizNumber);
					int selectedId = choices.getCheckedRadioButtonId();
					selected = (RadioButton) view.findViewById(selectedId);
					answers.add(new AnswerHandler(quizArray.get(quizNumber) , selected.getText().toString()));
					quizAnswers.add(quizArray.get(quizNumber));
					if(quizArray.size()-1 > quizNumber){
						nextQuiz(++quizNumber);
					}else{
						if(mark.size() != 0)
							showMarkQuiz();
						else
							showScore();
						Log.i(TAG," mark size : " + mark.size());
					}

				}else
					Toast.makeText(getActivity(), "Please select your answer", Toast.LENGTH_SHORT).show();
			}
		});


	}

	protected void showMarkQuiz() {
		final Dialog dialog = new Dialog(getActivity());
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow().setBackgroundDrawableResource(android.R.color.white);

		dialog.setContentView(R.layout.mark_list);
		ListView list  = (ListView) dialog.findViewById(R.id.list);
		list.setAdapter(new MarkAdapter(getActivity(), mark));
		dialog.findViewById(R.id.finalize).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				showScore();
				dialog.dismiss();
			}
		});
		dialog.show();
	}

	int total = 0;
	boolean isPass = false;
	ArrayList<QuizScoreHandler> quizScore = new ArrayList<QuizScoreHandler>();
	protected void showScore() {
		final Dialog dialog = new Dialog(getActivity());
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setCancelable(false);
		dialog.getWindow().setBackgroundDrawableResource(android.R.color.white);
		dialog.setContentView(R.layout.score);
		dialog.getWindow().setLayout(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		TextView score = (TextView) dialog.findViewById(R.id.score);

		int i = 0;
		for(AnswerHandler item : answers){
			i++;
			boolean isCorrect = false;
			if(item.getQuiz().getAnswer().equals(item.getAnswer())){
				total++;
				isCorrect = true;
			}
			quizScore.add(new  QuizScoreHandler(item.getQuiz(), item.getAnswer(), i,   isCorrect, item.getQuiz().getAnswer()));
			Log.i(TAG, "item " + item.getQuiz().getAnswer() + " : " + item.getAnswer() );
		}
		ListView list = (ListView) dialog.findViewById(R.id.list);
		list.setAdapter(new QuizScoreAdapter(getActivity(), quizScore));

		score.setText(String.valueOf(total));
		Log.i(TAG , "total socre : " + total);
		TextView finalize = (TextView) dialog.findViewById(R.id.finalize);
		TextView scoreInfo = (TextView) dialog.findViewById(R.id.score_info);
		if(total > 4){
			finalize.setText("Finalize");
			scoreInfo.setText("Your Score is " + total + " over 10 items, equivalent to the rating of "+getPercentage(total) +". You have satisfied the learning objectives of this chapter. You can now take your quiz.");
			isPass = true;
		}else{
			isPass = false;
			finalize.setText("Finalize");
			scoreInfo.setText("Your Score is "+ total + " over 10 items, equivalent to the rating of "+getPercentage(total) +". You did not satisfy the learning objectives of this chapter. Please study this chapter again. ");
			//			showAlertDialog(getActivity(), "Sorry, please try again...", "Your Score is "+ total + " over 10 items, equivalent to the rating of "+getPercentage(total) +". You did not satisfy the learning objectives of this chapter. Please study this chapter again. ", false);
		}
		finalize.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if(isPass){
					for(AnswerHandler item : answers){
						for(QuizHandler quiz : quizArray){
							if(quiz.getAnswer().equals(item.getAnswer())){
								db.addQuiz_result(new Quiz_resultHandler(quiz.getBase_id(), "Date", quiz.getBase_id(), item.getAnswer(), 1));
								break;
							}
						}

					}
					for(StatisticsHandler items : db.getAllStatistics())
						Log.i(TAG, " item info : " + items.getBase_id() + " date " + items.getDate_entry()+ " size " + items.getScore());
					dialog.dismiss();

					Toast.makeText(getActivity(), "Your result has been successfully added ", Toast.LENGTH_SHORT).show();
					switchFrag.goBack(new NetBeansLesson());
				}else{
					dialog.dismiss();
					switchFrag.goBack(new NetBeansLesson());
				}
				db.addStatistics(new StatisticsHandler(chapter, getDate(), quizArray.size(), getPercentage(total), NETBEANS, total));
				if(db.getStatsByChapterAndTypeCount(chapter, NETBEANS_EXC) <= 0){
					confirmation();
				}else
					Toast.makeText(getActivity(), "Please proceed to the next chapter", Toast.LENGTH_SHORT).show();
			}
		});
		//		dialog.findViewById(R.id.back).setOnClickListener(new OnClickListener() {
		//
		//			@Override
		//			public void onClick(View arg0) {
		//				dialog.dismiss();
		//				switchLesson.switchToQuiz(chapter, new JavaQuiz());
		//			}
		//		});

		dialog.show();

	}

	protected int getPercentage(int total) {
		int percentage = (int) ((( (double)total) / ((double)quizArray.size() ) * 50) + 50);
		Log.i(TAG, "percentage " + percentage + " total " + total + " " + quizArray.size() + " divide " + (total/quizArray.size()));
		return (int) percentage;
	}

	protected void nextQuiz(int quizNumber) {
		setWidget();
		quiz.setAnimation(animate);
		quiz.setText(quizArray.get(quizNumber).getQuestion());
	}

	private void setChoices() {
		//initialize choices
		choice1 = (RadioButton) view.findViewById(R.id.choice1);
		choice2 = (RadioButton) view.findViewById(R.id.choice2);
		choice3 = (RadioButton) view.findViewById(R.id.choice3);
		choice4 = (RadioButton) view.findViewById(R.id.choice4);
		ArrayList<String> arrayChoices  = new ArrayList<String>();
		choices.clearCheck();
		//clear
		choice1.setChecked(false);
		choice2.setChecked(false);
		choice3.setChecked(false);
		choice4.setChecked(false);

		//add data to array
		arrayChoices.add(quizArray.get(quizNumber).getAnswer());
		arrayChoices.add(quizArray.get(quizNumber).getDummy1());
		arrayChoices.add(quizArray.get(quizNumber).getDummy2());
		arrayChoices.add(quizArray.get(quizNumber).getDummy3());

		//shuffle array
		Collections.shuffle(arrayChoices);

		//set animation
		choice1.setAnimation(animate);
		choice2.setAnimation(animate);
		choice3.setAnimation(animate);
		choice4.setAnimation(animate);
		//set text
		choice1.setText(arrayChoices.get(0));
		choice2.setText(arrayChoices.get(1));
		choice3.setText(arrayChoices.get(2));
		choice4.setText(arrayChoices.get(3));

	}

	void confirmation(){
		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				switch (which){
				case DialogInterface.BUTTON_POSITIVE:
					Bundle bundle = new Bundle();
					bundle.putInt( "chapter", chapter );
					switchFrag.switchFragment(new SimulationQuiz(), bundle);
					break;
				case DialogInterface.BUTTON_NEGATIVE:
					switchFrag.goBack(new JavaLesson());
					break;
				}
			}
		};
	
	}

}
