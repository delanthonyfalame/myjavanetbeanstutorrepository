package com.example.machinelearning;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.machinelearning.Adapter.MarkAdapter;
import com.example.machinelearning.Database.Database;
import com.example.machinelearning.Database.QuizHandler;
import com.example.machinelearning.Database.Simulation_quizHandler;
import com.example.machinelearning.Database.Simulation_quizHandler2;
import com.example.machinelearning.Database.StatisticsHandler;
import com.example.machinelearning.Utilities.GlobalVariables;

public class SimulationQuiz2 extends SwitchFragmentParent implements GlobalVariables{
	protected static final String TAG = "Answer";
	private View view;
	private int chapter;
	Simulation_quizHandler2 simulationData;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.simulation, container, false);
		db = new Database(getSherlockActivity());
		Bundle bundle = this.getArguments();
		chapter = bundle.getInt("chapter");
		Log.i(TAG, "chapter : " + chapter);
		simulationData = db.getSimulation_quiz2(chapter);
		setWidget();
		return view;
	}
	
	private void setWidget() {
		TextView simulationQuestion = (TextView) view.findViewById(R.id.simulation_question);
		final EditText answer = (EditText) view.findViewById(R.id.answer);
		
		simulationQuestion.setText(simulationData.getQuestion());
		view.findViewById(R.id.finalize).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				confirmation(answer.getText().toString());
			}
		});
	}

	private static int countLines(String str){
		   String[] lines = str.split("\r\n|\r|\n");
		   return  lines.length;
		}
	
	void confirmation(final String answer){
		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				switch (which){
				case DialogInterface.BUTTON_POSITIVE:
					if(answer.equals(simulationData.getAnswer())){
						Log.i(TAG, "match!");
						Toast.makeText(getActivity(), "Your answer is correct!\n\nYou got a passing grade of 100", Toast.LENGTH_LONG).show();
						db.addStatistics(new StatisticsHandler(chapter, getDate(), chapter, 100, NETBEANS_EXC, 100));
					}else{
						Toast.makeText(getActivity(), "Your answer is incorrect!\n\nYou got a failing grade of 50", Toast.LENGTH_LONG).show();
						db.addStatistics(new StatisticsHandler(chapter, getDate(), chapter, 50, NETBEANS_EXC, 50));
					}
					simulationAnswer(answer);
					break;
				case DialogInterface.BUTTON_NEGATIVE:
					switchFrag.goBack(new NetBeansLesson());
					break;
				}
			}
		};
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Alert").setMessage("Are you sure you want to finish the Quiz?").setPositiveButton("Yes", dialogClickListener)
		.setNegativeButton("No", dialogClickListener).show();
	}
	
	public void simulationAnswer(String yourAnswer){
		final Dialog dialog = new Dialog(getActivity());
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.identification_result);
		TextView question = (TextView) dialog.findViewById(R.id.question);
		TextView correctAnswer = (TextView) dialog.findViewById(R.id.correct_answer);
		TextView userAnswer = (TextView) dialog.findViewById(R.id.your_answer);
		TextView explanation = (TextView) dialog.findViewById(R.id.explanation);
		question.setText(simulationData.getQuestion());
		correctAnswer.setText(simulationData.getAnswer());
		explanation.setText(simulationData.getExplanation());
		userAnswer.setText(yourAnswer);
		
		dialog.findViewById(R.id.done).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				dialog.dismiss();
				switchFrag.goBack(new NetBeansLesson());
			}
		});
		dialog.setCancelable(false);
		dialog.setOnDismissListener(new OnDismissListener() {
			
			@Override
			public void onDismiss(DialogInterface dialog) {
				switchFrag.goBack(new NetBeansLesson());
			}	
		});
		dialog.show();
	}
	

}
